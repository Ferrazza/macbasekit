//
//  SubtitleTableCellView.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 07/05/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

open class SubtitleTableCellView: NSTableCellView {

    @IBOutlet open var textFieldSubtitle: NSTextField!
    
    public static let titleFontSize:CGFloat = 16
    public static let detailFontSize:CGFloat = 14
    static let imageSize:CGFloat = 70
    static let elementsInset:CGFloat = 2
    static let textFieldHorizontalInset:CGFloat = 5

    open override func awakeFromNib() {
        super.awakeFromNib()
        self.textFieldSubtitle.allowsEditingTextAttributes = true
    }
    
    public var backgroundColor:NSColor?{
        didSet{
            if self.backgroundColor != nil{
                self.draw(self.frame)
            }
        }
    }
    
    public static func height(for title:String, subTitle:String?, columnWidth:CGFloat, imagePresent:Bool) -> CGFloat{

        let cellWidth = columnWidth
        var textFieldWidth = cellWidth - SubtitleTableCellView.textFieldHorizontalInset * 2
        if imagePresent {
            textFieldWidth = cellWidth - SubtitleTableCellView.imageSize - SubtitleTableCellView.textFieldHorizontalInset - SubtitleTableCellView.textFieldHorizontalInset * 2
        }
        
        let tfName = NSTextField(frame: NSRect(x: 0, y: 0, width:textFieldWidth , height: 0))
        tfName.stringValue = title
        tfName.font = NSFont.boldSystemFont(ofSize: SubtitleTableCellView.titleFontSize)
        let sizeName = tfName.sizeThatFits(NSSize(width: textFieldWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var result = sizeName.height + SubtitleTableCellView.elementsInset * 2
        
        var subTitleText = "description"
        if let s = subTitle{
            subTitleText = s
        }
        
        let tfSubtitle = NSTextField(frame: NSRect(x: 0, y: 0, width:textFieldWidth , height: 0))
        tfSubtitle.stringValue = subTitleText
        tfSubtitle.font = NSFont.systemFont(ofSize: SubtitleTableCellView.detailFontSize)
        let sizeSubTitle = tfSubtitle.sizeThatFits(NSSize(width: textFieldWidth, height: CGFloat.greatestFiniteMagnitude))
        result = result + sizeSubTitle.height + SubtitleTableCellView.elementsInset
        return result
    }
    
    public static func height(for title:String, attributedSubTitle:NSAttributedString, columnWidth:CGFloat, imagePresent:Bool) -> CGFloat{
        let cellWidth = columnWidth
        var textFieldWidth = cellWidth - SubtitleTableCellView.textFieldHorizontalInset * 2
        if imagePresent {
            textFieldWidth = cellWidth - SubtitleTableCellView.imageSize - SubtitleTableCellView.textFieldHorizontalInset - SubtitleTableCellView.textFieldHorizontalInset * 2
        }
        
        let tfName = NSTextField(frame: NSRect(x: 0, y: 0, width:textFieldWidth , height: 0))
        tfName.stringValue = title
        tfName.font = NSFont.boldSystemFont(ofSize: SubtitleTableCellView.titleFontSize)
        let sizeName = tfName.sizeThatFits(NSSize(width: textFieldWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var result = sizeName.height + SubtitleTableCellView.elementsInset * 2
        
        
        let tfSubtitle = NSTextField(frame: NSRect(x: 0, y: 0, width:textFieldWidth , height: 0))
        tfSubtitle.attributedStringValue = attributedSubTitle
        tfSubtitle.font = NSFont.systemFont(ofSize: SubtitleTableCellView.detailFontSize)
        let sizeSubTitle = tfSubtitle.sizeThatFits(NSSize(width: textFieldWidth, height: CGFloat.greatestFiniteMagnitude))
        result = result + sizeSubTitle.height + SubtitleTableCellView.elementsInset
        return result
    }

    
    open override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        let ctx = NSGraphicsContext.current?.cgContext
        //draw the background if needed
        if let color = self.backgroundColor{
            ctx?.setFillColor(color.cgColor)
            ctx?.fill(dirtyRect)
        }
        
        //draw the line under the cell
        let pathLine: CGMutablePath = CGMutablePath()
        pathLine.move(to: CGPoint(x: 0, y: 0))
        pathLine.addLine(to: CGPoint(x: dirtyRect.size.width, y: 0))
        ctx?.setLineWidth(0.5)
        ctx?.setStrokeColor(NSColor.gray.cgColor)
        ctx?.addPath(pathLine)
        ctx?.strokePath()
        
        
    }
    
}
