//
//  ImageDownloader.swift
//  PostKit
//
//  Created by Alessandro Ferrazza on 11/03/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//


public class ImageDownloader: PostObject {

    public var headerDict = [String:String]() //dict of element in header for urls
    public var utilizeFileManager = true//if set on utilize File Manager To Save files - default = false
    public var multiImageDownloadCount: Int = 0 //if utilize File Manager is ser True in order To Save files - default = 0
    public var utilizeMainBundle = false //if set true utilize MainBundle To Load Files (if there are no images in main bundle utilizes the online resources setting utilizemainbundle to no)
    public var baseUrlToRemoveForMainBundle:String? //Remove the base url from the main url
    
    //var multipleImagesArray = [NSImage]()
    
    func innerDownloadImage(percentDownloadBlock: @escaping (Float) -> Void,
                            completionBlock completion: @escaping (NSImage?, Error?) -> Void) {
        self.downloadAsynchronous(percentDownloadBlock: percentDownloadBlock) { [weak self] (data, response, error) in
            if let d = data{
                let image = NSImage(data: d)
                if let utilize = self?.utilizeFileManager, let i = image{
                    if utilize {
                        //removed the dispatch global because it slowed the download of images and images were downloaded incorrectly.
                        //DispatchQueue.global().async { [weak self] () in
                            if let url = self?.requestGenerator.url{
                                FileObject.shared.save(image: i, with: FileObject.fileName(for: url))
                            }
                        //}
                    }
                }
                completion(image, error)
            }
        }
    }
    
    ///downloaded flag is to know the image that has been downloaded or retrieved from file manager or main bundle or the imageobject itself
    ///the images downloaded will be saved in file manager if needed
    ///the images not downloaded will be not saved in file manager
    public func downloadImage(percentDownloadBlock: @escaping (Float) -> Void,
                              completionBlock completion: @escaping (_ image: NSImage?, _ error: Error?, _ downloaded: Bool) -> Void) {
        
        if self.utilizeMainBundle{
            var mainBundleUrl = self.requestGenerator.url.path
            if let baseUrlToRemove = self.baseUrlToRemoveForMainBundle {
                mainBundleUrl = mainBundleUrl.replacingOccurrences(of: baseUrlToRemove, with: "")
            }
            if let imageSaved = FileObject.shared.imageInMainBundle(with: mainBundleUrl){
                DispatchQueue.main.async {
                    percentDownloadBlock(1)
                    completion(imageSaved, nil, false)
                }
            }else{
                self.utilizeMainBundle = false
                self.downloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: completion)
            }
        }else if self.utilizeFileManager{
            if let imageSaved = FileObject.shared.image(named: FileObject.fileName(for: self.requestGenerator.url)){
                percentDownloadBlock(1)
                //print("FILE MANAGER = \(self.requestGenerator.url.absoluteString)")
                completion(imageSaved, nil, false)
                   
            }else{
                self.innerDownloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: { (image, error) in
                    //print("DOWNLOAD = \(self.requestGenerator.url.absoluteString)")
                    completion(image, error, true)
                })
                
            }
            
        }else {
            self.innerDownloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: { (image, error) in
                completion(image, error, true)
            })
        }
/*
        DispatchQueue.global().async {[weak self] () in
            if let weakSelf = self{
                if weakSelf.utilizeMainBundle{
                    if var mainBundleUrl = self?.requestGenerator.url.path{
                        if let baseUrlToRemove = self?.baseUrlToRemoveForMainBundle {
                            mainBundleUrl = mainBundleUrl.replacingOccurrences(of: baseUrlToRemove, with: "")
                        }
                        if let imageSaved = FileObject.shared.imageInMainBundle(with: mainBundleUrl){
                            DispatchQueue.main.async {
                                percentDownloadBlock(1)
                                completion(imageSaved, nil)
                            }
                        }else{
                            self?.utilizeMainBundle = false
                            self?.downloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: completion)
                        }
                    }
                }else if weakSelf.utilizeFileManager{
                    if let imageSaved = FileObject.shared.image(named: FileObject.fileName(for: weakSelf.requestGenerator.url)){
                        DispatchQueue.main.async {
                            percentDownloadBlock(1)
                            completion(imageSaved, nil)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self?.innerDownloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: completion)
                        }
                    }
                    
                }else {
                    self?.innerDownloadImage(percentDownloadBlock: percentDownloadBlock, completionBlock: completion)
                }
            }
        }
        */
    }

    ///downloaded flag is to know the image that has been downloaded
    ///the images downloaded will be saved in file manager if needed
    ///the images not downloaded will be not saved in file manager
    public func downloadImage(withCompletionBlock completion: @escaping (_ image: NSImage?, _ error: Error?, _ downloaded: Bool) -> Void) {
        self.downloadImage(percentDownloadBlock: { (percent) in
            //do nothing
        }, completionBlock: completion)
    }
    
    //MARK: Multiple downloads
    public func downloadImages(from imageObject:ImageObject,
                               percentDownloadBlock: @escaping (Float) -> Void,
                               completionStartImageWithNumber: @escaping (_ imageCount:Int, _ totalCount:Int) -> Void,
                               completionSingleImage: @escaping (NSImage?, Int, Error?) -> Void,
                               completionAllImages: @escaping ([NSImage]) -> Void){
        
        completionStartImageWithNumber(self.multiImageDownloadCount, imageObject.images.count)
        if imageObject.images.count > self.multiImageDownloadCount{
            //let imageValue = imageObject.imageValues[self.multiImageDownloadCount]
            if (imageObject.images[self.multiImageDownloadCount] != nil){
                self.completedSingleImage(with: imageObject,
                                          error: nil,
                                          downloaded: false,
                                          percentDownloadBlock: percentDownloadBlock,
                                          completionStartImageWithNumber: completionStartImageWithNumber,
                                          completionSingleImage: completionSingleImage,
                                          completionAllImages: completionAllImages)
            }else if let url = imageObject.imageUrls[self.multiImageDownloadCount]{
                self.requestGenerator.url = url
                self.downloadImage(percentDownloadBlock: percentDownloadBlock,
                                   completionBlock: { [weak self](image, error, downloaded) in
                                    if let count = self?.multiImageDownloadCount{
                                        imageObject.imageValues[count].image = image
                                        self?.completedSingleImage(with: imageObject,
                                                                   error: error,
                                                                   downloaded: downloaded,
                                                                   percentDownloadBlock: percentDownloadBlock,
                                                                   completionStartImageWithNumber: completionStartImageWithNumber,
                                                                   completionSingleImage: completionSingleImage,
                                                                   completionAllImages: completionAllImages)

                                    }
                })
            }else{
                self.completedSingleImage(with: imageObject,
                                          error: nil,
                                          downloaded: false,
                                          percentDownloadBlock: percentDownloadBlock,
                                          completionStartImageWithNumber: completionStartImageWithNumber,
                                          completionSingleImage: completionSingleImage,
                                          completionAllImages: completionAllImages)
            }
            
        }else{
            completionAllImages(imageObject.downloadedImages)
        }
        
        
    }
    
    
    func completedSingleImage(with imageObject:ImageObject,
                              error: Error?,
                              downloaded: Bool,
                              percentDownloadBlock: @escaping (Float) -> Void,
                              completionStartImageWithNumber: @escaping (_ imageCount:Int, _ totalCount:Int) -> Void,
                              completionSingleImage: @escaping (NSImage?, Int, Error?) -> Void,
                              completionAllImages: @escaping ([NSImage]) -> Void){
        let image = imageObject.imageValues[self.multiImageDownloadCount].image
        completionSingleImage(image, self.multiImageDownloadCount, error)
        self.multiImageDownloadCount = self.multiImageDownloadCount + 1
        if imageObject.images.count > self.multiImageDownloadCount{
            if let url = imageObject.imageValues[self.multiImageDownloadCount].url, let img = image{
                if self.utilizeFileManager{
                    if downloaded{
                        
                        FileObject.shared.save(image: img, with: FileObject.fileName(for: url))
                    }
                    //print("Saved with file object in \(FileObject.fileName(for: url))")
                }
            }
        }
        
        
        self.downloadImages(from: imageObject,
                            percentDownloadBlock: percentDownloadBlock,
                            completionStartImageWithNumber: completionStartImageWithNumber,
                            completionSingleImage: completionSingleImage,
                            completionAllImages: completionAllImages)
        
    
    }
}
