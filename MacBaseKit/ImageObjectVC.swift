//
//  ImageObjectVC.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 09/05/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

@available(OSX 10.11, *)
public protocol ImageObjectVCProtocol {
    func imageObjectVCProtocolDidEnd(with arrayImages:[NSImage]?, imageObjectVC:ImageObjectVC)
    func imageObjectVCProtocolCancel(imageObjectVC:ImageObjectVC)

}


@available(OSX 10.11, *)
open class ImageObjectVC: NSViewController, DragDestinationViewProtocol, ImagesCollectionViewProtocol {
    @IBOutlet open var progressionIndicator:NSProgressIndicator!
    @IBOutlet open var scrollView:NSScrollView!
    @IBOutlet open var imageView:NSImageView!
    
    @IBOutlet var progressionTextView: NSTextView!
    @IBOutlet var okButton: NSButton!
    @IBOutlet var cancelButton: NSButton!
    @IBOutlet var revertButton: NSButton!
    @IBOutlet var editButton: NSButton!
    @IBOutlet var checkButtonAddImage: NSButton!

    @IBOutlet open var dragView:DragDestinationView!
    
    @IBOutlet var countImageLabel: NSTextField!
    var arrayImagesModified = false

    open var delegate:ImageObjectVCProtocol?
    var baseFrameImage:CGRect!
    open var arrayImages = [NSImage]()
    var arrayOriginalImages = [NSImage]()
    open var isViewer = false


    public var utilizeFileManager = false //if set on utilize File Manager To Save files - default = NO
    public var utilizeMainBundle = false //if set on utilize Main Bundle To Save files - default = NO
    open var imageObject:ImageObject? = nil

    static let singleImageOffset:CGFloat = 30
    
    
    open var initialText:String? = nil

    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.progressionIndicator?.isHidden = true
        self.progressionTextView?.isEditable = false
        
        if let text = self.initialText{
            let modifiedText = text + "\n\n"
            let attributedText = NSMutableAttributedString(fromTaggedString: modifiedText, baseAttributes: [NSAttributedString.Key.font:NSFont.systemFont(ofSize: SubtitleTableCellView.detailFontSize), NSAttributedString.Key.foregroundColor:NSColor.controlTextColor])
            self.progressionTextView.textStorage?.append(attributedText)
            
            
        }
        self.dragView.setup()
        self.dragView.delegate = self
        self.downloadImages()
        
        //set the minimum size as the actual size
        //print(self.preferredMinimumSize)


        /*
        if self.arrayImageUrls == nil{
            self.setupScrolling()
        }
 */
        if self.imageObject == nil{
            self.setupScrolling()
        }
        if self.isViewer{
            self.cancelButton.isHidden = true
            self.revertButton.isHidden = true
            self.editButton.isHidden = true

            self.checkButtonAddImage.isHidden = true
            self.progressionIndicator.isHidden = true
        }
        self.editButton.isEnabled = false
    }
    
    @IBAction open func okButtonAction(sender:NSButton){
        if self.arrayImagesModified{
            self.delegate?.imageObjectVCProtocolDidEnd(with: self.arrayImages, imageObjectVC: self)
        }else{
            self.delegate?.imageObjectVCProtocolDidEnd(with: nil, imageObjectVC: self)
        }
        self.dismiss(self)
    }
    @IBAction open func cancelButtonAction(sender:NSButton){
        self.delegate?.imageObjectVCProtocolCancel(imageObjectVC: self)
        self.dismiss(self)

    }
    @IBAction open  func revertButtonAction(sender:NSButton){
        self.arrayImages = self.arrayOriginalImages
        self.setupScrolling()
    }
    @IBAction open func editButtonAction(sender:NSButton){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let collectionView = storyboard.instantiateController(withIdentifier: "ImagesCollectionView") as! ImagesCollectionView
        collectionView.delegate = self
        collectionView.images = self.arrayImages
        self.presentAsSheet(collectionView)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func setupScrolling(){
        if self.arrayImages.count > 0{
            self.imageView.image = self.arrayImages[0]
        }

        let imageOffset = ImageObjectVC.singleImageOffset * CGFloat(arrayImages.count)
        let totalOffset = self.scrollView.frame.size.height + imageOffset
        self.scrollView.documentView?.frame = CGRect(x: 0, y: 0, width: self.scrollView.bounds.size.width, height: totalOffset)
        self.scrollView.documentView?.scroll(NSPoint(x: 0, y: totalOffset))
        self.scrollView.contentView.postsBoundsChangedNotifications = true
        
        if self.arrayImages.count > 0{
            //self.imageView.image = self.arrayImages[0]
            //self.scrollView.contentView.scroll(to: NSPoint(x: 0, y: 0))
            self.imageView.frame = self.dragView.bounds
            self.baseFrameImage = imageView.frame
            
            self.scrollImage(clipView: self.scrollView.contentView)

            NotificationCenter.default.removeObserver(self)
            NotificationCenter.default.addObserver(forName: NSView.boundsDidChangeNotification,
                                                   object: self.scrollView.contentView,
                                                   queue: nil) { [weak self](notification) in
                                                    
                                                    if let clipView = notification.object as? NSClipView{
                                                        self?.scrollImage(clipView: clipView)
                                                    }
            }
            
            NotificationCenter.default.addObserver(forName: NSWindow.didResizeNotification,
                                                   object: nil,
                                                   queue: nil,
                                                   using: { [weak self](notification) in
                                                    if let bounds = self?.dragView.bounds{
                                                        self?.imageView.frame = bounds
                                                        self?.baseFrameImage = bounds

                                                    }
            })
        }
        
    }
    
    func scrollImage(clipView:NSClipView){
        let imageOffset = ImageObjectVC.singleImageOffset * CGFloat(arrayImages.count)
        let offset = clipView.bounds.origin.y //- 2.1 //the content view is 2 point higher than content set on document view!!!!!!!!
        //print(offset)
        //print(imageOffset)
        if offset >= 0 && offset <= imageOffset{//change image
            self.imageView.frame = self.baseFrameImage
            var imageNumber = Int(offset/ImageObjectVC.singleImageOffset)

            imageNumber = self.arrayImages.count - imageNumber - 1
            self.countImageLabel.stringValue = "\(imageNumber + 1)/\(self.arrayImages.count)"
            if imageNumber >= 0 && imageNumber < self.arrayImages.count{
                self.imageView.image = self.arrayImages[imageNumber]
            }
        }else{ //move image
            if offset <= 0{
                self.imageView.frame.origin.y = self.baseFrameImage.origin.y - offset
            }else if offset >= imageOffset{
                let offsetDiff = offset - imageOffset
                self.imageView.frame.origin.y = self.baseFrameImage.origin.y - offsetDiff
            }
        }
    }
    
    //MARK: Downloading
    func downloadImages(){
        
        if let imageObject = self.imageObject{
            if let imageUrl = imageObject.imageUrl{
                DispatchQueue.main.async {
                    self.loadingLabel(loading: true,
                                      with: "Downloading images from \(imageUrl.absoluteString))",
                        color: NSColor.green)
                }
            }
           
            if imageObject.allImagesHasBeenDownloaded(){
                for image in imageObject.images{
                    if let i = image{
                        self.arrayImages.append(i)
                        self.setupScrolling()
                    }
                }
                DispatchQueue.main.async {
                    self.editButton.isEnabled = true
                }
                return
            }
            imageObject.downloadImages(utilizeFileManager: self.utilizeFileManager,
                                       utilizeMainBundle: self.utilizeMainBundle,
                                       baseUrlToRemoveForMainBundle: nil,
                                       headers: nil,
                                       percentDownloadBlock: { (percent) in
                                        //nothing
            }, completionStartImageWithNumber: { [weak self](imageNumber, totalNumber ) in
                DispatchQueue.main.async {

                    let progression = Double(imageNumber)/Double(totalNumber)
                    self?.loadingLabel(loading: true,
                                       with: "Downloading image \(imageNumber) of \(totalNumber)",
                        color: nil)
                    self?.setPercentProgressionIndicator(percent: progression)
                }
            }, completionSingleImage: { (image, imageNumber, error) in
                //nothing
            }, completionAllImages: { [weak self](images) in
                DispatchQueue.main.async {
                    self?.loadingLabel(loading: false,
                                       with: "Images downloaded",
                                       color: NSColor.myDarkGreen)
                    self?.arrayImages.append(contentsOf: images)
                    self?.setupScrolling()
                    self?.editButton.isEnabled = true

                }
            })
        }else{
            DispatchQueue.main.async {
                self.loadingLabel(loading: false,
                                  with: "No Images to download",
                                  color: NSColor.myDarkRed)
            }
        }
    }
    
    /*
    func downloadSingleImage(from url:URL, completionHandler: @escaping (NSImage) -> Swift.Void){
        let session = URLSession.shared
        let task = session.downloadTask(with: url, completionHandler: { (url, response, error) in
            var addedImage = false
            if let unwUrl = url{
                if let image = NSImage(contentsOf: unwUrl){
                    completionHandler(image)
                    addedImage = true
                }
            }
            if addedImage == false{
                completionHandler(#imageLiteral(resourceName: "unknownImg"))
            }
        })
        task.resume()
    }
    */
    //Drag and drop
    public func dragDestinationViewReturn(images: [NSImage], point: NSPoint, view: DragDestinationView) {
        self.arrayImagesModified = true
        //self.arrayImages.append(contentsOf: images)
        //self.arrayOriginalImages.append(contentsOf: images)
        if self.checkButtonAddImage.state == .on{
            self.arrayImages.append(contentsOf: images)
            self.arrayOriginalImages.append(contentsOf: images)

        }else{
            self.arrayImages = images
            self.arrayOriginalImages = images
        }
        
        DispatchQueue.main.async {
            self.loadingLabel(loading: false,
                              with: "\(images.count) images loaded",
                color: NSColor.myDarkGreen)
            self.setupScrolling()
        }
    }
    public func dragDestinationViewConvertedImageFromDicom(withPercent percent:Double, name: String, errorString: String?, view: DragDestinationView) {
        DispatchQueue.main.async {
            if let e = errorString{
                self.loadingLabel(loading: false,
                                  with: "Error in converting \(name)\n\(e)",
                    color: NSColor.myDarkRed)
            }else{
                self.loadingLabel(loading: true,
                                  with: "Converting \(name)",
                    color: nil)
                self.setPercentProgressionIndicator(percent: percent)
                
            }
            
        }
    }
    //Cropping
    
    public func dragDestinationViewCrop(in rect: CGRect, view: DragDestinationView) {
        var croppedArrayImages = [NSImage]()
        for img in self.arrayImages{
            let newImg = self.crop(image: img, rect: rect)
            croppedArrayImages.append(newImg)
        }
        self.arrayImages = croppedArrayImages
        self.setupScrolling()
    }
    
    static func realSize(for image:NSImage) -> CGSize{
        var imageRepSize = CGSize(width: 0, height: 0)
        let imageReps = image.representations
        for imageRep in imageReps{
            if CGFloat(imageRep.pixelsHigh) > imageRepSize.height{
                imageRepSize.height = CGFloat(imageRep.pixelsHigh)
            }
            if CGFloat(imageRep.pixelsWide) > imageRepSize.width{
                imageRepSize.width = CGFloat(imageRep.pixelsWide)
            }
        }
        return imageRepSize
    }
    
    func crop(image:NSImage, rect:CGRect) -> NSImage{
        //invert the coordinates
        let correctedRect = CGRect(x: rect.origin.x, y: self.dragView.frame.size.height - rect.size.height - rect.origin.y, width: rect.size.width, height: rect.size.height)
        
        //calculate origin and dimension of image relative to view
        var imageSize:CGSize
        var imageOrigin:CGPoint
        
        //sometimes the image size obtained with (image.size) do not corresponde with real pixel size, so is better utilize the following method
        let imageRepSize = ImageObjectVC.realSize(for: image)
    
        //print("Normal size")
        //print(image.size)
        //print("imageRep size")
        //print(imageRepSize)

        let dragViewToImageRatio = (imageRepSize.width/imageRepSize.height) / (self.dragView.frame.size.width/self.dragView.frame.size.height)
        if dragViewToImageRatio > 1 {//image touches the drag view left and right
            imageSize = CGSize(width: self.dragView.frame.size.width, height: imageRepSize.height/imageRepSize.width * self.dragView.frame.size.width)
            imageOrigin = CGPoint(x: 0, y: (self.dragView.frame.size.height - imageSize.height)/2)
        }else{//image touches the the drag view up and down
            imageSize = CGSize(width: imageRepSize.width/imageRepSize.height * self.dragView.frame.size.height, height: self.dragView.frame.size.height)
            imageOrigin = CGPoint(x: (self.dragView.frame.size.width - imageSize.width)/2, y: 0)

        }
        //calculate crop rect relative image
        let correctedRectInImage = CGRect(x: correctedRect.origin.x - imageOrigin.x, y: correctedRect.origin.y - imageOrigin.y, width: correctedRect.size.width, height: correctedRect.size.height)
        
        //create imageref
        var rectImage = CGRect(x: 0, y: 0, width: imageRepSize.width, height: imageRepSize.height)
        let imageRef = image.cgImage(forProposedRect: &rectImage, context: nil, hints: nil)
        
        //find the correct rect by scaling the corrected rect
        let scale = imageRepSize.width / imageSize.width
        let croppingRect = CGRect(x: correctedRectInImage.origin.x * scale, y: correctedRectInImage.origin.y * scale, width: correctedRectInImage.size.width * scale, height: correctedRectInImage.size.height * scale)
        
        //crop
        let croppedImageRef = imageRef?.cropping(to: croppingRect)
        
        /*
         print("----------------")
         print(self.dragView.frame)
         print(imageSize)
         print(imageOrigin)
         print(correctedRectInImage)
         */
        
        return NSImage(cgImage: croppedImageRef!, size: croppingRect.size)
    }
    
    //MARK: Progression
    func loadingLabel(loading:Bool, with message:String, color:NSColor?){
        var actualColor = NSColor.controlTextColor
        if let c = color{
            actualColor = c
        }
        let dictAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: NSFont.systemFont(ofSize: 9), NSAttributedString.Key.foregroundColor: actualColor]
        if let progressionTextView = self.progressionTextView{
            if progressionTextView.attributedString().string.count > 0{
                progressionTextView.textStorage?.append(NSAttributedString(string:"\n", attributes: dictAttributes))
            }
            progressionTextView.textStorage?.append(NSAttributedString(string:">", attributes: dictAttributes))
            
            let actualMessage = NSMutableAttributedString(string: message, attributes: dictAttributes)
            progressionTextView.textStorage?.append(actualMessage)
            
            if let progressionIndicator = self.progressionIndicator{
                if loading == true{
                    progressionIndicator.isHidden = false
                    progressionIndicator.startAnimation(nil)
                }else{
                    progressionIndicator.isHidden = true
                    progressionIndicator.stopAnimation(nil)
                }
            }
            
            if let lenght = self.progressionTextView?.string.count{
                progressionTextView.scrollRangeToVisible(NSMakeRange(lenght, 0))
            }
        }
        
    }
    
    func setPercentProgressionIndicator(percent:Double?){
        if let progressionIndicator = self.progressionIndicator{
            progressionIndicator.isHidden = false
            if let p = percent{
                if progressionIndicator.isIndeterminate{
                    progressionIndicator.isIndeterminate = false
                }
                progressionIndicator.doubleValue = p
            }else{
                progressionIndicator.isIndeterminate = true
            }
        }
        
    }
    //imagesCollectionView protocol
    
    public func imagesCollectionViewEnded(with images: [NSImage]) {
        self.arrayImagesModified = true
        self.arrayImages = images
        
        DispatchQueue.main.async {
            self.loadingLabel(loading: false,
                              with: "\(images.count) images loaded",
                color: NSColor.myDarkGreen)
            self.setupScrolling()
        }
    }
}


