//
//  ColorExtension.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 07/07/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

extension NSColor{
    static let otherColorComponent:CGFloat = 0.6
    static let mainColorComponent:CGFloat = 0.6
    
    public static func darkerColor(for color:NSColor, reducingBy value:CGFloat)-> NSColor{
        let ciColor:CIColor = CIColor(color: color)!

        return NSColor(calibratedRed: CGFloat.maximum(ciColor.red - value, 0.0),
                       green: CGFloat.maximum(ciColor.green - value, 0.0),
                       blue: CGFloat.maximum(ciColor.blue - value, 0.0),
                       alpha: ciColor.alpha)
    }
    public static func lighterColor(for color:NSColor, increasing value:CGFloat)-> NSColor{
        let ciColor:CIColor = CIColor(color: color)!

        return NSColor(calibratedRed: CGFloat.minimum(ciColor.red + value, 1.0),
                       green: CGFloat.minimum(ciColor.green + value, 1.0),
                       blue: CGFloat.minimum(ciColor.blue + value, 1.0),
                       alpha: ciColor.alpha)
    }
    public static func systemBorderedCellTableBackgroundColor() -> NSColor{
        return NSColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
    }
    
    public static var myGreen:NSColor{
        get{
            return NSColor(red: NSColor.otherColorComponent, green: 1, blue: NSColor.otherColorComponent, alpha: 1)
        }
    }
    
    public static var myYellow:NSColor{
        get{
            return NSColor(red: 1, green: 1, blue: NSColor.otherColorComponent, alpha: 1)
        }
    }
    
    public static var myBlue:NSColor{
        get{
            return NSColor(red: NSColor.otherColorComponent, green: NSColor.otherColorComponent, blue: 1, alpha: 1)
        }
    }
    
    public static var myRed:NSColor{
        get{
            return NSColor(red: 1, green: NSColor.otherColorComponent, blue: NSColor.otherColorComponent, alpha: 1)
        }
    }
    
    public static var myDarkGreen:NSColor{
        get{
            return NSColor(red: 0, green: NSColor.mainColorComponent, blue: 0, alpha: 1)
        }
    }
    
    public static var myDarkYellow:NSColor{
        get{
            return NSColor(red: NSColor.mainColorComponent, green: NSColor.mainColorComponent, blue: 0, alpha: 1)
        }
    }
    
    public static var myDarkBlue:NSColor{
        get{
            return NSColor(red: 0, green: 0, blue: NSColor.mainColorComponent, alpha: 1)
        }
    }
    
    public static var myDarkRed:NSColor{
        get{
            return NSColor(red: NSColor.mainColorComponent, green: 0, blue: 0, alpha: 1)
        }
    }

    public convenience init(hex: String) {//format #ffffff
        let colorString = hex.replacingOccurrences(of: "#", with: "")

        let scanner = Scanner(string: colorString)
        //scanner.scanLocation = 0 //deprecated, replaced with currentIndex but when created is always 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    public var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
}
