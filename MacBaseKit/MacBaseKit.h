//
//  MacBaseKit.h
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 20/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for MacBaseKit.
FOUNDATION_EXPORT double MacBaseKitVersionNumber;

//! Project version string for MacBaseKit.
FOUNDATION_EXPORT const unsigned char MacBaseKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MacBaseKit/PublicHeader.h>


