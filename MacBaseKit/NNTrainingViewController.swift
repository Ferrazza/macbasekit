//
//  NNTrainingViewController.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 29/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
public protocol NNTrainingViewControllerDelegate {
    func trainingViewControllerDelegateDidEnd(with network:NeuralNet, controller: NNTrainingViewController)
    func trainingViewControllerDelegateDidCancel(controller: NNTrainingViewController)

}
/**
 Set neuralNetwork, dataSet and delegate
 and call view controller from interface builder
 */
public class NNTrainingViewController: NSViewController, NSTextFieldDelegate {
    public var neuralNetwork:NeuralNet?
    public var dataSet:NeuralNet.Dataset?
    public var delegate:NNTrainingViewControllerDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTrainingTextfield()
        self.setupTrainingPopUps()
        self.doneButton.isEnabled = false
        
    }
    
    //MARK: TRAINING VIEWS
    @IBOutlet var doneButton:NSButton!
    @IBOutlet var labelStatusNetwork:NSTextField!

    
    @IBOutlet var learningRate:NSTextField!
    @IBOutlet var momentum:NSTextField!
    @IBOutlet var numerosity:NSTextField!
    @IBOutlet var maxEpochNum:NSTextField!
    @IBOutlet var errorThreshold:NSTextField!
    
    @IBOutlet var errorFunction:NSPopUpButton!
    @IBOutlet var hiddenActivaction:NSPopUpButton!
    @IBOutlet var outputActivaction:NSPopUpButton!
    
    @IBOutlet var logTextView:NSTextView!
    
    @IBOutlet var progressIndicator: NSProgressIndicator!
    
    //MARK: SAVING AND LOADING
    @IBAction func end(with sender: NSButton){
        if let nn = self.neuralNetwork{
            self.delegate?.trainingViewControllerDelegateDidEnd(with: nn, controller: self)
        }
    }
    @IBAction func cancel(with sender: NSButton){
        self.delegate?.trainingViewControllerDelegateDidCancel(controller: self)
        
    }
    
    //MARK: Training textfield
    
    func setupTrainingTextfield(){
        self.learningRate.stringValue = NNSettings.learningRate.format(f: 3)
        self.learningRate.delegate = self
        self.momentum.stringValue = NNSettings.momentum.format(f: 3)
        self.momentum.delegate = self
        self.numerosity.stringValue = "\(NNSettings.numerosity)"
        self.numerosity.delegate = self
        self.maxEpochNum.stringValue = "\(NNSettings.maxEpochs)"
        self.maxEpochNum.delegate = self
        self.errorThreshold.stringValue = NNSettings.errorThreshold.format(f: 10)
        self.errorThreshold.delegate = self
        
        self.logTextView.font = NSFont.systemFont(ofSize: 10)
        
    }
    public func controlTextDidEndEditing(_ obj: Notification) {
        if let tf = obj.object as? NSTextField{
            let text = tf.stringValue
            switch tf.tag{
            case 0://learningRate
                NNSettings.learningRate = text.floatValue
            case 1://momentum
                NNSettings.momentum = text.floatValue
            case 2://numerosity
                NNSettings.numerosity = text.intValue
            case 3://maxEpochNum
                NNSettings.maxEpochs = text.intValue
            default://errorThreshold
                NNSettings.errorThreshold = text.floatValue
            }
        }
    }
    
   
    
    //MARK: Training popups
    
    func setupTrainingPopUps(){
        self.errorFunction.removeAllItems()
        self.errorFunction.addItems(withTitles: [NNSettings.meanSquaredKey,
                                                 NNSettings.crossEntropyKey,
                                                 NNSettings.percentageKey])
        let titleError = NNSettings.shared.title(for: NNSettings.errorFunction)
        self.errorFunction.selectItem(withTitle: titleError)
        
        self.hiddenActivaction.removeAllItems()
        self.hiddenActivaction.addItems(withTitles: [NNSettings.linearKey,
                                                     NNSettings.rectifiedLinearKey,
                                                     NNSettings.hyperbolicTangentKey,
                                                     NNSettings.sigmoidKey])
        let titleHiddenActivation = NNSettings.shared.title(for: NNSettings.hiddenActivation)
        self.hiddenActivaction.selectItem(withTitle: titleHiddenActivation)
        self.outputActivaction.removeAllItems()
        self.outputActivaction.addItems(withTitles: [NNSettings.linearKey,
                                                     NNSettings.rectifiedLinearKey,
                                                     NNSettings.hyperbolicTangentKey,
                                                     NNSettings.sigmoidKey,
                                                     NNSettings.softmaxKey])
        let titleOutputActivation = NNSettings.shared.title(for: NNSettings.outputActivation)
        self.outputActivaction.selectItem(withTitle: titleOutputActivation)
    }
    
    //MARK: Training Action
    @IBAction func popUpButtonChangeValue(sender:NSPopUpButton){
        if let title = sender.titleOfSelectedItem{
            
            switch sender.tag {
            case 0://error function
                NNSettings.errorFunction = NNSettings.shared.errorFunction(for: title)
            case 1://hidden activation
                NNSettings.hiddenActivation = NNSettings.shared.hiddenActivation(for: title)
            default://output activation
                NNSettings.outputActivation = NNSettings.shared.outputActivation(for: title)
            }
        }
    }
    
    @IBAction open func trainNeuralNetwork(sender:NSButton){
        self.setStatusTraining()
        self.view.window?.makeFirstResponder(nil)
        
        let text = "Training with:\n - learning rate = \(NNSettings.learningRate)\n - momentum = \(NNSettings.momentum)\n - numerosity = \(NNSettings.numerosity)\n - max epoch number = \(NNSettings.maxEpochs)\n - error threshold = \(NNSettings.errorThreshold.format(f: 10))\n - error function = \(NNSettings.shared.title(for: NNSettings.errorFunction))\n - hidden activation = \(NNSettings.shared.title(for: NNSettings.hiddenActivation))\n - output activation = \(NNSettings.shared.title(for: NNSettings.outputActivation))"
        self.log(text: text)
        
        if let nn = self.neuralNetwork, let dataSet = self.dataSet{
            NNManager.shared.train(neuralNetwork: nn,
                                   with: dataSet,
                                   epochCompletion: { [weak self](epochNum, epochErr) in
                                    DispatchQueue.main.async {
                                        let text = "Epoch number: \(epochNum)\nEpoch error:\(epochErr.format(f: 10))"
                                            self?.log(text: text)
                                    }
                }, finalCompletion: { [weak self](neuralNetwork) in
                    self?.neuralNetwork = neuralNetwork
                    DispatchQueue.main.async {
                        self?.log(text: "Neural Network trained!")
                        self?.setStatusTrained()
                    }
            })
        }else{
            self.log(text: "No neural network o dataset!")
        }
    }
    
    //MARK:helper
    func log(text:String){
        if self.logTextView.string.count == 0{
            self.logTextView.string.append(text)
        }else{
            self.logTextView.string.append("\n\(text)")
        }
        if let lenght = self.logTextView?.string.count{
            logTextView.scrollRangeToVisible(NSMakeRange(lenght, 0))
        }
    }
    
    func setStatusTraining(){
        self.progressIndicator.isHidden = false
        self.doneButton.isEnabled = false
        self.progressIndicator.startAnimation(nil)
        self.labelStatusNetwork.stringValue = "Network in training"
        self.labelStatusNetwork.textColor = NSColor(calibratedRed: 0.7, green: 0.7, blue: 0.0, alpha: 1)
    }
    func setStatusTrained(){
        self.progressIndicator.stopAnimation(nil)
        self.progressIndicator.isHidden = true
        self.doneButton.isEnabled = true
        self.labelStatusNetwork.stringValue = "Network trained!"
        self.labelStatusNetwork.textColor = NSColor.myDarkGreen
    }
}
