//
//  PostController.swift
//  PostKit
//
//  Created by Alessandro Ferrazza on 10/03/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//



@objc public protocol PostControllerDelegate{
    @objc optional func postControllerUpdatedList() //this is a method called when a list will be updated
}

public class PostController: NSObject {
    
    public static let postControllerAddDownloadNotificationKey = "postControllerAddDownloadNotificationKey"
    public static let postControllerAddUploadNotificationKey = "postControllerAddUploadNotificationKey"
    public static let postControllerRemoveDownloadNotificationKey = "postControllerRemoveDownloadNotificationKey"
    public static let postControllerRemoveUploadNotificationKey = "postControllerRemoveUploadNotificationKey"
    
    public static let shared = PostController()
    
    var innerDownloadPostList = [PostObject]()
    var innerUploadPostList = [PostObject]()

    func add(downloadPost post:PostObject){
        self.innerDownloadPostList.append(post)
        self.postNotificationWithName(name: PostController.postControllerAddDownloadNotificationKey, index: nil)
    }
    
    func add(uploadPost post:PostObject){
        self.innerUploadPostList.append(post)
        self.postNotificationWithName(name: PostController.postControllerAddUploadNotificationKey, index: nil)

    }
    
    func remove(downloadPost post:PostObject){
        let index = self.downloadPostList.firstIndex { (postObject) -> Bool in
            //In this case the download correspond to a specific url so the postObject will be identified with url
            return post.requestGenerator.url == postObject.requestGenerator.url
        }
        if let i = index{
            self.innerDownloadPostList.remove(at: i)
            self.postNotificationWithName(name: PostController.postControllerRemoveDownloadNotificationKey, index: i)
        }
        
    }
    
    func remove(uploadPost post:PostObject){
        let index = self.uploadPostList.firstIndex { (postObject) -> Bool in
            //In this case the url is the same because is the php for the upload and the difference between the upload will be the filename
            if let postName = post.requestGenerator.uploadFileName, let postObjectName = postObject.requestGenerator.uploadFileName{
                return postName == postObjectName
            }
            return false
        }
        if let i = index{
            self.innerUploadPostList.remove(at: i)
            self.postNotificationWithName(name: PostController.postControllerRemoveUploadNotificationKey, index: i)
        }
    }

    public var downloadPostList:[PostObject]{
        get{
            return self.innerDownloadPostList
        }
    }
    public var uploadPostList:[PostObject]{
        get{
            return self.innerUploadPostList
        }
    }
    
    func postNotificationWithName(name: String, index:Int?){
        //this will be sendd for every insert and remove so that any table can be updated on the basis of the notifications
        if let i = index{
            NotificationCenter.default.post(name: NSNotification.Name(name), object: nil, userInfo: ["index": i])
        }else{
            NotificationCenter.default.post(name: NSNotification.Name(name), object: nil)
        }
    }
    
    //helper observer
    public func addObserverForAddUpload(observer: Any?,
                                        using block: @escaping (Notification) -> Swift.Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: PostController.postControllerAddUploadNotificationKey), object: nil, queue: nil, using: block)
        
    }
    public func addObserverForRemoveUpload(observer: Any?,
                                             using block: @escaping (Notification) -> Swift.Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: PostController.postControllerRemoveUploadNotificationKey), object: nil, queue: nil, using: block)
        
    }

    public func addObserverForAddDownload(observer: Any?,
                                          using block: @escaping (Notification) -> Swift.Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: PostController.postControllerAddDownloadNotificationKey), object: nil, queue: nil, using: block)

    }
    public func addObserverForRemoveDownload(observer: Any?,
                                          using block: @escaping (Notification) -> Swift.Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: PostController.postControllerRemoveDownloadNotificationKey), object: nil, queue: nil, using: block)
        
    }
    
    
    
}
