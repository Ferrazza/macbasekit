//  Converted with Swiftify v1.0.6274 - https://objectivec2swift.com/
//
//  PostObject.swift
//  MyNotification
//
//  Created by Alessandro Ferrazza on 11/3/13.
//  Copyright (c) 2013 Ferrazza. All rights reserved.
//

@objc public protocol PostObjectDelegate{
    @objc optional func postObject(with postObject:PostObject, connectionDidFinishLoading:Data)
    @objc optional func postObject(with postObject:PostObject, percentDownload:Float)
    @objc optional func postObject(with postObject:PostObject, percentUpload:Float)
    @objc optional func postObject(with postObject:PostObject, connectionDidFail:Error)
}


typealias CompletionBlock = (Data?, URLResponse?, Error?) -> Swift.Void
typealias PercentProgressionBlock = (Float) -> Void

open class PostObject: NSObject{
    
    //var didFinishDownloading = false //this is because methods didFinishDownloadingTo and didCompleteWithError are both called and if called didFinishDownloadingTo this flag is used to avoid that completion will be called in didCompleteWithError //Not more needed the error was that in didCompleteWithError the completion was called also if error was nil
   
    public var delegate:PostObjectDelegate?
    var requestGenerator:RequestGenerator
    
    var completionBlock:CompletionBlock?
    var percentDownloadBlock:PercentProgressionBlock?
    var percentUploadBlock:PercentProgressionBlock?
    
    var session:URLSession?
    var downloadTask:URLSessionDownloadTask?
    var downloadResumeData:Data?
    var uploadTask:URLSessionDataTask?
    public var isUploading = false
    public var isDownloading = false
    public var url:URL!

    public init(with url: URL){
        self.requestGenerator = RequestGenerator(with: url)
        self.url = url
        
    }

    public func addHeaderToRequest(withValue value: String, forKey key: String) {
        self.requestGenerator.headerValues[key] = value
    }
    
    
    public func cancelDownload(){
        if self.isDownloading{
            self.isDownloading = false
            self.downloadTask?.cancel()
            self.downloadTask = nil
            PostController.shared.remove(downloadPost: self)
        }
    }
    
    public func pauseDownload(){
        if self.isDownloading{
            self.downloadTask?.cancel(byProducingResumeData: { [weak self](data) in
                self?.isDownloading = false
                if data != nil {
                    self?.downloadResumeData = data
                }
            })
        }
    }
    public func resumeDownload(){
        if self.isDownloading == false{
            if let resumeData = self.downloadResumeData {
                self.downloadTask = self.session?.downloadTask(withResumeData: resumeData)
                self.downloadTask?.resume()
                self.isDownloading = true
            }
        }
    }
    
    public func cancelUpload(){
        if self.isUploading{
            self.isUploading = false
            self.uploadTask?.cancel()
            self.uploadTask = nil
        }
    }
    
    public func listOfUpload(){
    }
    //MARK: Send Request
    
    public func sendAsynchronousRequest(withCompletionBlock completion:@escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        let request = self.requestGenerator.generateRequest()
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask:URLSessionDataTask = self.session!.dataTask(with: request, completionHandler: completion)
        dataTask.resume()
    }
    
    public func sendAsynchronousRequest() {
        sendAsynchronousRequest { (data, response, error) in //do nothing
        }
    }
    
    //MARK: Post
    
    
    public func sendAsynchronous(post: String,
                                 withCompletionBlock completion:@escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        let request = self.requestGenerator.generateRequest(post: post)
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask:URLSessionDataTask = self.session!.dataTask(with: request, completionHandler: completion)
        dataTask.resume()
    }
    
    public func sendAsynchronous(post: String) {
        self.sendAsynchronous(post: post) { (data, response, error) in //do nothing
        }
    }

    public func sendAsynchronousPost(withDict dict: [String: String]) {
        self.sendAsynchronous(post: PostObject.post(fromDict: dict))
    }
    
    public func sendAsynchronousPost(withDict dict: [String: String], withCompletionBlock completion:@escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        self.sendAsynchronous(post: PostObject.post(fromDict: dict), withCompletionBlock: completion)
    }
    
    //MARK: Get
    public func sendAsynchronous(get: String,
                                 withCompletionBlock completion:@escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        let request = self.requestGenerator.generateRequest(get: get)
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask:URLSessionDataTask = self.session!.dataTask(with: request, completionHandler: completion)
        dataTask.resume()
    }
    public func sendAsynchronous(get: String) {
        self.sendAsynchronous(get: get) { (data, response, error) in //do nothing
        }
    }
    
    public func sendAsynchronousGet(withDict dict: [String: String]) {
        self.sendAsynchronous(get: PostObject.post(fromDict: dict))
    }
    
    public func sendAsynchronousGet(withDict dict: [String: String],
                                    withCompletionBlock completion:@escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        self.sendAsynchronous(get: PostObject.post(fromDict: dict), withCompletionBlock: completion)
    }
    
    
    //MARK: Upload Data
    var uploadCompletionBlockCalledInDidReceiveData:Bool?//flag to know if completion in upload has be called in DidReceiveData
    public func uploadAsynchronous(data fileData:Data, withFileName fileName: String, dictPost: [String:String],
                                   percentUploadBlock: @escaping (Float) -> Void,
                                   withCompletionBlock completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = self.requestGenerator.generateRequest(for: fileData, withFileName: fileName, dictPost: dictPost)
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        self.uploadTask = self.session?.uploadTask(with: request, from: request.httpBody!)
        self.percentUploadBlock = percentUploadBlock
        self.completionBlock = completion
        self.uploadCompletionBlockCalledInDidReceiveData = false
        self.uploadTask?.resume()
        PostController.shared.add(uploadPost: self)

        
    }
    
    public func uploadAsynchronous(data fileData: Data, withFileName fileName: String, dictPost: [String:String],
                                   withCompletionBlock completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        self.uploadAsynchronous(data: fileData, withFileName: fileName, dictPost: dictPost,
                                percentUploadBlock: { (Float) in
                                    //do nothing
        }, withCompletionBlock: completion)
    }
    
    //MARK: Download Data
    public func downloadAsynchronous(percentDownloadBlock: @escaping (Float) -> Void,
                                     withCompletionBlock completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = self.requestGenerator.generateRequest()
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        self.downloadTask = self.session?.downloadTask(with: request)
        self.percentDownloadBlock = percentDownloadBlock
        self.completionBlock = completion
        self.isDownloading = true
        self.downloadTask?.resume()
        PostController.shared.add(downloadPost: self)

    }

    public func downloadAsynchronous(withCompletionBlock completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.downloadAsynchronous(percentDownloadBlock: { (Float) in
            //do nothing
        }, withCompletionBlock: completion)
    }
    
    //------------------------------------
    //------------------------------------
    //------------------------------------

    
    // MARK: Helpers
    
    public class func logData(_ data: Data) {
        if let string = String(bytes: data, encoding: String.Encoding.utf8){
            print("Post Object data: \(string)")
        }else{
            print("Post Object data: Data cannot be converted to string)")
        }
    }
    
    public class func stringifyData(_ data: Data) -> String {
        if let result = String(bytes: data, encoding: String.Encoding.utf8){
            return result
        }
        return "Error in stringifing data in PostObject"
    }
    
    class func post(fromDict dict: [String: String]) -> String {
        var post = String()
        for (i, key) in dict.keys.enumerated(){
            if let value = dict[key]{
                post += "\(key)=\(value)"
                if i < dict.keys.count - 1 {
                    //tutte tranne l'ultima
                    post += "&"
                }
            }
           
        }
        return post
    }
    
    class func urlEncode(string: String) -> String? {
        let result = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return result
    }
    
    
   
}


//------------------------------------
//MARK: DELEGATION EXTENSIONS----------------
//------------------------------------

extension PostObject:URLSessionDelegate{
    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        //print("\(self.description) -> urlSession didBecomeInvalidWithError")

        if error != nil{

            self.completionBlock?(nil, nil, error)
            self.delegate?.postObject?(with: self, connectionDidFail: error!)
            self.downloadTask = nil
            self.uploadTask = nil
            PostController.shared.remove(uploadPost: self)
            PostController.shared.remove(downloadPost: self)
        }
    }
}
extension PostObject: URLSessionDataDelegate{
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        //called in upload task only if data will be received else it is not called
        //print("\(self.description) -> urlSession dataTask didReceive data")

        self.isUploading = false
        self.uploadTask = nil

        self.completionBlock?(data, nil, nil)
        self.delegate?.postObject?(with: self, connectionDidFinishLoading: data)
        PostController.shared.remove(uploadPost: self)
        
        if let called = self.uploadCompletionBlockCalledInDidReceiveData{
            //if uploadCompletionBlockCalledInDidReceiveData is an upload task and setting it true avoid to call completion two time (e.g. here and in urlSession task didCompleteWithError) -- see below
            if called == false{
                self.uploadCompletionBlockCalledInDidReceiveData = true
            }
        }


    }
}


extension PostObject: URLSessionDownloadDelegate{

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        //called in download
        //print("\(self.description) -> urlSession downloadTask didFinishDownloadingTo")

        if let response = downloadTask.response as? HTTPURLResponse{
            //print("urlSession downloadTask didFinishDownloadingTo")

            switch response.statusCode {
            case 200:
                var data:Data?
                do{
                    data = try Data(contentsOf: location)
                    
                }catch{
                    print("error in retrieving data")
                }
                self.isDownloading = false
                self.downloadTask = nil
                self.completionBlock?(data, nil, nil)
                if let d = data{
                    self.delegate?.postObject?(with: self, connectionDidFinishLoading: d)
                }
                PostController.shared.remove(downloadPost: self)

            default:
                print("Error in downloading file for download task with id: \(downloadTask.taskIdentifier)\n - Response status code: \(response.statusCode)")
                print(self.url ?? "no url")

                self.completionBlock?(nil, response, nil)
            }
            
        }
        
        
    }
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        let percentDownload = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite) * 100
        //print("percent download: \(percentDownload)%")
        
        self.delegate?.postObject?(with: self, percentDownload:percentDownload)
        self.percentDownloadBlock?(percentDownload)
    }

}
extension PostObject: URLSessionTaskDelegate{
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        //Called in both download and upload
        //print("\(self.description) -> urlSession task didCompleteWithError")

        self.downloadTask = nil
        self.uploadTask = nil
        PostController.shared.remove(uploadPost: self)
        PostController.shared.remove(downloadPost: self)
        if error != nil{//the completion is called only if an error is present
            self.completionBlock?(nil, nil, error)
            self.delegate?.postObject?(with: self, connectionDidFail: error!)
        }else{//and when it is not called in other methods
            //in upload is the only method called if no data response is present otherwise the method urlSession dataTask didReceive data is called
            //so must call the completion in this case
            if let called = self.uploadCompletionBlockCalledInDidReceiveData{
                if called == false{
                    self.completionBlock?(nil, nil, error)
                    self.uploadCompletionBlockCalledInDidReceiveData = nil
                }
            }
        }

    }
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let percentUpload = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        //print("percent upload: \(percentUpload)")
        
        self.delegate?.postObject?(with: self, percentUpload: percentUpload)
        self.percentUploadBlock?(percentUpload)
    }
}
