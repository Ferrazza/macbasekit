//
//  CustomSegue.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 31/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import AppKit

// Simple enum for transition type.
public enum TransitionType {
    case present, dismiss
}

// Protocol that view controllers can implement to receive notification of transition.
// This could be used to change controller behaviours.
public protocol TransitionAnimatorNotifiable {
    
    // Notify the transition completion
    func notifyTransitionCompletion(_ transition: TransitionType)
}

// An animator to present view controller using NSViewControllerTransitionOptions
open class TransitionAnimator: NSObject, NSViewControllerPresentationAnimator {
    
    // Duration of animation (default: 0.3)
    open var duration: TimeInterval
    // Animation options for view transitions
    open var transition: NSViewController.TransitionOptions
    // Background color used on destination controller if not already defined
    open var backgroundColor = NSColor.windowBackgroundColor
    // If false, destination controller take the size of the source controller
    // If true, when sliding the destination controller keep one of its size element.(ex: for slide down and up, the height is kept)
    // (default: false)
    open var keepOriginalSize = false
    // Remove view of fromViewController from view hierarchy. Best use with crossfade effect.
    open var removeFromView = false
    // Optional origin point for displayed view
    open var origin: NSPoint? = nil {
        didSet {
            assert(keepOriginalSize)
        }
    }
    
    fileprivate var fromView: NSView? = nil
    
    // Init
    public init(duration: TimeInterval =  0.3, transition: NSViewController.TransitionOptions = [NSViewController.TransitionOptions.crossfade, NSViewController.TransitionOptions.slideDown]) {
        self.duration = duration
        self.transition = transition
    }
    
    // MARK: NSViewControllerPresentationAnimator
    
    
    @objc open func animatePresentation(of viewController: NSViewController, from fromViewController: NSViewController) {
        let fromFrame = fromViewController.view.frame
        
        let originalFrame = viewController.view.frame
        let startFrame = transition.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
        var destinationFrame = transition.slideStopFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
        
        if let origin = self.origin {
            destinationFrame.origin = origin
        }
        
        viewController.view.frame = startFrame
        viewController.view.autoresizingMask = [NSView.AutoresizingMask.width, NSView.AutoresizingMask.height]
        
        if transition.contains(NSViewController.TransitionOptions.crossfade) {
            viewController.view.alphaValue = 0
        }
        
        if !viewController.view.wantsLayer { // remove potential transparency
            viewController.view.wantsLayer = true
            viewController.view.layer?.backgroundColor = backgroundColor.cgColor
            viewController.view.layer?.isOpaque = true
        }
        // maybe create an intermediate container view to remove from controller view from hierarchy
        if removeFromView {
            fromView = fromViewController.view
            fromViewController.view = NSView(frame: fromViewController.view.frame)
            fromViewController.view.addSubview(fromView!)
        }
        fromViewController.view.addSubview(viewController.view)
        
        NSAnimationContext.runAnimationGroup(
            { [unowned self] context in
                context.duration = self.duration
                context.timingFunction =  CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
                
                viewController.view.animator().frame = destinationFrame
                if self.transition.contains(NSViewController.TransitionOptions.crossfade) {
                    viewController.view.animator().alphaValue = 1
                    self.fromView?.animator().alphaValue = 0
                }
                
            }, completionHandler: { [unowned self] in
                if self.removeFromView {
                    self.fromView?.removeFromSuperview()
                }
                if let src = viewController as? TransitionAnimatorNotifiable {
                    src.notifyTransitionCompletion(.present)
                }
                if let dst = viewController as? TransitionAnimatorNotifiable {
                    dst.notifyTransitionCompletion(.present)
                }
        })
    }
    
    @objc open func animateDismissal(of viewController: NSViewController, from fromViewController: NSViewController) {
        let fromFrame = fromViewController.view.frame
        let originalFrame = viewController.view.frame
        let destinationFrame = transition.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
        
        if self.removeFromView {
            fromViewController.view.addSubview(self.fromView!)
        }
        
        NSAnimationContext.runAnimationGroup(
            { [unowned self] context in
                context.duration = self.duration
                context.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
                
                viewController.view.animator().frame = destinationFrame
                if self.transition.contains(NSViewController.TransitionOptions.crossfade) {
                    viewController.view.animator().alphaValue = 0
                    self.fromView?.animator().alphaValue = 1
                }
                
            }, completionHandler: {
                viewController.view.removeFromSuperview()
                if self.removeFromView {
                    if let view = self.fromView {
                        fromViewController.view = view
                    }
                }
                
                if let src = viewController as? TransitionAnimatorNotifiable {
                    src.notifyTransitionCompletion(.dismiss)
                }
                if let dst = viewController as? TransitionAnimatorNotifiable {
                    dst.notifyTransitionCompletion(.dismiss)
                }
        })
    }
}


// MARK: NSViewControllerTransitionOptions
extension NSViewController.TransitionOptions {
    
    func slideStartFrame(fromFrame: NSRect, keepOriginalSize: Bool, originalFrame: NSRect) -> NSRect {
        if self.contains(NSViewController.TransitionOptions.slideLeft) {
            let width = keepOriginalSize ? originalFrame.width : fromFrame.width
            return NSRect(x: fromFrame.width, y: 0, width: width, height: fromFrame.height)
        }
        if self.contains(NSViewController.TransitionOptions.slideRight) {
            let width = keepOriginalSize ? originalFrame.width : fromFrame.width
            return NSRect(x: -width, y: 0, width: width, height: fromFrame.height)
        }
        if self.contains(NSViewController.TransitionOptions.slideDown) {
            let height = keepOriginalSize ? originalFrame.height : fromFrame.height
            return NSRect(x: 0, y: fromFrame.height, width: fromFrame.width, height: height)
        }
        if self.contains(NSViewController.TransitionOptions.slideUp) {
            let height = keepOriginalSize ? originalFrame.height : fromFrame.height
            return NSRect(x: 0, y: -height, width: fromFrame.width, height: height)
        }
        if self.contains(NSViewController.TransitionOptions.slideForward) {
            switch NSApp.userInterfaceLayoutDirection {
            case .leftToRight:
                return NSViewController.TransitionOptions.slideLeft.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            case .rightToLeft:
                return NSViewController.TransitionOptions.slideRight.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            @unknown default:
                //do nothing
                break
            }
        }
        if self.contains(NSViewController.TransitionOptions.slideBackward) {
            switch NSApp.userInterfaceLayoutDirection {
            case .leftToRight:
                return NSViewController.TransitionOptions.slideRight.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            case .rightToLeft:
                return NSViewController.TransitionOptions.slideLeft.slideStartFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            @unknown default:
                //do nothing
                break

            }
        }
        return fromFrame
    }
    
    func slideStopFrame(fromFrame: NSRect, keepOriginalSize: Bool, originalFrame: NSRect) -> NSRect {
        if !keepOriginalSize {
            return fromFrame
        }
        if self.contains(NSViewController.TransitionOptions.slideLeft) {
            return NSRect(x: fromFrame.width - originalFrame.width , y: 0, width: originalFrame.width , height: fromFrame.height)
        }
        if self.contains(NSViewController.TransitionOptions.slideRight) {
            return NSRect(x: 0, y: 0, width: originalFrame.width , height: fromFrame.height)
        }
        if self.contains(NSViewController.TransitionOptions.slideUp) {
            return NSRect(x: 0, y: 0, width: fromFrame.width, height: originalFrame.height )
        }
        if self.contains(NSViewController.TransitionOptions.slideDown) {
            return NSRect(x: 0, y: fromFrame.height - originalFrame.height , width: fromFrame.width, height: originalFrame.height)
        }
        if self.contains(NSViewController.TransitionOptions.slideForward) {
            switch NSApp.userInterfaceLayoutDirection {
            case .leftToRight:
                return NSViewController.TransitionOptions.slideLeft.slideStopFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            case .rightToLeft:
                return NSViewController.TransitionOptions.slideRight.slideStopFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            @unknown default:
                //do nothing
                break
            }
        }
        if self.contains(NSViewController.TransitionOptions.slideBackward) {
            switch NSApp.userInterfaceLayoutDirection {
            case .leftToRight:
                return NSViewController.TransitionOptions.slideRight.slideStopFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            case .rightToLeft:
                return NSViewController.TransitionOptions.slideLeft.slideStopFrame(fromFrame: fromFrame, keepOriginalSize: keepOriginalSize, originalFrame: originalFrame)
            @unknown default:
                //do nothing
                break
            }
        }
        return fromFrame
    }
    
}

// A segue with custom animator.
// You can configure the animator in `prepareForSegue`
open class PresentWithAnimatorSegue: NSStoryboardSegue {
    
    // An animator used to present the view controller. (by default an TransitionAnimator)
    open var animator: NSViewControllerPresentationAnimator = TransitionAnimator()
    // Add destination controller as child to source controller
    open var addAsChild = false
    
    override open func perform() {
        guard let fromController = self.sourceController as? NSViewController,
            let toController = self.destinationController as? NSViewController
            else { return }
        
        if toController.parent == nil && addAsChild {
            fromController.addChild(toController)
        }
        fromController.present(toController, animator: animator)
    }
    
}

// MARK: Utility class with transition type configured
// Slide down segue
open class SlideDownSegue: PresentWithAnimatorSegue {
    
    override init(identifier: NSStoryboardSegue.Identifier, source sourceController: Any, destination destinationController: Any) {
        super.init(identifier: identifier, source: sourceController, destination: destinationController)
        (animator as? TransitionAnimator)?.transition = [NSViewController.TransitionOptions.slideDown, NSViewController.TransitionOptions.crossfade]
    }
    
}

// Slide up segue
open class SlideUpSegue: PresentWithAnimatorSegue {
    
    override init(identifier: NSStoryboardSegue.Identifier, source sourceController: Any, destination destinationController: Any) {
        super.init(identifier: identifier, source: sourceController, destination: destinationController)
        (animator as? TransitionAnimator)?.transition = [NSViewController.TransitionOptions.slideUp, NSViewController.TransitionOptions.crossfade]
    }
    
}

// Slide left segue
open class SlideLeftSegue: PresentWithAnimatorSegue {
    
    override init(identifier: NSStoryboardSegue.Identifier, source sourceController: Any, destination destinationController: Any) {
        super.init(identifier: identifier, source: sourceController, destination: destinationController)
        (animator as? TransitionAnimator)?.transition = [NSViewController.TransitionOptions.slideLeft, NSViewController.TransitionOptions.crossfade]
    }
    
}

// Slide right segue
open class SlideRightSegue: PresentWithAnimatorSegue {
    
    override init(identifier: NSStoryboardSegue.Identifier, source sourceController: Any, destination destinationController: Any) {
        super.init(identifier: identifier, source: sourceController, destination: destinationController)
        (animator as? TransitionAnimator)?.transition = [NSViewController.TransitionOptions.slideRight, NSViewController.TransitionOptions.crossfade]
    }
    
}

// Crossfade segue
open class CrossfadeSegue: PresentWithAnimatorSegue {
    
    override init(identifier: NSStoryboardSegue.Identifier, source sourceController: Any, destination destinationController: Any) {
        super.init(identifier: identifier, source: sourceController, destination: destinationController)
        (animator as? TransitionAnimator)?.transition = NSViewController.TransitionOptions.crossfade
    }
    
}
