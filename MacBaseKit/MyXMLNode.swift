//
//  File.swift
//  XmlKit
//
//  Created by Alessandro Ferrazza on 05/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class MyXMLNode{
    public var name:String
    public var attributes = [String:String]()
    public var childLevel:Int = 0
    public var parent:MyXMLNode?
    
    public init(name:String) {
        self.name = name
    }
    
    public func setAttributes(with attrubutes:[String:String]){
        for (key, value) in attrubutes{
            let correctedValue = MyXMLNode.reformatNFormat(for: value)
            let correctedKey = key.replacingOccurrences(of: " ", with: "_")
            self.attributes[correctedKey] = correctedValue
        }
    }
    
    public func set(value:String?, for attribute:String) ->Bool{
        if let v = value{
            let correctedValue = MyXMLNode.reformatNFormat(for: v)
            let correctedKey = attribute.replacingOccurrences(of: " ", with: "_")
            self.attributes[correctedKey] = correctedValue
            return true
        }
        return false
    }
    public func removeAllAttributes(){
        self.attributes.removeAll()
    }
    static func stringByRemovingXMLNotPermittedCharacters(string:String) ->String{
        var correctedValue = string
        if MyXMLNode.checkforXMLNotPermittedCharacters(string: string){
            correctedValue = correctedValue.replacingOccurrences(of: "&", with: "&amp;")
            correctedValue = correctedValue.replacingOccurrences(of: ">", with: "&gt;")
            correctedValue = correctedValue.replacingOccurrences(of: "<", with: "&lt;")
            correctedValue = correctedValue.replacingOccurrences(of: "\"", with: "&quot;")
            correctedValue = correctedValue.replacingOccurrences(of: "\'", with: "&apos;")
            
        }
        
        return correctedValue
    }
    
    static func checkforXMLNotPermittedCharacters(string:String) -> Bool{
        let arrayInvalidChar = ["<", ">", "&", "<", "\"", "\'"]
        for s in arrayInvalidChar{
            if (string.range(of: s) != nil){
                return true
            }
        }
        return false
    }
    
    static public func cleanedStringFromXML(string:String?)->String?{
        var result:String?
        if let s = string{
            do{
                let s1 = s.replacingOccurrences(of: "\n", with: "")
                let lenght = s1.count
                let regex = try NSRegularExpression(pattern: "  +", options: .caseInsensitive)
                result = regex.stringByReplacingMatches(in: s1, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, lenght), withTemplate: " ")
            }catch{
                print("Error in cleaning XML: see MyXMLNode")
            }
            
        }
        return result
    }
    
    public static func reformatNFormat(for xml:String) -> String{
        return xml.replacingOccurrences(of: "\n", with: "\\n")
    }
    
    
    ///This is to convert string from xml to linebreak
    ///ATTENTION: this work only if the attributes are added to XMLElement with functions setAttributes() or addAttributes
    public static func reformatNFormat(from xml:String) -> String{
        return xml.replacingOccurrences(of: "\\n", with: "\n")
    }
}
