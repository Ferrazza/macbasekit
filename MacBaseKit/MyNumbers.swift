//
//  MyNumbers.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 21/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation
extension Float {
    public func format(f: Int) -> String {
        let fString = "\(f)"
        return String(format: "%.0\(fString)f", self)
    }
}
extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
