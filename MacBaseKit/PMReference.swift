//
//  PMReference.swift
//  PubMedKit
//
//  Created by Alessandro Ferrazza on 07/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class PMReference{
    public var PMID:String
    public var title:String
    public var authors:String?
    public var journal:String?
    public var date:String?
    public var referenceDescription:String{
        get{
            var result = self.title
            if let authors = self.authors{
                result.append("\n\(authors)")
            }
            if let journal = self.journal{
                result.append("\n\(journal)")
            }
            return result
        }
    }
    public var referencAttributedDescription:NSAttributedString?{
        get{
            let boldFont = NSFont.boldSystemFont(ofSize: NSFont.systemFontSize)
            let attrBold = [NSAttributedString.Key.font : boldFont, NSAttributedString.Key.foregroundColor : NSColor.black]
            let italicFont = NSFont.systemFont(ofSize: NSFont.systemFontSize)
            let attrItalic = [NSAttributedString.Key.font : italicFont, NSAttributedString.Key.foregroundColor : NSColor.gray]
            

            let result = NSMutableAttributedString(string: self.title,
                                                   attributes: attrBold)
            if let authors = self.authors{
                result.append(NSAttributedString(string: "\n\(authors)",
                    attributes: attrItalic))
            }
            if let journal = self.journal{
                result.append(NSAttributedString(string: "\n\(journal)",
                    attributes: attrItalic))
            }
            return result
        }
    }
    
    public init(PMID:String, summary:[String : String]) {
        self.PMID = PMID
        self.title = PMReference.getTitle(from: summary)
        self.date = PMReference.getDate(from: summary)
        self.authors = PMReference.getAuthors(from: summary)
        self.journal = PMReference.getJournal(from: summary)
    }
    
    public init(attributedDict: [String : String]) {
        self.PMID = attributedDict["PMID"]!
        self.title = attributedDict["title"]!
        if let date = attributedDict["date"]{
            self.date = date
        }
        if let authors = attributedDict["authors"]{
            self.authors = authors
        }
        if let journal = attributedDict["journal"]{
            self.journal = journal
        }

    }
    
    static func getTitle(from summaryDict:[String : Any])->String{
        var result = "No title"
        if let r = summaryDict["title"] as? String{
            result = r
        }
        return result
    }
    static func getDate(from summaryDict:[String : Any])->String{
        var result = ""
        if let r = summaryDict["pubdate"] as? String{
            result = r
        }
        return result
    }

    static func getAuthors(from summaryDict:[String : Any])->String?{
        var result: String?
        if let authors = summaryDict["authors"] as? [[String:String]]{
            result = ""
            for index in 0..<(authors.count){
                let author = authors[index]
                if let name = author["name"]{
                    if index < authors.count - 1 && index <= 10{
                        result!.append("\(name), ")
                    }else{
                        result!.append("\(name).")
                        break
                    }
                }
            }
        }
        return result
    }
    
    static func getJournal(from summaryDict:[String : Any])->String?{
        var result: String?
        if let fullJournalName = summaryDict["fulljournalname"] as? String{
            result = ""
            result!.append("\(fullJournalName). ")
        }
        if let pages = summaryDict["pages"] as? String{
            if result == nil{
                result = ""
            }
            if pages.count > 0{
                result!.append("\(PMReference.getDate(from: summaryDict)); \(pages). ")
            }else{
                result!.append("\(PMReference.getDate(from: summaryDict)). ")
            }
        }
        return result
    }
    
    static func getAuthorsAndJournal(from summaryDict:[String : Any])->String?{
        var result: String?
        
        if let authors = PMReference.getAuthors(from: summaryDict){
            result = authors
        }
        if let jName = PMReference.getJournal(from: summaryDict){
            if result != nil{
                result?.append("\n\(jName)")
            }else{
                result = jName
            }
        }
        return result
    }

}
