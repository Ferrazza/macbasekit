//
//  MyImage.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 10/07/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation
import CoreImage

extension NSImage{
    
    public func blurredImage(withRadius: CGFloat) -> NSImage {
        /*
        let context = CIContext(options: nil)

        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(ima) CIImage(image: self)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(withRadius, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage
 */
        print("to implement blurredImage")
        return self
    }
    
    public func PNGRepresentation()->Data?{
        var result:Data?
        if let dataTiff = self.tiffRepresentation{
            let bitmap = NSBitmapImageRep(data: dataTiff)
            result = bitmap?.representation(using: NSBitmapImageRep.FileType.png, properties: [:])
        }
        return result
    }
    /*
    public func image(withLevel level:Int, window:Int) -> NSImage?{
        
        if let data = self.tiffRepresentation{
            if let bitRep = NSBitmapImageRep(data: data){
                let newImage = NSImage(size: CGSize(width: bitRep.pixelsWide , height: bitRep.pixelsHigh))
                let rep = NSBitmapImageRep(bitmapDataPlanes: nil,
                                           pixelsWide: bitRep.pixelsWide,
                                           pixelsHigh: bitRep.pixelsHigh,
                                           bitsPerSample: 8,
                                           samplesPerPixel: 4,
                                           hasAlpha: true,
                                           isPlanar: false,
                                           colorSpaceName: NSColorSpaceName.deviceRGB,
                                           bytesPerRow: 0,
                                           bitsPerPixel: 0)
                
                newImage.addRepresentation(rep!)
                newImage.lockFocus()
                
                
                for x in 0...bitRep.pixelsWide {
                    for y in 0...bitRep.pixelsHigh {
                        /*
                        if let color = bitRep.colorAt(x: x, y: y){
                            if let components = color.cgColor.components{
                                var newColor = NSColor.red
                                let white = components[0]
                                if white > 0.2{
                                    newColor = NSColor.white
                                }
                                
                                rep?.setColor(newColor, atX: x, y: y) //Non cambia il colore
                            }
                            
                        }
 */
                        let newColor = NSColor.red
                        if let r = rep{
                            print(r.colorSpace.numberOfColorComponents)
                        }
                        print(newColor.colorSpace.numberOfColorComponents)
                        rep?.setColor(newColor, atX: x, y: y) //Non cambia il colore

                    }
                }
                newImage.unlockFocus()

                
                return newImage
            }
            
        }
        return nil
        
        /*
        if let data = self.PNGRepresentation(){
            let cfData = data as CFData
            if let source = CGImageSourceCreateWithData(cfData, nil){
                if let imageRef = CGImageSourceCreateImageAtIndex(source, 0, nil){
                    let width = imageRef.width
                    let height = imageRef.height
                    
                }
            }
            
            
            
        }
        */
        
            

    }
 */
}
