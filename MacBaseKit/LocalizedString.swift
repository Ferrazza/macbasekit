//
//  LocalizedString.swift
//  CalendarList
//
//  Created by Alessandro Ferrazza on 16/08/15.
//  Copyright (c) 2015 Alessandro Ferrazza. All rights reserved.
//

import Foundation
extension NSMutableAttributedString {
    
    //MARK: SEARCH
    public func addSearchPattern(forSearchText:String, withColor:NSColor){
        //TO ENABLE SEARCH
        //1. Put the search field in the toolbar in the window
        //2. connect it to an action in the window controller
        //3. make sure to set the class of the window controller to the subclass with the action
        //4. Implement the action
        do{
            let regex = try NSRegularExpression(pattern: forSearchText, options: NSRegularExpression.Options.caseInsensitive)
            let baseString = self.string
            for match in regex.matches(in: baseString,
                                       options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds,
                                       range: NSRange(location: 0, length: baseString.utf16.count)){
                                        self.addAttribute(NSAttributedString.Key.backgroundColor, value: withColor, range: match.range)
            }
            
        }catch{
            print("Error in regex for addSearchPattern method")
        }
    }
    //MARK: TAGGED STRING
    //Per utilizzare le tagged string bisogna:
    //1. Convertire una attributed string in string con tag con il metodo convertToTaggedString() -> salvarla nel file xml o dovunque si voglia (es. aggiunge ad un testo bold un tag <b> all'inizio e alla fine)
    //2. Riconvertire la tag string in una attributed string creandola con il metodo init(fromTaggedString:String, baseAttributes:[NSAttributedStringKey:Any]?) -> baseAttributes è opzionale e permette di decidere gli attributi di base da usare a cui vengono aggiunti quelli con i tag.
    //3. se voglio modificare i tag di base posso usare il metodo di NSMutableAttributedString addAttribute(forTag:String, key:NSAttributedStringKey, obj:Any):
    /*
    es. il seguente modifica il colore del testo evidenziato in bold (tag b) e il font di italicBold (tag j):
 
    NSMutableAttributedString.addAttribute(forTag: "b", key: NSAttributedStringKey.foregroundColor, obj: NSColor.blue)
    NSMutableAttributedString.addAttribute(forTag: "j", key: NSAttributedStringKey.font, obj: NSFont(name: "Menlo-Bold", size: font.pointSize) ?? font)
    */
    
    /*
    ------------------------------------------
    IMPORTANTE
    ------------------------------------------
     PER PERMETTERE AL NSTEXTFIELD DI ESSERE EDITATO CON ATTRIBUTED TEXT BISOGNA FARE:
     textField.allowsEditingTextAttributes = true
    ------------------------------------------
    */
    
    func taggedAttributesDict(for baseAttributes:[NSAttributedString.Key:Any]?) -> [String:[NSAttributedString.Key:Any]]{
        var font = NSFont.systemFont(ofSize: NSFont.systemFontSize)
        var attributes:[NSAttributedString.Key:Any] = [NSAttributedString.Key.font:font]
        if let a = baseAttributes{
            if let providedFont = a[NSAttributedString.Key.font] as? NSFont{
                font = providedFont
            }
            attributes = a
        }
        
        var italicAttributes = attributes
        italicAttributes[NSAttributedString.Key.font] = font.italic()
        italicAttributes[NSAttributedString.Key.foregroundColor] = NSColor.controlTextColor //bianco/nero ma che cambia in dark mode
        var dict = ["i":italicAttributes]
        
        var boldAttributes = attributes
        boldAttributes[NSAttributedString.Key.font] = font.bold()
        boldAttributes[NSAttributedString.Key.foregroundColor] = NSColor.controlTextColor //bianco/nero ma che cambia in dark mode
        dict["b"] = boldAttributes
        
        var italicBoldAttributes = attributes
        italicBoldAttributes[NSAttributedString.Key.font] = font.italicBold()
        italicBoldAttributes[NSAttributedString.Key.foregroundColor] = NSColor.controlTextColor //bianco/nero ma che cambia in dark mode
        dict["j"] = italicBoldAttributes

        for (key, addedAttributes) in NSMutableAttributedString.addedAttributes{
            if var attributes = dict[key]{
                attributes.update(other: addedAttributes)
                dict[key] = attributes
            }else{
                dict[key] = addedAttributes
            }
        }
        
        return dict
    }
    
    public convenience init(fromTaggedString:String, baseAttributes:[NSAttributedString.Key:Any]?){
        var string = fromTaggedString
        var tagValue = [String]()
        var valuesRanges = [(String, NSRange, NSRange?)]()
        let tag = "<.>"
        //1. find the tags and store them in valuesRanges
        do{
            let regex = try NSRegularExpression(pattern: tag, options: NSRegularExpression.Options.caseInsensitive)
            let matches = regex.matches(in: string,
                                      options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds,
                                      range: NSRange(location: 0, length: string.utf16.count))
            var first = true
            for match in matches{
                let range = match.range
                //ranges.append(range)
                var substring = string[match.range.lowerBound..<match.range.upperBound]
                substring = substring.replacingOccurrences(of: "<", with: "")
                substring = substring.replacingOccurrences(of: ">", with: "")
                tagValue.append(substring)
                if first{
                    let value:(String, NSRange, NSRange?) = (substring, range, nil)
                    valuesRanges.append(value)
                }else{
                    if let value = valuesRanges.last{
                        if value.0 == substring{
                            let newValue:(String, NSRange, NSRange?) = (value.0, value.1, range)
                            valuesRanges.removeLast()
                            valuesRanges.append(newValue)
                        }else{
                            let value:(String, NSRange, NSRange?) = (substring, range, nil)
                            valuesRanges.append(value)
                            first = true
                        }
                    }
                }
                if first{
                    first = false
                }else{
                    first = true
                }
            }
            
        }catch{
            print("Error in regex for addSearchPattern method")
        }
        
        //2. remove tags from string
        for i in (0..<valuesRanges.count).reversed(){
            let valueRange = valuesRanges[i]
            if let lastRange = valueRange.2{
                let firstRange = valueRange.1
                //remove tag
                let lastRangeIndex = string.index(string.startIndex, offsetBy: lastRange.lowerBound)..<string.index(string.startIndex, offsetBy: lastRange.upperBound)
                string.removeSubrange(lastRangeIndex)
                
                let firstRangeIndex = string.index(string.startIndex, offsetBy: firstRange.lowerBound)..<string.index(string.startIndex, offsetBy: firstRange.upperBound)
                string.removeSubrange(firstRangeIndex)
            }
        }
        
        //3. recalculate ranges and store the values in valuesIndexes (tag, rangeToModify)
        var valuesIndexes = [(String, NSRange)]()

        for i in (0..<valuesRanges.count){
            let valueRange = valuesRanges[i]
            if let lastRange = valueRange.2{
                let firstRange = valueRange.1
                let tag = valueRange.0
                let tagLenght = tag.count + 2
                
                //Append valueIndex
                let offsetFirst = tagLenght * (i*2)
                let offsetLast = tagLenght * (i*2 + 1)

                let finalRange:NSRange = NSRange((firstRange.lowerBound - offsetFirst)..<(lastRange.lowerBound - offsetLast ))
                let valueIndex:(String, NSRange) = (tag, finalRange)
                valuesIndexes.append(valueIndex)
            }
        }
        
       
        self.init(string: string, attributes: baseAttributes)

        for valueIndex in valuesIndexes{
            for (tag, attributes) in self.taggedAttributesDict(for: baseAttributes){
                if tag == valueIndex.0{
                    self.addAttributes(attributes, range: valueIndex.1)
                }
            }
            
        }
    }
}

extension NSAttributedString {
    static var addedAttributes = [String:[NSAttributedString.Key:Any]]()
    
    static public func addAttribute(forTag:String, key:NSAttributedString.Key, obj:Any){
        var attributes = NSAttributedString.addedAttributes[forTag]
        if attributes == nil{
            attributes = [key:obj]
        }else{
            attributes![key] = obj
        }
        NSAttributedString.addedAttributes[forTag] = attributes
    }
    
    public func convertToTaggedString()->String{
        var string = self.string
        self.enumerateAttributes(in: NSMakeRange(0, self.length), options: NSAttributedString.EnumerationOptions.reverse) { (dict, range, obj) in
            let boldTag = "<b>"
            let italicTag = "<i>"
            let italicBoldTag = "<j>"


            for (key, value) in dict{
                if key == NSAttributedString.Key.font{
                    if let font = value as? NSFont{
                        let finalPosition = range.length + range.location
                        let initialPosition = range.location
                        var tag = ""
                        if font.fontName.contains("Bold") || font.fontName.contains("Emphasized"){
                            tag = boldTag
                        }
                        if font.fontName.contains("Italic") || font.fontName.contains("Oblique"){
                            tag = italicTag
                        }
                        if (font.fontName.contains("Bold") || font.fontName.contains("Emphasized"))&&(font.fontName.contains("Italic") || font.fontName.contains("Oblique")){
                            tag = italicBoldTag
                        }
                        //if there are added tag the name can be different
                        for (addedTag, attributes) in NSAttributedString.addedAttributes{
                            if let addedFont = attributes[NSAttributedString.Key.font] as? NSFont{
                                if addedFont.fontName == font.fontName{
                                    tag = "<" + addedTag + ">"
                                }
                            }
                        }
                        //string.insert(contentsOf: tag, at: String.Index.init(encodedOffset: finalPosition))
                        //string.insert(contentsOf: tag, at: String.Index.init(encodedOffset: initialPosition))
                        string.insert(contentsOf: tag, at: String.Index(utf16Offset: finalPosition, in: string))
                        string.insert(contentsOf: tag, at: String.Index(utf16Offset: initialPosition, in: string))
                    }
                }
            }
        }
        return string
    }
    
    
}


extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    
    
    public var localized: String {
        return NSLocalizedString(self, comment: "")
        //return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    public var stringByDeletingPathExtension:String{
        return (self as NSString).deletingPathExtension
    }
    
    public var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    public var intValue: Int {
        return NSString(string: self).integerValue
    }
    public var floatValue: Float {
        return NSString(string: self).floatValue
    }
    public var doubleValue: Double {
        return NSString(string: self).doubleValue
    }
    
    public var stringByDeletingLastPathComponent: String{
        return  String(NSString(string: self).deletingLastPathComponent)
    }
    
    public func urlEncode()->String?{
        let escapedAddress = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        /*
        var result = String()
        let chars = self.characters.map{String($0)}
        for char in chars{
            if char == " "{
                result.append(Character("+"))
            }else if (char == "." || char == "-" || char == "_" || char == "~" ||
                (char >= "a" && char <= "z") ||
                (char >= "A" && char <= "Z") ||
                (char >= "0" && char <= "9")){
                result = result.stringByAppendingFormat("%c", char)
            }else{
                result = result.stringByAppendingFormat("%%%02X", char)
            }
            print(result)
        }
 */
        return escapedAddress
        
    }
    public func attributed()->NSAttributedString{
        let result = NSAttributedString (string: self,
                                         attributes: nil)
        return result
    }
    public func attributed(with color:NSColor?, font:NSFont?)->NSAttributedString{
        var attributes = [NSAttributedString.Key:Any]()
        if let c = color{
            attributes[NSAttributedString.Key.foregroundColor] = c
        }
        if let f = font{
            attributes[NSAttributedString.Key.font] = f
        }
        let result = NSAttributedString (string: self,
                           attributes: attributes)
        return result
    }
    public func mutableAttributed(with color:NSColor?, font:NSFont?)->NSMutableAttributedString{
        var attributes = [NSAttributedString.Key:Any]()
        if let c = color{
            attributes[NSAttributedString.Key.foregroundColor] = c
        }
        if let f = font{
            attributes[NSAttributedString.Key.font] = f
        }
        let result = NSMutableAttributedString(string: self,
                                         attributes: attributes)
        return result
    }
    //Permit to use int as subscriptions
    public subscript (r: CountableClosedRange<Int>) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[startIndex...endIndex])
    }
    /*
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[Range(start ..< end)])
    }
    */
    public subscript (bounds: CountableRange<Int>) -> String {
        let startIndex =  self.index(self.startIndex, offsetBy: bounds.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: bounds.upperBound - bounds.lowerBound)
        return String(self[startIndex..<endIndex])
    }
    
    public subscript (r: CountablePartialRangeFrom<Int>) -> String {
        let startIndex =  self.index(self.startIndex, offsetBy: r.lowerBound)
        return String(self[startIndex...])
    }
    
}








