//
//  ImageCollectionViewItem.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 22/10/2018.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

class ImageCollectionViewItem: NSCollectionViewItem {

    var image: NSImage? {
        didSet {
            guard isViewLoaded else { return }
            if let imageValue = image {
                self.imageView?.image = imageValue
            } else {
                imageView?.image = nil
            }
        }
    }
    
    // 2
    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.darkGray.cgColor
        
        view.layer?.borderColor = NSColor.white.cgColor
        view.layer?.borderWidth = 0.0
    }
    
    override var isSelected: Bool{
        didSet {
            view.layer?.borderWidth = isSelected ? 5.0 : 0.0
        }
    }
}
