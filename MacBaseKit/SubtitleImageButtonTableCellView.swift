//
//  SubtitleImageButtonTableCellView.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 26/11/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
public protocol SubtitleImageButtonTableCellViewProtocol {
    func subtitleImageButtonTableCellView(cell: SubtitleImageButtonTableCellView)
}
open class SubtitleImageButtonTableCellView: SubtitleImageTableCellView {
    open var delegate: SubtitleImageButtonTableCellViewProtocol?
    var buttonImage:NSButton?
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        if self.buttonImage == nil{
            self.buttonImage = NSButton(frame: CGRect.zero)
            self.setButtonFrame(withSuperviewSize:self.frame.size)
            self.addSubview(self.buttonImage!)
            self.buttonImage?.target = self
            self.buttonImage?.action = #selector(SubtitleImageButtonTableCellView.imageButtonClicked(sender:))
            self.buttonImage?.isTransparent = true
        }
    }

    open override func setFrameSize(_ newSize: NSSize) {
        super.setFrameSize(newSize)
        self.setButtonFrame(withSuperviewSize:newSize)
    }
    
    func setButtonFrame(withSuperviewSize size:CGSize){
        if let imageView = self.imageView{
            let inset:CGFloat = 2
            let frame = CGRect(x: inset, y: size.height - imageView.frame.size.height - inset, width: imageView.frame.size.width, height: imageView.frame.size.height)
            self.buttonImage?.frame = frame
        }
    }
    /*
    open override func layout() {
        super.layout()
        if let imageView = self.imageView{
            let inset:CGFloat = 2
            let frame = CGRect(x: inset, y: self.frame.size.height - inset, width: imageView.frame.size.width, height: imageView.frame.size.height)
            self.buttonImage?.frame = frame
        }
        
    }
 */
    @objc func imageButtonClicked(sender:NSButton){
        self.delegate?.subtitleImageButtonTableCellView(cell: self)
    }
   
    
    override open func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
