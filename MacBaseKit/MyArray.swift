//
//  MyArray.swift
//  Radiology
//
//  Created by Alessandro Ferrazza on 17/01/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import Foundation

extension Array {
    
    public func find(_ includedElement: (Element) -> Bool) -> Int? {
        for (idx, element) in self.enumerated() {
            if includedElement(element) {
                return idx
            }
        }
        return nil
    }
    
    public func arrayByRemovingAtIndexes(_ indexes:[Int])->[Element]{
        var newArray = [Element]()
        for (index, element) in self.enumerated(){
            if !indexes.contains(index) {
                newArray.append(element)
            }
        }
        return newArray
    }
    public mutating func moveObjectFromIndex(_ fromIndex:Int, toIndex:Int){
        let object = self[fromIndex]
        self.remove(at: fromIndex)
        self.insert(object, at: toIndex)
        
    }
    
    public mutating func removeAtIndexes(_ indexes:[Int])->[Element]{ //return the removed array
        var newArray = [Element]()
        var removedArray = [Element]()

        for (index, element) in self.enumerated(){
            if !indexes.contains(index) {
                newArray.append(element)
            }else{
                removedArray.append(element)
            }
        }
        self = newArray
        return removedArray
    }
    
    //this method mantain the order of given array a by adding the new order
    public func insertionSort<T>(_ a: [T], _ isOrderedBefore: (T, T) -> Bool) -> [T] {
        guard a.count > 1 else { return a }
        
        var b = a
        for i in 1..<b.count {
            var y = i
            let temp = b[y]
            while y > 0 && isOrderedBefore(temp, b[y-1]) {
                b[y] = b[y - 1]
                y -= 1
            }
            b[y] = temp
        }
        return b
    }
    public func contains(string:String) ->Bool{
        let result = self.contains(where: { (e) -> Bool in
            if let str = e as? String{
                if string == str{
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        })
        return result
    }
    public func containsAllValues(values:[String]) ->Bool{
        var result = true
        for value in values{
            let contain = self.contains(string: value)
            if contain == false{
                result = false
                break
            }
        }
        if values.count == 0{
            return false
        }
        return result
    }
    
    
}
