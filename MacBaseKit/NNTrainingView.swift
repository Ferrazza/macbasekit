//
//  NNTrainingView.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 21/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
/**
 This is a view that show the training error in a graph
 */
class NNTrainingView: NSView {

    var epochErrors = [Float]()
    var inset:CGFloat = 5
    var epochDate:Date = Date()
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        //Common values
        let textCommonAttrs = [
        NSAttributedString.Key.font: NSFont.systemFont(ofSize: 12),
        NSAttributedString.Key.foregroundColor: NSColor.myDarkBlue]
        
        //fill e stroke graph
        NSColor.white.setFill()
        dirtyRect.fill()
        let rectPath = NSBezierPath()
        rectPath.appendRect(dirtyRect)
        rectPath.lineWidth = 2
        NSColor.lightGray.setStroke()
        rectPath.stroke()
        
        //Axis
        let dirtyBounds = CGRect(origin: CGPoint.zero, size: dirtyRect.size)
        let graphRect = dirtyBounds.insetBy(dx: self.inset, dy: self.inset)
        let axisPath: NSBezierPath = NSBezierPath()
        axisPath.move(to: graphRect.origin)
        axisPath.line(to: CGPoint(x: graphRect.origin.x, y: graphRect.maxY))
        axisPath.move(to: graphRect.origin)
        axisPath.line(to: CGPoint(x: graphRect.maxX, y: graphRect.origin.y))
        axisPath.lineWidth = 1
        NSColor.black.setStroke()
        axisPath.stroke()
        
        //Threshold error line
        let lineRect = graphRect.insetBy(dx: self.inset, dy: self.inset)
        
        let minErrorPath: NSBezierPath = NSBezierPath()
        minErrorPath.move(to: CGPoint(x: graphRect.minX, y: lineRect.origin.y))
        minErrorPath.line(to: CGPoint(x: graphRect.maxX, y: lineRect.origin.y))
        
        
        //Is drawn only if it is not the last
        if self.epochErrors.count != (NNSettings.maxEpochs){
            //Threshold error line
            NSColor.myDarkGreen.setStroke()
            minErrorPath.lineWidth = 1
            minErrorPath.stroke()
            
            //Threshold error text
            let textErrorThreshold = NNSettings.errorThreshold.format(f: 3)
            let labelSize = textErrorThreshold.size(withAttributes:textCommonAttrs)
            let pointErrorText = CGPoint(x: lineRect.maxX - labelSize.width, y: lineRect.origin.y)
            textErrorThreshold.draw(at: pointErrorText, withAttributes: textCommonAttrs)
        }
        
        
        
        //Graph
        if self.epochErrors.count > 1{
            if let max = self.epochErrors.max(){
                let finalMin = NNSettings.errorThreshold
                //let finalMin = min
                
                let epochUnit = lineRect.size.width/CGFloat(NNSettings.maxEpochs - 1)
                
                let deltaError = CGFloat(max - finalMin)
                let conversion = lineRect.height/deltaError
                let linePath: NSBezierPath = NSBezierPath()
                let firstError = CGFloat(self.epochErrors[0])
                
                linePath.move(to: CGPoint(x: lineRect.origin.x, y: ((firstError - CGFloat(finalMin)) * conversion) + lineRect.origin.y))
                for i in 1...(self.epochErrors.count - 1){
                    let error = CGFloat(self.epochErrors[i])
                    let newPoint = CGPoint(x: lineRect.origin.x + (CGFloat(i) * epochUnit), y: ((error - CGFloat(finalMin)) * conversion) + lineRect.origin.y)
                    linePath.line(to: newPoint)
                }
                
                NSColor.myDarkBlue.setStroke()
                linePath.lineWidth = 1
                linePath.stroke()
                //Final line error
                if self.epochErrors.count == (NNSettings.maxEpochs){
                    if let finalError = self.epochErrors.last{

                        let error = CGFloat(finalError)
                        let finalPoint0 = CGPoint(x: lineRect.origin.x + (CGFloat(self.epochErrors.count) * epochUnit), y: ((error - CGFloat(finalMin)) * conversion) + lineRect.origin.y)
                        let finalPoint1 = CGPoint(x: dirtyRect.origin.x, y: ((error - CGFloat(finalMin)) * conversion) + lineRect.origin.y)
                        
                        let finalLinePath: NSBezierPath = NSBezierPath()
                        finalLinePath.move(to: finalPoint0)
                        finalLinePath.line(to: finalPoint1)
                        NSColor.myDarkGreen.setStroke()
                        finalLinePath.lineWidth = 1
                        finalLinePath.stroke()
                    }
                }
            }
        }
       
        
        //Error text
        if let finalError = self.epochErrors.last{
            var text = ""
            var color = NSColor.myDarkBlue
            if self.epochErrors.count == (NNSettings.maxEpochs){
                //Last
                text = "Final error value = \(finalError.format(f: 5))"
                color = NSColor.myDarkGreen
            }else{
                let actualDate = Date()
                let intervalTime = actualDate.timeIntervalSince(self.epochDate)
                self.epochDate = actualDate
                let remainingTime = intervalTime * TimeInterval(NNSettings.maxEpochs - 1 - self.epochErrors.count)
                let remainingTimeString = "Remaining \(remainingTime.stringify())"
                text = "\(self.epochErrors.count):\t\(finalError.format(f: 5))\t\(remainingTimeString)"
            }
            
            
            let attrs = [
                NSAttributedString.Key.font: NSFont.systemFont(ofSize: 12),
                NSAttributedString.Key.foregroundColor: color]
            let labelSize = text.size(withAttributes:attrs)
            let point = CGPoint(x: lineRect.maxX - labelSize.width, y: lineRect.size.height)
            text.draw(at: point, withAttributes: attrs)

        }

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NNManager.NNNotificationNameTrainingEphocError), object: nil, queue: nil) { (notification) in
            if let epochErrors = notification.userInfo?[NNSettings.epochErrorsKey] as? [Float]{
                self.epochErrors = epochErrors
                DispatchQueue.main.async {[weak self]() -> Void in
                    if let weakSelf = self{
                        weakSelf.setNeedsDisplay(weakSelf.bounds)
                    }

                }
            }
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
