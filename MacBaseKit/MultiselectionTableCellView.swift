//
//  MultiselectionTableCellView.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 12/09/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa


public protocol MultiselectionTableCellViewProtocol {
    func multiselectionTableCellView(selected:Bool, cell:MultiselectionTableCellView)
}

open class MultiselectionTableCellView: SubtitleImageTableCellView {

    @IBOutlet open var checkButton:NSButton!{
        didSet{
            self.checkButton.target = self
            self.checkButton.action = #selector(MultiselectionTableCellView.selectionAction(with:))
        }
    }
    @objc func selectionAction(with sender:NSButton){
        var selected = false
        if sender.state.rawValue > 0{
            selected = true
        }
        self.delegate?.multiselectionTableCellView(selected: selected, cell: self)
    }
    
    public var delegate:MultiselectionTableCellViewProtocol?
    
    override open func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
