//
//  PythonManager.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 24/10/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
open class PythonManager{
    
    public static let shared = PythonManager()

    public func dicomsToImages(from urls:[URL],
                               completionSingleImage:@escaping (_ imageNumber:Int, _ image:NSImage?, _ errorString:String?)->Void,
                               completionFinal:@escaping (_ images:[NSImage])->Void){
        self.innerDicomsToImages(from: urls,
                                 images: [NSImage](),
                                 completionSingleImage: completionSingleImage,
                                 completionFinal: completionFinal)
    }
    
    func innerDicomsToImages(from urls:[URL],
                             images:[NSImage],
                             completionSingleImage:@escaping (_ imageNumber:Int, _ image:NSImage?, _ errorString:String?)->Void,
                             completionFinal:@escaping (_ images:[NSImage])->Void){
        if urls.count == 0{
            completionFinal(images)
        }else{
            let url = urls[0]
            self.dicomToImage(from: url, completion: { (image, errorString) in
                var arrayImages = images
                var arrayUrls = urls

                if let i = image{
                    arrayImages.append(i)
                }
                completionSingleImage(arrayImages.count, image, errorString)
                arrayUrls.remove(at: 0)
                self.innerDicomsToImages(from: arrayUrls,
                                         images: arrayImages,
                                         completionSingleImage: completionSingleImage,
                                         completionFinal: completionFinal)
            })
        }
    }
    
    public func dicomToImage(from url:URL, completion:@escaping (_ image:NSImage?, _ errorString:String?)->Void){
        let urlString = url.absoluteString
        self.runPythonScript(withName: "in_app_dicom_to_image", args: [urlString]) { (data, errorString) in
            
            var image:NSImage?
            if let d = data{
                image = NSImage(data:d)
            }
            completion(image, errorString)
        }
    }
    
    
    public func runPythonScript(withName scriptName: String, args:[String], completion:@escaping (_ data:Data?, _ errorString:String?)->Void) {
        
        DispatchQueue.global().async {
            var errorString:String?
            let bundle = Bundle(for: PythonManager.self)
            guard let pythonFile = bundle.path(forResource: scriptName, ofType: "py") else {
                completion(nil, "ERROR: Script with name \(scriptName) not found")
                return
            }
            let outPipe = Pipe()
            let errPipe = Pipe();
            
            let task = Process()
            
            task.launchPath = "/usr/bin/python"
            task.arguments = [pythonFile]
            task.arguments?.append(contentsOf: args)
            task.standardInput = Pipe()
            task.standardOutput = outPipe
            task.standardError = errPipe
            task.launch()
            
            let data = outPipe.fileHandleForReading.readDataToEndOfFile()
            task.waitUntilExit()
            
            let exitCode = task.terminationStatus
            if (exitCode != 0) {
                print("ERROR: \(exitCode)")
                let data = errPipe.fileHandleForReading.readDataToEndOfFile()
                if let resultString = String(data: data, encoding: String.Encoding.ascii){
                    errorString = "ERROR: The script ended with following error \n:\(resultString)"
                }else{
                    errorString = "ERROR: Unknown error"
                }
            }
            DispatchQueue.main.async {
                completion(data, errorString)
            }
        }
        
        /*
        if let resultString = String(data: data, encoding: String.Encoding.ascii){
            //print("Result for python script: \(resultString)")
        }
         */
    }
    
    //MARK: NOT WORKING
    //NOT WORKING THE FUNCTION SYSTEM IS DEPRECATED IN SWIFT 4
    static func runSystemScriptWithDictionary(dictionaryPath: String, pattern: String, args:[String]){
        let bundle = Bundle(for: PythonManager.self)
        guard let pythonFile = bundle.path(forResource: dictionaryPath, ofType: "py") else {
            return
        }
        
        var launchPath = "/usr/bin/python" + " " + pythonFile
        
        for arg in args{
            launchPath = launchPath  + " " + arg
        }
        //let data = system(launchPath)
        //return data
    }

}
