//
//  MyXMLElement.swift
//  XmlKit
//
//  Created by Alessandro Ferrazza on 05/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class MyXMLElement: MyXMLNode {
    
    public var childs = [MyXMLNode]()
    public var value:String?
    public override var childLevel: Int{
        didSet{
            for node in self.childs{
                node.childLevel = self.childLevel + 1
            }
        }
    }
    public override init(name: String) {
        super.init(name: name)
    }

    public init(name: String, value: String?) {
        self.value = value
        super.init(name: name)
    }

    public convenience init(name: String, attributes:[String:String], value: String?) {
        self.init(name: name, value: value)
        self.setAttributes(with: attributes)
    }
    
    public func getValue() -> String?{
        return MyXMLNode.cleanedStringFromXML(string: self.value)
    }
    public func setChildLevel(level:Int){
        self.childLevel = level
        for node in self.childs{
            node.childLevel = childLevel + 1
        }
    }
    //MARK: Adding child
    public func add(child: MyXMLNode){
        self.childs.append(child)
        child.childLevel = self.childLevel + 1
        child.parent = self
    }
    
    
    public func insert(child:MyXMLNode, at index:Int){
        if index > self.childs.count{
            print("ERROR XMLElement: index for chid too high!!!")
        }
        self.childs.insert(child, at: index)
        child.childLevel = self.childLevel + 1
        child.parent = self
    }
    
    //MARK: Getting child
    public func getChild(with name:String) -> MyXMLElement?{
        for child in self.childs{
            if child.name == name{
                return child as? MyXMLElement
            }
        }
        return nil
    }
    
    public func getChild(with name:String, attributeName:String, attributeValue:String) -> MyXMLElement?{
        for child in self.childs{
            if let value = child.attributes[attributeName]{
                if value == attributeValue{
                    return child as? MyXMLElement
                }
            }
        }
        return nil
    }
    
    public func getChilds(with name:String) -> [MyXMLElement]{
        var arrayElements = [MyXMLElement]()
        for child in self.childs{
            if child.name == name{
                if let element = child as? MyXMLElement{
                    arrayElements.append(element)
                }
            }
        }
        return arrayElements
    }
    //removing childs
    public func removeChilds(with name:String){
        var indexes = [Int]()
        for (index, child) in self.childs.enumerated(){
            if child.name == name{
                if let _ = child as? MyXMLElement{
                    indexes.append(index)
                }
            }
        }
        let _ = self.childs.removeAtIndexes(indexes)
    }
    
    //MARK: Conversion
    public func toString() -> String{
        let line = "\n"
        let tab =  "\t"
        var result = ""
        for _ in 0..<(self.childLevel){
            result.append(tab)
        }
        //open tag + name
        let corrName = MyXMLNode.stringByRemovingXMLNotPermittedCharacters(string: self.name)
        result.append("<\(corrName)")
        //attributes
        for (key, value) in self.attributes{
            let corrKey = MyXMLNode.stringByRemovingXMLNotPermittedCharacters(string: key)
            let corrValue = MyXMLNode.stringByRemovingXMLNotPermittedCharacters(string: value)
            let attrString = " \(corrKey)=\"\(corrValue)\""
            result.append(attrString)
        }
        //close tag
        if self.value != nil || self.childs.count > 0{
            result.append(">\(line)")
        }else{
            result.append("/>\(line)")
        }
        
        //value inside open and close tag
        if let v = self.getValue(){
            let corrValue = MyXMLNode.stringByRemovingXMLNotPermittedCharacters(string: v)
            if corrValue != " "{//if after correction is equal to a space, it is nothing
                result.append("\(corrValue)\(line)")
            }
        }
        
        //Child
        for child in self.childs{
            if let element = child as? MyXMLElement{
                result.append("\(element.toString())")
            }
        }
        //closing tag
        if self.value != nil || self.childs.count > 0{
            for _ in 0..<(self.childLevel){
                result.append(tab)
            }
            result.append("</\(corrName)>\(line)")
        }
        return result
    }
    
    public func download() -> String{
        let paths = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask)
        let downloadUrl = paths[0]
        let filename =  downloadUrl.appendingPathComponent("outputXML.txt")
        
        do {
            try self.toString().write(to: filename, atomically: true, encoding: String.Encoding.utf8)
            return "File saved in Download with name outputXML.txt"
        } catch {
            return "failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding"
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
