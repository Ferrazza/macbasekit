//
//  ImagesCollectionView.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 22/10/2018.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

@available(OSX 10.11, *)
public protocol ImagesCollectionViewProtocol {
    func imagesCollectionViewEnded(with images:[NSImage])
}
@available(OSX 10.11, *)
class ImagesCollectionView: NSViewController {

    public var delegate:ImagesCollectionViewProtocol? = nil

    var images = [NSImage]()
    var indexPathsOfItemsBeingDragged: Set<IndexPath>!
    var draggingItem:NSCollectionViewItem?
    @IBOutlet weak var collectionView: NSCollectionView!
    @IBOutlet var removeButton: NSButton!

    var acceptableTypes: [NSPasteboard.PasteboardType] {
        //return [NSPasteboard.PasteboardType.fileURL]//only availaben on ios 10.13
        return [NSPasteboard.PasteboardType("Apple URL pasteboard type"), NSPasteboard.PasteboardType.tiff]//tiff è per le immagini
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCollectionView()
    }
    
    //Configure collection view
    fileprivate func configureCollectionView() {

        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 160.0, height: 140.0)
        flowLayout.sectionInset = NSEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
        flowLayout.minimumInteritemSpacing = 20.0
        flowLayout.minimumLineSpacing = 20.0
        self.collectionView.collectionViewLayout = flowLayout

        self.view.wantsLayer = true

        self.collectionView.layer?.backgroundColor = NSColor.black.cgColor
        
        self.registerForDragAndDrop()
    }
    
    @IBAction open func okButtonAction(sender:NSButton){
        self.delegate?.imagesCollectionViewEnded(with: self.images)
        self.dismiss(self)
    }
    @IBAction open func cancelButtonAction(sender:NSButton){
        self.dismiss(self)
        
    }
    @IBAction open func removeButtonAction(sender:NSButton){
        var indexes = [Int]()
        for ip in self.collectionView.selectionIndexPaths{
            indexes.append(ip.item)
        }
        _ = self.images.removeAtIndexes(indexes)
        self.collectionView.deleteItems(at: self.collectionView.selectionIndexPaths)


    }
    
    func moveImageFrom(fromIndexPath:IndexPath, toIndexPath:IndexPath){
        self.images.moveObjectFromIndex(fromIndexPath.item, toIndex: toIndexPath.item)
    }
    func insertImages(images:[NSImage], atIndexPath:IndexPath){
        self.images.insert(contentsOf: images, at: atIndexPath.item)
        self.collectionView.reloadData()
    }
    //register for drag and drop
    func registerForDragAndDrop() {
        // 1
        self.collectionView.registerForDraggedTypes(self.acceptableTypes)
        // 2
        self.collectionView.setDraggingSourceOperationMask(NSDragOperation.every, forLocal: true)
        // 3
        self.collectionView.setDraggingSourceOperationMask(NSDragOperation.every, forLocal: false)
    }
    
    
}
@available(OSX 10.11, *)
extension ImagesCollectionView : NSCollectionViewDataSource {
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ itemForRepresentedObjectAtcollectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let item = storyboard.instantiateController(withIdentifier: "ImageCollectionViewItem") as! ImageCollectionViewItem
        item.imageView?.image = self.images[indexPath.item]
        return item
    }
    
    /*
    func collectionView(_ collectionView: NSCollectionView, viewForSupplementaryElementOfKind kind: NSCollectionView.SupplementaryElementKind, at indexPath: IndexPath) -> NSView {
        // 1
        let identifier: String = kind
        let view = collectionView.makeSupplementaryView(ofKind: kind, withIdentifier: NSUserInterfaceItemIdentifier(rawValue: identifier), for: indexPath)
        if self.indexPathsOfItemsBeingDragged != nil{
            view.isHidden = true
        }else{
            view.isHidden = false
        }
        return view
    }
 */
    
    
}

@available(OSX 10.11, *)
extension ImagesCollectionView : NSCollectionViewDelegate {
    func collectionView(_ collectionView: NSCollectionView, canDragItemsAt indexPaths: Set<IndexPath>, with event: NSEvent) -> Bool{
        return true
    }
    
    func collectionView(_ collectionView: NSCollectionView, pasteboardWriterForItemAt indexPath: IndexPath) -> NSPasteboardWriting? {
        let image = images[indexPath.item]
        return image
    }
    
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItemsAt indexPaths: Set<IndexPath>) {

        self.indexPathsOfItemsBeingDragged = indexPaths
        
        //---------------
        if let indexPath = indexPaths.first{
            if let item = self.collectionView.item(at: indexPath) {
                self.draggingItem = item  // draggingItem is temporary instance variable of NSCollectionViewItem
            }
        }
    }
    
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, dragOperation operation: NSDragOperation) {
        self.indexPathsOfItemsBeingDragged = nil
        self.draggingItem = nil
    }
    func collectionView(_ collectionView: NSCollectionView, validateDrop draggingInfo: NSDraggingInfo, proposedIndexPath proposedDropIndexPath: AutoreleasingUnsafeMutablePointer<NSIndexPath>, dropOperation proposedDropOperation: UnsafeMutablePointer<NSCollectionView.DropOperation>) -> NSDragOperation {
        
        if let draggingItem = self.draggingItem{
            if let currentIndexPath = collectionView.indexPath(for: draggingItem), currentIndexPath != proposedDropIndexPath.pointee as IndexPath {  // guard to move once only
                
                self.collectionView.animator().moveItem(at: currentIndexPath, to: proposedDropIndexPath.pointee as IndexPath)
            }
        }
        if self.indexPathsOfItemsBeingDragged != nil{
            
            
            return NSDragOperation.move
        }else{
            
            return NSDragOperation.copy
        }
        
            /*
        if proposedDropOperation.pointee == NSCollectionView.DropOperation.on {
            proposedDropOperation.pointee = NSCollectionView.DropOperation.before
        }
        // 4
        if self.indexPathsOfItemsBeingDragged == nil {
            return NSDragOperation.copy
        } else {
            return NSDragOperation.move
        }
 */
    }
    
    
    
    
    func collectionView(_ collectionView: NSCollectionView, acceptDrop draggingInfo: NSDraggingInfo, indexPath: IndexPath, dropOperation: NSCollectionView.DropOperation) -> Bool {
        if self.indexPathsOfItemsBeingDragged != nil {

            let indexPathOfFirstItemBeingDragged = self.indexPathsOfItemsBeingDragged.first!
            var toIndexPath: IndexPath
            if indexPathOfFirstItemBeingDragged.compare(indexPath) == .orderedAscending {
                toIndexPath = IndexPath(item: indexPath.item-1, section: indexPath.section)
            } else {
                toIndexPath = IndexPath(item: indexPath.item, section: indexPath.section)
            }

            self.moveImageFrom(fromIndexPath: indexPathOfFirstItemBeingDragged, toIndexPath: toIndexPath)
        } else {
            // 5
            var droppedObjects = [NSImage]()
            draggingInfo.enumerateDraggingItems(options: NSDraggingItemEnumerationOptions.concurrent, for: collectionView, classes: [NSURL.self], searchOptions: [NSPasteboard.ReadingOptionKey.urlReadingFileURLsOnly : NSNumber(value: true)]) { (draggingItem, idx, stop) in
                if let url = draggingItem.item as? URL {
                    let image = NSImage(byReferencing: url)
                    droppedObjects.append(image)
                }
            }
            // 6
            self.insertImages(images: droppedObjects, atIndexPath: indexPath)
        }
        return true
    }
    /*
    func collectionView(collectionView: NSCollectionView, draggingSession session: NSDraggingSession, willBeginAtPoint screenPoint: NSPoint, forItemsAtIndexPaths indexPaths: Set<NSIndexPath>) {
        
        if let indexPath = draggingIndexPaths.first,
            item = collectionView.itemAtIndexPath(indexPath) {
            draggingItem = item  // draggingItem is temporary instance variable of NSCollectionViewItem
        }
    }
    
    func collectionView(collectionView: NSCollectionView, draggingSession session: NSDraggingSession, endedAtPoint screenPoint: NSPoint, dragOperation operation: NSDragOperation) {
        draggingItem = nil
    }
    
    func collectionView(collectionView: NSCollectionView, validateDrop draggingInfo: NSDraggingInfo, proposedIndexPath proposedDropIndexPath: AutoreleasingUnsafeMutablePointer<NSIndexPath?>, dropOperation proposedDropOperation: UnsafeMutablePointer<NSCollectionViewDropOperation>) -> NSDragOperation {
        
        if let proposedDropIndexPath = proposedDropIndexPath.memory,
            draggingItem = draggingItem,
            currentIndexPath = collectionView.indexPathForItem(draggingItem)
            where currentIndexPath != proposedDropIndexPath {  // guard to move once only
            
            collectionView.animator().moveItemAtIndexPath(currentIndexPath, toIndexPath: proposedDropIndexPath)
        }
 
        return .Move
    }
 */
    
}

