#!/usr/bin/python


import dicom, os, sys, export_as_image

window = None
level = None
path = '.'
#print('------------------------------')
#print('-------Start converting-------')


'''
try:
    window = int(sys.argv[1])
    level = int(sys.argv[2])
except BaseException as e:
    print('No window or level set')
'''
#img = export_as_image.save_PIL(dcm)
#window = int(sys.argv[1])
#level = int(sys.argv[2])

args = sys.argv
dcmfile = args[1]
if len(args) > 2:
    window = int(args[2])
    level = int(args[3])

'''
count = 0
for arg in args:
    if count > 0:
        dcmfiles.append(arg)
    count += 1
'''
#images = export_as_image.convert_to_png(dcmfiles, window, level)
image = export_as_image.convert_dicom_to_png(dcmfile, window, level)
print(image)

