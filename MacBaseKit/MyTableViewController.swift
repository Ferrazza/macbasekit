//
//  MyTableViewController.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 21/04/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa



protocol TableViewWithSectionProtocol {
    func tableView(_ tableView: NSTableView, numberOfRowsInSection section: Int) -> Int
    func numberOfSections(in tableView: NSTableView) -> Int
    func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String?
    func tableView(_ tableView: NSTableView, colorForHeaderInSection section: Int) -> NSColor?
    func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn:NSTableColumn?) -> NSView?
    func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    func tableView(_ tableView: NSTableView, heightForSection section: Int) -> CGFloat
    func tableView(_ tableView: NSTableView, didSelectRowAt indexPath: IndexPath)
    func tableView(_ tableView: NSTableView, didRightClickSelectRowAt indexPath: IndexPath, in rect:CGRect)
    func tableView(_ tableView: NSTableView, didRightClickSelectSection section: Int, in rect:CGRect)
    
    func tableView(_ tableView: NSTableView, canSelectRowAt indexPath: IndexPath) -> Bool
    @available(OSX 10.11, *)
    func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction]

}
open class MyTableViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate,  TableViewWithSectionProtocol {
   
    static let sectionTextFieldHeight:CGFloat = 22
    @IBOutlet open var tableView: NSTableView!
    public var alwaysShowsSection = false //show the section also if the title is nil with title "Section ..."

    /*
    public var columnZeroWidth:CGFloat!
    */
    override open func awakeFromNib() {
        super.awakeFromNib()
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //remove headers
        self.tableView.headerView?.isHidden = true
        if var frame = self.tableView.headerView?.frame{
            frame.size.height = 0
            self.tableView.headerView?.frame = frame
            
        }
        NotificationCenter.default.addObserver(forName: NSTableView.columnDidResizeNotification, object: nil, queue: nil) { (notification) in
            if let tableView = notification.object as? NSTableView{
                if tableView == self.tableView{
                    //let column = tableView.tableColumns[0]
                    tableView.reloadData()
                    //self.columnZeroWidth = column.width
                }
            }
        }
    }
    
    
    
    //MARK: helper methods
    open func indexPath(for cell: NSTableCellView)->IndexPath? {
        let row = self.tableView.row(for: cell)
        let value = self.indexPathOrSection(in: self.tableView, for: row)
        return value.indexPath
    }
    func indexPathOrSection(in tableView:NSTableView, for row:Int)->(indexPath:IndexPath?, section:Int?){
        var count = 0
        for s in 0..<self.numberOfSections(in: tableView) {
            if count == row{
                return (nil, s)
            }
            count = count + 1
            
            for r in 0..<self.tableView(tableView, numberOfRowsInSection: s){
                if count == row{
                    let ip = IndexPath(item: r, section: s)
                    return (ip, nil)
                }
                count = count + 1
            }
        }
        return (nil, nil)
    }
    
    open func rows(for indexPath:IndexPath, in tableView:NSTableView)->Int{
        var row = 0
        for s in 0..<indexPath.section {
            row = row + 1
            var rows = self.tableView(tableView, numberOfRowsInSection: s)
            if (indexPath.section) == s{
                rows = indexPath.item
            }
            for _ in 0..<rows{
                row = row + 1
            }
        }
        return  row
    }
    
    
    open func rowNumber(for indexPath:IndexPath, in tableView:NSTableView)->Int{
        var row = 0
        for s in 0...indexPath.section {
            row = row + 1
            var rows = self.tableView(tableView, numberOfRowsInSection: s)
            if (indexPath.section) == s{
                rows = indexPath.item
            }
            for _ in 0..<rows{
                row = row + 1
            }
        }
        return  row
    }
    
    open func rowNumber(for section:Int, in tableView:NSTableView)->Int{
        var row = 0
        if section > 0{
            for s in 0...(section - 1) {
                row = row + 1
                let rows = self.tableView(tableView, numberOfRowsInSection: s)
                row = row + rows
            }
        }
        
        return  row
    }
    
        
    open func numberOfRows(in tableView: NSTableView) -> Int {
        var lastIp = IndexPath(item: 0, section: 0)
        lastIp.section = self.numberOfSections(in: tableView)
        var sectionNumber:Int = 0
        if self.numberOfSections(in: tableView) > 0{
            sectionNumber = self.numberOfSections(in: tableView) - 1
        }
        lastIp.item = self.tableView(tableView, numberOfRowsInSection: sectionNumber)
        let rows = self.rows(for: lastIp, in: tableView)
        return rows
    }
    
    open func indexPathForSelectedRow() ->IndexPath?{
        let value = self.indexPathOrSection(in: self.tableView, for: self.tableView.selectedRow)
        return value.indexPath
    }
    //MARK: NSTableView Methods
    
    let sectionFont = NSFont.systemFont(ofSize: 20)
    public func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let ipOrSection = self.indexPathOrSection(in: tableView, for: row)
        if let ip = ipOrSection.indexPath{
            return self.tableView(tableView, cellForRowAt: ip, for: tableColumn)
        }else if let section = ipOrSection.section{
            let cellIdentifier = "section"
            var title = ""
            if let t = self.tableView(tableView, titleForHeaderInSection: section){
                title = t
            }else if self.alwaysShowsSection{
                title = "section \(section)"
            }
            
            //var backgroundColor = NSColor(calibratedRed: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            var backgroundColor = NSColor.controlBackgroundColor //to support dark mode
            if let c = self.tableView(tableView, colorForHeaderInSection: section){
                backgroundColor = c
            }
            var textField = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: cellIdentifier), owner: nil) as? NSTextField
            if textField == nil{
                if let width = tableColumn?.width{
                    textField = NSTextField(frame: NSRect(x: 0.1, y: 0.1, width: width, height: MyTableViewController.sectionTextFieldHeight))
                    textField?.identifier = NSUserInterfaceItemIdentifier(rawValue: cellIdentifier)
                    textField?.font = self.sectionFont
                }
                
            }
            
            textField?.stringValue = title
            textField?.drawsBackground = true
            textField?.backgroundColor = backgroundColor
            textField?.isBordered = false
            textField?.wantsLayer = true
            //textField?.layer?.borderColor = NSColor(calibratedRed: 0.75, green: 0.75, blue: 0.75, alpha: 1).cgColor
            textField?.layer?.borderColor = NSColor.controlHighlightColor.cgColor //to support dark mode
            textField?.layer?.borderWidth = 1

            //cell.textField?.alignment = NSTextAlignment.center
            return textField

        }
        return nil
    }
    
    public func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        let ipOrSection = self.indexPathOrSection(in: tableView, for: row)
        if let ip = ipOrSection.indexPath{
            return self.tableView(tableView, heightForRowAt: ip)
        }else if let section = ipOrSection.section{
            //the section is visible only if the number of rows is > 0 and the title is set
            if self.tableView(tableView, numberOfRowsInSection: section) > 0{
                return self.tableView(tableView, heightForSection: section)
            }
        }
        return 0.01 //cannot be 0
    }
    
    //MARK: TableView Selection
    public func tableViewSelectionDidChange(_ notification: Notification) {
        let tableView = notification.object as! NSTableView

        let row = tableView.selectedRow
        let ipOrSection = self.indexPathOrSection(in: tableView, for: row)
        if let ip = ipOrSection.indexPath{
            self.tableView(tableView, didSelectRowAt: ip)
        }
    }
    
    
    public func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        let ipOrSection = self.indexPathOrSection(in: tableView, for: row)
        if ipOrSection.section != nil{
            return false
        }else if let ip = ipOrSection.indexPath{
            return self.tableView(tableView, canSelectRowAt: ip)
        }
        return true
    }
    
    //MARK: RightClick
    open override func rightMouseUp(with event: NSEvent) {
        super.rightMouseUp(with: event)
        if let tableView = self.tableView{
            let point: NSPoint = tableView.convert(event.locationInWindow, from: nil)
            let row = tableView.row(at: point)
            let value = self.indexPathOrSection(in:tableView, for: row)
            let rect = tableView.frameOfCell(atColumn: 0, row: row)
            if let indexPath = value.indexPath{//ROWS
                self.tableView(tableView, didRightClickSelectRowAt: indexPath, in:rect)
            }else if let section = value.section{
                self.tableView(tableView, didRightClickSelectSection: section, in: rect)
            }
        }
        
        
    }
    //MARK: Force touc action
    /*
    open override func mouseUp(with event: NSEvent) {
        //super.mouseUp(with: event)
        if event.clickCount == 2{
            if let tableView = self.tableView{
                let point: NSPoint = tableView.convert(event.locationInWindow, from: nil)
                let row = tableView.row(at: point)
                let value = self.indexPathOrSection(in:tableView, for: row)
                let rect = tableView.frameOfCell(atColumn: 0, row: row)
                if let indexPath = value.indexPath{//ROWS
                    print("DOUBLE CLICK at row \(indexPath.item)")
                    
                    //self.tableView(tableView, didRightClickSelectRowAt: indexPath, in:rect)
                }else if let section = value.section{
                    //self.tableView(tableView, didRightClickSelectSection: section, in: rect)
                }
            }
        }
    }
    */

    
    //MARK: Remove row
    public func removeRows(at indexPaths:[IndexPath], withAnimation:NSTableView.AnimationOptions){
        let indexSet = NSMutableIndexSet()
        var dictSectionRows = [Int:Int]()//a dict counting the row deleted for every section
        for ip in indexPaths{
            let row = self.rowNumber(for: ip, in: self.tableView)
            indexSet.add(row)
            if let count = dictSectionRows[ip.section]{
                dictSectionRows[ip.section] = count + 1
            }else{
                dictSectionRows[ip.section] = 1
            }
        }
        for (section, count) in dictSectionRows{
            let rowInSection = self.tableView(self.tableView, numberOfRowsInSection: section)
            
            if rowInSection == count{//all the rows will be deleted and entire section must be deleted
                let sectionRow = self.rowNumber(for:section, in: self.tableView)
                indexSet.add(sectionRow)
            }
        }
        
        
        
        self.tableView.beginUpdates()
        self.tableView.removeRows(at: indexSet as IndexSet, withAnimation: withAnimation)
        self.tableView.endUpdates()
    }
    
    //MARK: Reload row
    public func reloadRows(at indexPaths:[IndexPath], forColumns columns:[Int]){
        let indexSet = NSMutableIndexSet()
        for ip in indexPaths{
            let row = self.rowNumber(for: ip, in: self.tableView)
            indexSet.add(row)
        }
        let indexSetColumns = NSMutableIndexSet()
        for i in columns{
            indexSetColumns.add(i)
        }

        self.tableView.beginUpdates()
        self.tableView.reloadData(forRowIndexes: indexSet as IndexSet, columnIndexes: indexSetColumns as IndexSet)
        self.tableView.endUpdates()
    }
    
    public func reloadRows(inSection section:Int, forColumns columns:[Int]){
        let indexSet = NSMutableIndexSet()
        let number = self.tableView(self.tableView, numberOfRowsInSection: section)
        for row in 0...(number - 1){
            let ip = IndexPath(item: row, section: section)
            let row = self.rowNumber(for: ip, in: self.tableView)
            indexSet.add(row)
        }
        let indexSetColumns = NSMutableIndexSet()
        for i in columns{
            indexSetColumns.add(i)
        }
        
        self.tableView.beginUpdates()
        self.tableView.reloadData(forRowIndexes: indexSet as IndexSet, columnIndexes: indexSetColumns as IndexSet)
        self.tableView.endUpdates()
    }
    
    //MARK: Adding row
    public func insertRows(at indexPaths:[IndexPath], withAnimation:NSTableView.AnimationOptions){
        let indexSet = NSMutableIndexSet()
        for ip in indexPaths{
            let row = self.rowNumber(for: ip, in: self.tableView)
            indexSet.add(row)
        }
        
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: indexSet as IndexSet, withAnimation: withAnimation)
        self.tableView.endUpdates()
    }
    public func addRow(inSection:Int, withAnimation:NSTableView.AnimationOptions){
        let row = self.tableView(self.tableView, numberOfRowsInSection: inSection) - 1
        let ip = IndexPath(item: row, section: inSection)
        self.insertRows(at: [ip], withAnimation: withAnimation)
    }
    //MARK: To Implement Methods
    
    open func tableView(_ tableView: NSTableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    open func numberOfSections(in tableView: NSTableView) -> Int {
        return 1
    }
    open func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    open func tableView(_ tableView: NSTableView, colorForHeaderInSection section: Int) -> NSColor? {
        return nil
    }
    open func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn:NSTableColumn?) -> NSView? {
        return nil
    }
    open func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    open func tableView(_ tableView: NSTableView, heightForSection section: Int) -> CGFloat {
        if let title = self.tableView(tableView, titleForHeaderInSection: section){
            if title.count > 0{
                let column = tableView.tableColumns[0]
                let widthColumn = column.width
                let tf = NSTextField(frame: NSRect(x: 0, y: 0, width:widthColumn , height: 0))
                tf.stringValue = title
                tf.font = self.sectionFont
                let sizeTitle = tf.sizeThatFits(NSSize(width: widthColumn, height: CGFloat.greatestFiniteMagnitude))
                let inset:CGFloat =  -4
                return sizeTitle.height + inset
            }
        }

        return MyTableViewController.sectionTextFieldHeight
    }
    
    open func tableView(_ tableView: NSTableView, didSelectRowAt indexPath: IndexPath) {

    }
    open func tableView(_ tableView: NSTableView, didRightClickSelectRowAt indexPath: IndexPath, in rect:CGRect) {
        
    }
    open func tableView(_ tableView: NSTableView, didRightClickSelectSection section: Int, in rect:CGRect){
        
    }
    
    open func tableView(_ tableView: NSTableView, canSelectRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    
    @available(OSX 10.11, *)
    open func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        return [NSTableViewRowAction]()
    }
    //MARK: Row action
    @available(OSX 10.11, *)
    public func tableView(_ tableView: NSTableView, rowActionsForRow row: Int, edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        var rowActions = [NSTableViewRowAction]()
        let ipOrSection = self.indexPathOrSection(in: tableView, for: row)
        if let ip = ipOrSection.indexPath{
            rowActions = self.tableView(tableView, actionsForRowAtIndexPath: ip, forEdge: edge)
        }
        return rowActions
    }
    
    //func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?
    //func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat
}
