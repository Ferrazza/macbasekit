
//
//  DrawHelper.swift
//  DrawKit
//
//  Created by Alessandro Ferrazza on 17/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

open class DrawHelper {
    
    
    
    public static func image(with color:NSColor, backgroundColor: NSColor?, size: CGFloat, instructionsBlock: @escaping (_ ctx: CGContext?)->Void) -> NSImage{
        
        //UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(size), height: CGFloat(size)), false, UIScreen.main.scale)
        // draw original image into the context
        //initialImage.draw(at: CGPoint.zero)
        //in mac os UIGraphicsBeginImageContextWithOptions become the follows
        let initialImage = NSImage(size: CGSize(width: size, height: size))
        let rep = NSBitmapImageRep(bitmapDataPlanes: nil,
                                   pixelsWide: Int(size),
                                   pixelsHigh: Int(size),
                                   bitsPerSample: 8,
                                   samplesPerPixel: 4,
                                   hasAlpha: true,
                                   isPlanar: false,
                                   colorSpaceName: NSColorSpaceName.calibratedRGB,
                                   bytesPerRow: 0,
                                   bitsPerPixel: 0)
        
        initialImage.addRepresentation(rep!)
        initialImage.lockFocus() //lockFocus makes the current context be the image and you draw directly onto it, then use the image

        //------------------------------------
        // get the context for CoreGraphics
        //let ctx = UIGraphicsGetCurrentContext()
        //UIGraphicsGetCurrentContext become the following
        let ctx = NSGraphicsContext.current?.cgContext
        
        //color background if needed
        if let color = backgroundColor {
            let fillRect = CGRect(x: 0, y: 0, width: size, height: size)
            ctx?.setFillColor(color.cgColor)
            ctx?.fill(fillRect)
        }
        // set stroking color and draw circle
        color.setStroke()
        instructionsBlock(ctx)
        
        // make image out of bitmap context
        //let retImage: NSImage? = UIGraphicsGetImageFromCurrentImageContext()
        
        initialImage.unlockFocus()
        return initialImage
    }
    
    
    public static func circolarPathEmpty(with initialAngle:CGFloat, finalAngle:CGFloat, radius:CGFloat, offset:CGSize) -> CGMutablePath{
        
        let path: CGMutablePath = CGMutablePath()
        path.move(to: CGPoint(x: radius + cos(initialAngle) * radius + offset.width, y: radius + CGFloat(sin(initialAngle)) * radius + offset.height), transform: .identity)
        path.addRelativeArc(center: CGPoint(x: radius + offset.width, y: radius + offset.height), radius: radius, startAngle: initialAngle, delta: finalAngle - initialAngle, transform: .identity)
        return path
    }
    
    public static func closedCircolarPathEmpty(with initialAngle:CGFloat, finalAngle:CGFloat, radius:CGFloat, offset:CGSize) -> CGMutablePath{
        
        let path: CGMutablePath = CGMutablePath()
        path.move(to: CGPoint(x: radius + cos(initialAngle) * radius + offset.width, y: radius + CGFloat(sin(initialAngle)) * radius + offset.height), transform: .identity)
        path.addRelativeArc(center: CGPoint(x: radius + offset.width, y: radius + offset.height), radius: radius, startAngle: initialAngle, delta: finalAngle - initialAngle, transform: .identity)
        path.addLine(to: CGPoint(x: radius + offset.width, y: radius + offset.height))
        path.closeSubpath()
        return path
    }
    
    public static func drawImagesInSlices(images:[[NSImage]], size: CGFloat) -> NSImage{
        return self.image(with: NSColor.blue, backgroundColor: nil, size: size) { (ctx) in

            let angleSlice = (CGFloat(-Double.pi) * 2)/CGFloat(images.count)
            let startAngle = CGFloat(Double.pi)/2
            var initialAngle = startAngle
            //var finalAngle = startAngle + angleSlice
            for (i, imageGroup)  in images.enumerated(){
                initialAngle = startAngle + angleSlice * CGFloat(i)
                //let finalAngle = startAngle + angleSlice  * CGFloat(i + 1)
                
                let subAngleSlice = angleSlice/CGFloat(imageGroup.count)
                var subInitialAngle = initialAngle
                //var subFinalAngle = initialAngle + angleSlice
                
                for (index, image) in imageGroup.enumerated(){
                    subInitialAngle = initialAngle + subAngleSlice * CGFloat(index)
                    let subFinalAngle = initialAngle + subAngleSlice  * CGFloat(index + 1)

                    let path = self.closedCircolarPathEmpty(with: subInitialAngle, finalAngle: subFinalAngle, radius: size/2, offset: CGSize(width: 0, height: 0))
                    let color = NSColor(patternImage: image)
                    
                    ctx?.saveGState()
                    
                    ctx?.setFillColor(color.cgColor)
                    ctx?.addPath(path)
                    ctx?.fillPath()
                    
                    ctx?.restoreGState()
                }
                
            }
            
        }
        
    }

    public static func errorInConnectionImage(with color: NSColor, size: CGFloat) -> NSImage {
        // begin a graphics context of sufficient size
        
        return self.image(with: color, backgroundColor: nil, size: size) { (ctx) in
            // set stroking color and draw circle
            color.setStroke()
            //get radius
            ctx?.saveGState()
            let insectFactor: CGFloat = 0.1
            let drawingInset = size * insectFactor
            let drawingRect = CGRect(x: 0, y: 0, width: size, height: size).insetBy(dx: drawingInset, dy: drawingInset)
            let drawingSize = drawingRect.size.height
            //CGPathAddRoundedRect(path, NULL, drawingRect, drawingSize/2, drawingSize/2);
            let initialAngle = CGFloat(-Double.pi/2 - Double.pi/4)
            let finalAngle = CGFloat(-Double.pi/2 + Double.pi/4)
            let radius = drawingSize / 6
            let centerX = size / 2
            let centerY = size - drawingInset
            let lineWidth = drawingInset / 4
            ctx?.saveGState()
            let centerPath: CGMutablePath = CGMutablePath()
            centerPath.addRelativeArc(center: CGPoint(x: centerX, y: centerY), radius: lineWidth, startAngle: 0, delta: CGFloat(Double.pi * 2), transform: .identity)
            ctx?.addPath(centerPath)
            for i in 1..<6 {
                let path: CGMutablePath = CGMutablePath()
                path.addRelativeArc(center: CGPoint(x: centerX, y: centerY), radius: radius * CGFloat(i), startAngle: initialAngle, delta: finalAngle - initialAngle, transform: .identity)
                ctx?.addPath(path)
            }
            ctx?.setLineWidth(CGFloat(lineWidth))
            ctx?.strokePath()
            ctx?.restoreGState()
            let cruxPath: CGMutablePath = CGMutablePath()
            //CGPathMoveToPoint(cruxPath, NULL, CGRectGetMinX(drawingRect), CGRectGetMinY(drawingRect));
            //CGPathAddLineToPoint(cruxPath, NULL, CGRectGetMaxX(drawingRect), CGRectGetMaxY(drawingRect));
            cruxPath.move(to: CGPoint(x: drawingRect.maxX, y: drawingRect.minY), transform: .identity)
            cruxPath.addLine(to: CGPoint(x: drawingRect.minX, y: drawingRect.maxY), transform: .identity)
            ctx?.addPath(cruxPath)
            ctx?.setLineWidth(CGFloat(lineWidth * 2))
            ctx?.setLineCap(CGLineCap.round)
            ctx?.strokePath()

        }
    }

/*
    public static func animateImageCharging(with color: NSColor, size: CGFloat) -> NSImage {
        let numberImages:Int = 30
        var frames = [NSImage]()
        for i in 0..<numberImages{
            let image = self.image(with: color, backgroundColor: NSColor.clear, size: size) { (ctx) in
                color.setStroke()
                //get center
                let deltaAngle:CGFloat = .pi * 1.8
                let rotation = .pi * 2 / CGFloat(numberImages)
                var fractionRadius:CGFloat = 0.5
                //fraction of image size of the radius
                let path1 = DrawHelper.circolarPathEmpty(with: CGFloat(i) * rotation,
                                                         finalAngle: (CGFloat(i) * rotation) + deltaAngle,
                                                         radius: size/2 * fractionRadius,
                                                         offset: CGSize(width: CGFloat(size / 2 * (CGFloat(i) - fractionRadius)), height: CGFloat(size / 2 * (1 - fractionRadius))))
                
                fractionRadius = 0.7
                let path2 = DrawHelper.circolarPathEmpty(with: -CGFloat(i) * rotation,
                                                         finalAngle: -CGFloat(i) * rotation + deltaAngle,
                                                         radius: size/2 * fractionRadius,
                                                         offset: CGSize(width: CGFloat(size / 2 * (CGFloat(i) - fractionRadius)), height: CGFloat(size / 2 * (1 - fractionRadius))))
                ctx?.addPath(path1)
                ctx?.addPath(path2)
                ctx?.strokePath()
            }
            
            frames.append(image)
        
        }
        return NSImage.animatedImage(with: frames, duration: 1)!
        
    }
    */
    public static func selectedImage(with color: NSColor, backgroundColor: NSColor?, size: CGFloat) -> NSImage {
        return self.image(with: color, backgroundColor: backgroundColor, size: size) { (ctx) in
            ctx?.saveGState()
            //get radius
            let inset = size / 5
            //float finalSize = size - inset * 2;
            let path: CGMutablePath = CGMutablePath()
            path.move(to: CGPoint(x: CGFloat(inset), y: CGFloat(size / 2)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset * 2), y: CGFloat(size / 2 + inset)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset * 4), y: CGFloat(size / 2 - inset)), transform: .identity)
            ctx?.addPath(path)
            ctx?.strokePath()
        }
    }
    public static func scrollImage(with color: NSColor, backgroundColor: NSColor?, size: CGFloat) -> NSImage {
        // begin a graphics context of sufficient size
        
        return self.image(with: color, backgroundColor: backgroundColor, size: size) { (ctx) in
            let inset = size / 6
            let finalSize = size - inset * 2
            ctx?.saveGState()
            let path: CGMutablePath = CGMutablePath()
            path.move(to: CGPoint(x: CGFloat(inset + finalSize / 2), y: CGFloat(inset)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset + finalSize / 2), y: CGFloat(inset + finalSize)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset + finalSize / 4), y: CGFloat(inset + finalSize / 4 * 3)), transform: .identity)
            path.move(to: CGPoint(x: CGFloat(inset + finalSize / 2), y: CGFloat(inset + finalSize)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset + finalSize / 4 * 3), y: CGFloat(inset + finalSize / 4 * 3)), transform: .identity)
            path.move(to: CGPoint(x: CGFloat(inset + finalSize / 4), y: CGFloat(inset + finalSize / 4)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset + finalSize / 2), y: CGFloat(inset)), transform: .identity)
            path.addLine(to: CGPoint(x: CGFloat(inset + finalSize / 4 * 3), y: CGFloat(inset + finalSize / 4)), transform: .identity)
            ctx?.addPath(path)
            ctx?.strokePath()
        }
    }

    public static func roundSelectionImage(with color: NSColor, size: CGFloat, selected:Bool) -> NSImage {
        return self.image(with: color, backgroundColor: nil, size: size) { (ctx) in
            ctx?.saveGState()
            //get radius
            var radius: CGFloat = size / 2 - 2
            let path: CGMutablePath = DrawHelper.circolarPathEmpty(with: 0,
                                                                   finalAngle: .pi * 2,
                                                                   radius: radius,
                                                                   offset: CGSize(width: size / 2 - radius, height: size / 2 - radius))
            ctx?.addPath(path)
            ctx?.strokePath()
            if selected {
                ctx?.restoreGState()
                ctx?.setFillColor(color.cgColor)
                radius *= 0.8
                let pathIn: CGMutablePath = DrawHelper.circolarPathEmpty(with: 0,
                                                                         finalAngle: .pi * 2,
                                                                         radius: radius,
                                                                         offset: CGSize(width: size / 2 - radius, height: size / 2 - radius))                    
                ctx?.addPath(pathIn)
                ctx?.fillPath()
            }
            
        }
    }
    
    public static func centeredArrowImage(withDirectionUp:Bool, color:NSColor, backgroundColor:NSColor?, size:CGFloat)->NSImage{
        return self.image(with: color, backgroundColor: nil, size: size) { (ctx) in

            //color background if needed
            if let bc = backgroundColor{
                let fillRect = CGRect(x: 0, y: 0, width: size, height: size)
                ctx?.setFillColor(bc.cgColor)
                ctx?.fill(fillRect)
            }
            
            // set stroking color and draw circle
            color.setStroke()
            
            //get radius
            let inset:CGFloat = 1
            let finalSize = size - inset * 2
            var height = finalSize/2
            if (withDirectionUp){
                height = -height
            }
            ctx?.saveGState()
            let path = CGMutablePath()
            path.move(to: CGPoint(x: inset, y: inset  + finalSize/2 - height/2))
            path.addLine(to: CGPoint(x: inset + finalSize/2, y: inset  + finalSize/2 + height/2))
            path.addLine(to: CGPoint(x: inset + finalSize, y: inset  + finalSize/2 - height/2))
            ctx?.addPath(path)
            ctx?.strokePath()
        }
    }
    
    public static func uploadImage(color:NSColor, backgroundColor:NSColor?, size:CGFloat)->NSImage{
        return self.image(with: color, backgroundColor: nil, size: size) { (ctx) in
            
            //color background if needed
            if let bc = backgroundColor{
                let fillRect = CGRect(x: 0, y: 0, width: size, height: size)
                ctx?.setFillColor(bc.cgColor)
                ctx?.fill(fillRect)
            }
            
            // set stroking color and draw circle
            color.setStroke()
            
            //get radius
            let inset:CGFloat = size/10
            let finalSize = size - inset * 2
            let heightArrow = finalSize/3*2
            let baseArrowDistance = finalSize/2
            
            
            ctx?.saveGState()
            let path = CGMutablePath()
            path.move(to: CGPoint(x: inset + (finalSize - baseArrowDistance)/2, y: inset + finalSize))
            path.addLine(to: CGPoint(x: inset + (finalSize - baseArrowDistance)/2, y: inset  + heightArrow))
            path.addLine(to: CGPoint(x: inset, y: inset  + heightArrow))
            path.addLine(to: CGPoint(x: size/2, y: inset))
            path.addLine(to: CGPoint(x: inset + finalSize, y: inset + heightArrow))
            path.addLine(to: CGPoint(x: inset + (finalSize - baseArrowDistance)/2 + baseArrowDistance, y: inset + heightArrow))
            path.addLine(to: CGPoint(x: inset + (finalSize - baseArrowDistance)/2 + baseArrowDistance, y: inset + finalSize))

            ctx?.addPath(path)
            ctx?.strokePath()
        }
    }

}
