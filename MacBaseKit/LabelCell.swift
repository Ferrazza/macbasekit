//
//  LabelCell.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 20/11/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

public class LabelCell: NSTableCellView {

    override public func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        let ctx = NSGraphicsContext.current?.cgContext
        let pathLine: CGMutablePath = CGMutablePath()
        pathLine.move(to: CGPoint(x: 0, y: 0))
        pathLine.addLine(to: CGPoint(x: dirtyRect.size.width, y: 0))
        ctx?.setLineWidth(0.5)
        ctx?.setStrokeColor(NSColor.gray.cgColor)
        ctx?.addPath(pathLine)
        ctx?.strokePath()    }
    
}
