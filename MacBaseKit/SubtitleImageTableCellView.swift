//
//  SubtitleImageTableCellView.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 09/05/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

open class SubtitleImageTableCellView: SubtitleTableCellView {

    @IBOutlet open var progressIndicator: NSProgressIndicator!

    open override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        //self.wantsLayer = true
        //self.imageView?.layer?.cornerRadius = 10

        // Drawing code here.
    }
    
}
