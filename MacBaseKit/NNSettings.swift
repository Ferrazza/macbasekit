//
//  NNSettings.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 20/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class NNSettings{
    
    public static let shared = NNSettings()
    
    
    
    //MARK: Setup
    public func setupSetting(with neuralNetwork:NeuralNet?){
        if let nn = neuralNetwork{
            NNSettings.layerNodes = nn.layerNodeCounts
            NNSettings.momentum = nn.momentumFactor
            NNSettings.learningRate = nn.learningRate
            NNSettings.batchSize = nn.batchSize
            NNSettings.hiddenActivation = nn.hiddenActivation
            NNSettings.outputActivation = nn.outputActivation
        }
    }
    //MARK:--------NETWORK----------
    public static var layerNodes:[Int] {
        get{
            if let result = UserDefaults.standard.object(forKey: "NNLayerNodes") as? [Int]{
                return result
            }else{
                return [4, 5, 4]
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNLayerNodes")
        }
    }
    
    //MARK:--------KEYS--------------------
    public static var meanSquaredKey = "NNMeanSquared"
    public static var crossEntropyKey = "NNCrossEntropy"
    public static var percentageKey = "NNPercentage"
    
    public static var linearKey = "NNLinear"
    public static var rectifiedLinearKey = "NNRectifiedLinear"
    public static var hyperbolicTangentKey = "NNHyperbolicTangent"
    public static var sigmoidKey = "NNSigmoid"
    public static var softmaxKey = "NNSoftmax"
    
    public static var epochErrorsKey = "NNEpochErrors"

    public func errorFunction(for title:String)->NeuralNet.ErrorFunction{
        switch title{
        case NNSettings.meanSquaredKey:
            return NeuralNet.ErrorFunction.meanSquared
        case NNSettings.crossEntropyKey:
            return NeuralNet.ErrorFunction.crossEntropy
        default:
            return NeuralNet.ErrorFunction.percentage
        }
    }
    
    public func title(for errorFunction:NeuralNet.ErrorFunction)->String{
        switch errorFunction{
        case NeuralNet.ErrorFunction.meanSquared:
            return NNSettings.meanSquaredKey
        case NeuralNet.ErrorFunction.crossEntropy:
            return NNSettings.crossEntropyKey
        default:
            return NNSettings.percentageKey
        }
    }
    public func hiddenActivation(for title:String)->NeuralNet.ActivationFunction.Hidden{
        switch title{
        case NNSettings.linearKey:
            return NeuralNet.ActivationFunction.Hidden.linear
        case NNSettings.rectifiedLinearKey:
            return NeuralNet.ActivationFunction.Hidden.rectifiedLinear
        case NNSettings.hyperbolicTangentKey:
            return NeuralNet.ActivationFunction.Hidden.hyperbolicTangent
        default:
            return NeuralNet.ActivationFunction.Hidden.sigmoid
        }
    }
    public func title(for hiddenActivation:NeuralNet.ActivationFunction.Hidden)->String{
        switch hiddenActivation{
        case NeuralNet.ActivationFunction.Hidden.linear:
            return NNSettings.linearKey
        case NeuralNet.ActivationFunction.Hidden.rectifiedLinear:
            return NNSettings.rectifiedLinearKey
        case NeuralNet.ActivationFunction.Hidden.hyperbolicTangent:
            return NNSettings.hyperbolicTangentKey
        default:
            return NNSettings.sigmoidKey
        }
    }
    
    public func outputActivation(for title:String)->NeuralNet.ActivationFunction.Output{
        switch title{
        case NNSettings.linearKey:
            return NeuralNet.ActivationFunction.Output.linear
        case NNSettings.rectifiedLinearKey:
            return NeuralNet.ActivationFunction.Output.rectifiedLinear
        case NNSettings.hyperbolicTangentKey:
            return NeuralNet.ActivationFunction.Output.hyperbolicTangent
        case NNSettings.sigmoidKey:
            return NeuralNet.ActivationFunction.Output.sigmoid
        default:
            return NeuralNet.ActivationFunction.Output.softmax
        }
    }
    
    public func title(for outputActivation:NeuralNet.ActivationFunction.Output)->String{
        switch outputActivation{
        case NeuralNet.ActivationFunction.Output.linear:
            return NNSettings.linearKey
        case NeuralNet.ActivationFunction.Output.rectifiedLinear:
            return NNSettings.rectifiedLinearKey
        case NeuralNet.ActivationFunction.Output.hyperbolicTangent:
            return NNSettings.hyperbolicTangentKey
        case NeuralNet.ActivationFunction.Output.sigmoid:
            return NNSettings.sigmoidKey
        default:
            return NNSettings.softmaxKey
        }
    }
    
    //MARK:--------SETTINGS FOR TRAINING----------
    public static var learningRate:Float{
        get{
            if let result = UserDefaults.standard.object(forKey: "NNLearningRate") as? Float{
                return result
            }else{
                return 0.01
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNLearningRate")
        }
    }
    public static var momentum:Float{
        get{
            if let result = UserDefaults.standard.object(forKey: "NNMomentum") as? Float{
                return result
            }else{
                return 0.5
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNMomentum")
        }
    }
    public static var numerosity:Int{
        get{
            if let result = UserDefaults.standard.object(forKey: "NNNumerosity") as? Int{
                return result
            }else{
                return 10000
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNNumerosity")
        }
    }
    public static var maxEpochs:Int{
        get{
            if let result = UserDefaults.standard.object(forKey: "NNMaxEpochs") as? Int{
                return result
            }else{
                return 10
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNMaxEpochs")
        }
    }
    public static var errorThreshold:Float{
        get{
            if let result = UserDefaults.standard.object(forKey: "NNErrorThreshold") as? Float{
                return result
            }else{
                return 0.000000003
            }
        }
        set (newId){
            UserDefaults.standard.set(newId, forKey: "NNErrorThreshold")
        }
    }
    
    public static var batchSize:Int = 1 //Per dare un batch size superiore a 1 devo modificare il formato dei dati che vengono forniti (array di array anzichè array).
    
    
    public static var errorFunction:NeuralNet.ErrorFunction{
        //meanSquare è quello che mi da il valore più basso di errore nel training
        get{
            if let title = UserDefaults.standard.object(forKey: "NNErrorFunction") as? String{
                return NNSettings.shared.errorFunction(for: title)
            }else{
                return NeuralNet.ErrorFunction.crossEntropy
            }
        }
        set (newId){
            let title = NNSettings.shared.title(for: newId)
            UserDefaults.standard.set(title, forKey: "NNErrorFunction")
        }
    }
    public static var hiddenActivation:NeuralNet.ActivationFunction.Hidden{
        get{
            if let title = UserDefaults.standard.object(forKey: "NNHiddenActivation") as? String{
                return NNSettings.shared.hiddenActivation(for: title)
            }else{
                return NeuralNet.ActivationFunction.Hidden.sigmoid
            }
        }
        set (newId){
            let title = NNSettings.shared.title(for: newId)
            UserDefaults.standard.set(title, forKey: "NNHiddenActivation")
        }
    }
    public static var outputActivation:NeuralNet.ActivationFunction.Output{
        get{
            if let title = UserDefaults.standard.object(forKey: "NNOutputActivation") as? String{
                return NNSettings.shared.outputActivation(for: title)
            }else{
                return NeuralNet.ActivationFunction.Output.sigmoid
            }
        }
        set (newId){
            let title = NNSettings.shared.title(for: newId)
            UserDefaults.standard.set(title, forKey: "NNOutputActivation")
        }
    }

    
    
}
