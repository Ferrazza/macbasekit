//
//  NNManager.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 20/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class NNManager{
    
    public static let shared = NNManager()

    
    
    
    //MARK: Create network
    
    public func newNeuralNetwork(with nodes:[Int]) -> NeuralNet?{
        do{
            if let structure = self.newStructure(with:nodes){
                let neuralNetwork = try NeuralNet(structure: structure)
                return neuralNetwork
            }
        }catch{
            print("Error in generating neural network")
        }
        return nil
        
    }
    
    public func newStructure(with nodes:[Int]) ->NeuralNet.Structure?{
        do{
            NNSettings.layerNodes = nodes
            let structure = try NeuralNet.Structure(nodes: nodes,
                                                    hiddenActivation: NNSettings.hiddenActivation,
                                                    outputActivation: NNSettings.outputActivation,
                                                    batchSize: NNSettings.batchSize,
                                                    learningRate: NNSettings.learningRate,
                                                    momentum: NNSettings.momentum)
            
            return structure
        }catch{
            print("Error in generating structure")
        }
        return nil
    }
    
    //MARK: Train network
    static let NNNotificationNameTrainingEphocError = "NotificationNameTrainingEphocError"
    var epochErrors = [Float]()
    
    public func train(neuralNetwork:NeuralNet, with dataset:NeuralNet.Dataset, epochCompletion:@escaping (_ epochNumber:Int, _ epochError:Float) -> Void, finalCompletion:@escaping (_ neuralNetwork:NeuralNet?)->Void){
        self.epochErrors.removeAll()
        DispatchQueue.global().async{() in
            do{
                let finalEpoch = try neuralNetwork.train(dataset,
                                                         maxEpochs: NNSettings.maxEpochs,
                                                         errorThreshold: NNSettings.errorThreshold,
                                                         errorFunction: NNSettings.errorFunction,
                                                         epochCallback: { [weak self](epochNumber, epochError) -> Bool in
                                                            epochCompletion(epochNumber, epochError)
                                                            self?.epochErrors.append(epochError)
                                                            NotificationCenter.default.post(name: NSNotification.Name(NNManager.NNNotificationNameTrainingEphocError), object: nil, userInfo: [NNSettings.epochErrorsKey: self?.epochErrors ?? [Float]()])

                                                            //print("Epoch number:\(epochNumber)")
                                                            //print("Epoch error:\(epochError.format(f: 8))")
                                                            if epochError < NNSettings.errorThreshold{
                                                                return false
                                                            }
                                                            return true
                })
                
                print("Final epoch number:\(finalEpoch.epochs)")
                print("Final epoch error:\(finalEpoch.error.format(f: 8))")
                finalCompletion(neuralNetwork)
            }catch {
                print("Error in training neural network")
            }
        }
    }
    
    //MARK: Saving and Loading Neural Network
    public func save(neuralNetwork:NeuralNet, to url:URL, completion:@escaping (_ stringCompletion:String)->Void){
        DispatchQueue.global().async {
            do{
                try neuralNetwork.save(to: url)
                completion("Neural network saved at url:\n\(url)")
            }catch{
                completion("Error during saving neural network at url:\n\(url)")
            }
        }
        
    }
    
    public func load(from url:URL, completion:@escaping (_ neruralNetwork:NeuralNet?, _ stringCompletion:String)->Void){
        DispatchQueue.global().async {
            do{
                let neuralNetwork = try NeuralNet(url: url)
                NNSettings.shared.setupSetting(with: neuralNetwork)
                completion(neuralNetwork, "Neural network loaded from url:\n\(url)")
            }catch{
                completion(nil, "Error during loading neural network from url:\n\(url)")
            }
        }
    }
    
    public static var savedWeights:[[Float]]? {
        get{
            if let result = UserDefaults.standard.object(forKey: "NNWeights") as? [[Float]]{
                print("retrieving weights: \(result)")
                return result
            }else{
                return nil
            }
        }
        set (newId){
            if let weights = newId{
                print("saving weights: \(weights)")
                UserDefaults.standard.set(weights, forKey: "NNWeights")
            }else{
                UserDefaults.standard.removeObject(forKey: "NNWeights")
            }
        }
    }
    
    public static var savedBiases:[[Float]]? {
        get{
            if let result = UserDefaults.standard.object(forKey: "NNBiases") as? [[Float]]{
                //print("retrieving weights: \(result)")
                return result
            }else{
                return nil
            }
        }
        set (newId){
            if let weights = newId{
                //print("saving weights: \(weights)")
                UserDefaults.standard.set(weights, forKey: "NNBiases")
            }else{
                UserDefaults.standard.removeObject(forKey: "NNBiases")
            }
        }
    }
}
