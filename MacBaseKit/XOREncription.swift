//
//  XOREncription.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 29/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public class XOREncription{
    
    /**
    //ENCRIPT
    if let pass = password{
        var finalData = data
        if let encriptedData = XOREncription.encrypt(dataUtf8String: data, with: pass){
            finalData = encriptedData
        }
    }
    */
    /**
    //DECRIPT
    if let pass = password{
        var finalData = data
        if let decriptedData = XOREncription.decrypt(dataUtf8String: data, with: pass){
            finalData = decriptedData
        }
    }
    */
    
    public static func encrypt(dataUtf8String:Data, with password:String)->Data?{
        if let string = String(data: dataUtf8String, encoding: String.Encoding.utf8){
            return XOREncription.encript(text: string, with: password)
        }
        return nil
    }
    
    public static func decrypt(dataUtf8String:Data, with password:String)->Data?{
        if let string = XOREncription.decript(data: dataUtf8String, with: password){
            let data = string.data(using: String.Encoding.utf8)
            return data
        }
        return nil
    }
    
    
    
    public static func encript(text:String, with password:String)->Data{
        let textUtf8 = [UInt8](text.utf8)
        let passwordUtf8 = [UInt8](password.utf8)
        
        var encrypted = [UInt8]()
        
        // encrypt bytes
        for (i, t) in textUtf8.enumerated() {
            var elementIndex = i
            if elementIndex >= passwordUtf8.count{
                elementIndex = i%passwordUtf8.count
            }
            encrypted.append(t ^ passwordUtf8[elementIndex])
        }
        return XOREncription.data(from: encrypted)
        //let data = Data(encrypted)
        //return data
    }
    
    public static func decript(data:Data, with password:String)->String?{
        let encrypted = XOREncription.array(from: data)
        let passwordUtf8 = [UInt8](password.utf8)
        
        var decrypted = [UInt8]()
        
        // decrypt bytes
        for (i, t) in encrypted.enumerated() {
            var elementIndex = i
            if elementIndex >= passwordUtf8.count{
                elementIndex = i%passwordUtf8.count
            }
            decrypted.append(t ^ passwordUtf8[elementIndex])
        }
        
        return String(bytes: decrypted, encoding: String.Encoding.utf8)
        
    }
    
    public static func data(from array:[UInt8])->Data{
        let arr = array
        let data = Data(buffer: UnsafeBufferPointer(start: arr, count: arr.count))
        return data
    }
    public static func array(from data:Data)->[UInt8]{
        let arr = data.withUnsafeBytes {
            Array(UnsafeBufferPointer<UInt8>(start: $0, count: data.count/MemoryLayout<UInt8>.stride))
        }
        return arr
    }
}
