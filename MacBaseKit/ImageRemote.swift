//
//  ImageRemote.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 08/07/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation

public protocol ImageObjectProtocol{
    //This protocol is to provide data to permit to save and delete remotely imageObject
    func mainDirectoryName()->String

    func imageObjectProtocolDirectoryImage()->String //this must provide a directory used to save images (es. Images/), can be a string with no character if you want that the path saved in imageObject is complete. Es. if you do not set it (es. "") the path saved will be completely es. Images/Path/To/Image, else if you want save only the last path components return something (Es. "Images/") in this way the path saved will be Path/To/Image
    func imageObjectProtocolPhpFileToUpload()->URL //Must return the complete url to upload a file that take this post values:
    //"dir":uploadDirectoryName
    //"overwrite":String(overwrite)
    //"extension":"png
    func imageObjectProtocolPhpFileToDelete()->URL//Must return the complete url to delete a file that take this post values:
    //"url":baseUrlImage (es. Path/To/Image)
    func imageObjectProtocolPhpFileToGetSubDirectoryName()->URL?//Must return the complete url to a file that Get SubDirectory Name and take this post values:
    //"directoryName":directoryName,
    //"baseSubDirectoryName":"imageSequence"
    //if not provided the files will be overwrited

}

public class ImageOrUrl{
    public var image: NSImage?
    public var url: URL?
    public init(image: NSImage) {
        self.image = image
    }
    public init(url: URL) {
        self.url = url
    }
    
}

public class ImageObject{
    
    public var delegate:ImageObjectProtocol?
    
    public var imageTitle:String = ""
    public var imageDescription:String?
    public var imageTitleTraduction:String = ""
    public var imageDescriptionTraduction:String?
    
    //these are used for advanced saving
    public var imageSequenceBaseUrl:String?
    public var imageSequenceCount:Int?
    public var imageSequenceFormat:Int? //if 3 the url is baseUrl001, baseUrl002...

    public var rootUrl:URL?//is the base url added to the url in the value when retrived through imageUrl or imageUrls to make the complete url
    //var _imageUrl: URL?
    
    public var imageId:String{
        get{
            if let url = self.imageValue?.url{
                return url.absoluteString
            }else if let image = self.imageValue?.image{
                return image.description
            }else{
                let formatter = DateFormatter()
                return formatter.string(from: Date())
            }
        }
    }
    
    var imageValue:ImageOrUrl?{
        get{
            if self.imageValues.count > 0{
                return imageValues[0]
            }
            return nil
        }
    }
    
    public func setImage(image:NSImage, at index:Int){
        if self.images.count > index{
            self.imageValues[index].image = image
        }
    }
    var imageValues = [ImageOrUrl]()
    
    public init(imageUrl: URL) {
        self.imageValues = [ImageOrUrl(url:imageUrl)]
    }
    public init(image: NSImage) {
        self.imageValues = [ImageOrUrl(image:image)]
    }
    public init(imageUrls: [URL]) {
        for url in imageUrls{
            self.imageValues.append(ImageOrUrl(url:url))
        }
    }
    public init(images: [NSImage]) {
        for image in images{
            self.imageValues.append(ImageOrUrl(image:image))
        }
    }
    
    public convenience init (title:String, description:String?, imageUrl:URL){
        self.init(imageUrl: imageUrl)
        self.imageTitle = title
        self.imageDescription = description
    }

    public convenience init (title:String, description:String?, image:NSImage){
        self.init(image: image)
        self.imageTitle = title
        self.imageDescription = description
    }

    public static func imageObject(for xmlElement:MyXMLElement, rootUrlForImageObject:URL?) ->ImageObject?{
        return ImageObject.imageObject(for: xmlElement, title: nil, description: nil, rootUrlForImageObject: rootUrlForImageObject)
    }
    //static let isImageSequenceKey = "isImageSequence"
    static let nameKey = "name"
    static let imageSequenceBaseUrlKey = "imageSequenceBaseUrl"
    static let imageSequenceCountKey = "imageSequenceCount"
    static let imageSequenceFormatKey = "imageSequenceFormat"
    //static let imageUrlKey = "imageUrl"
    static var descriptionKey = "description";


    public static func imageObject(for xmlElement:MyXMLElement, title:String?, description:String?, rootUrlForImageObject:URL?) ->ImageObject?{

        var result:ImageObject?
    
        //var isImageSequence = true
        //if let isSequence = xmlElement.attributes[ImageObject.isImageSequenceKey]{
        //    isImageSequence = isSequence.boolValue
        //}
        if let name = xmlElement.attributes[ImageObject.nameKey]{
            var imageTitle = name
            if let t = title{
                imageTitle = t
            }
            var imageDescription = description
            if let d = xmlElement.attributes[ImageObject.descriptionKey]{
                imageDescription = MyXMLNode.reformatNFormat(from:d)
            }
            //add sequence
            if let baseUrl = xmlElement.attributes[ImageObject.imageSequenceBaseUrlKey],
                let count = xmlElement.attributes[ImageObject.imageSequenceCountKey],
                let format = xmlElement.attributes[ImageObject.imageSequenceFormatKey]{
                let countInt = count.intValue
                let formatInt = format.intValue
                let extensionString = ".png"//default
                
                let imageSequenceUrls = ImageObject.arrayUrls(forImageSequenceBaseUrl: baseUrl, imageSequenceCount: count.intValue, imageSequenceFormat: format.intValue, extensionString: extensionString)
                result = ImageObject(imageUrls:imageSequenceUrls)
                result?.imageSequenceCount = countInt
                result?.imageSequenceFormat = formatInt
                result?.imageSequenceBaseUrl = baseUrl
            }
            //single images are also considered as sequences
            //if isImageSequence{
            
            //}
            /*
            else if let urlString = xmlElement.attributes[ImageObject.imageUrlKey]{
                if let url = URL(string: urlString){
                    result = ImageObject(imageUrl: url)
                }
            }
 */
            result?.imageTitle = imageTitle
            result?.imageDescription = imageDescription
        }
        result?.rootUrl = rootUrlForImageObject
        return result
        
    }
    
    public func generateXmlElement(withName:String)->MyXMLElement{
        
        let imageElement = MyXMLElement(name: withName)
        var attr = [ImageObject.nameKey:self.imageTitle]
        if let d = self.imageDescription{
            attr[ImageObject.descriptionKey] = d
        }
        /*
        if let i = self.imageUrl{
            attr[ImageObject.imageUrlKey] = i.absoluteString
        }
 
        if self.isSequence {
            attr[ImageObject.isImageSequenceKey] = "\(true)"
        }
         */
        if let sequenceBaseUrl = self.imageSequenceBaseUrl{
            attr[ImageObject.imageSequenceBaseUrlKey] = sequenceBaseUrl
        }
        if let sequenceCount = self.imageSequenceCount{
            attr[ImageObject.imageSequenceCountKey] = "\(sequenceCount)"
        }
        if let sequenceFormat = self.imageSequenceFormat{
            attr[ImageObject.imageSequenceFormatKey] = "\(sequenceFormat)"
        }
        imageElement.setAttributes(with: attr)
        
        return imageElement
    }


    public static func arrayUrls(forImageSequenceBaseUrl:String, imageSequenceCount:Int, imageSequenceFormat:Int, extensionString:String)->[URL]{
        var imageSequenceUrls = [URL]()
        if imageSequenceCount > 0{
            for index in 1...imageSequenceCount{
                let urlString = String(format: "%@%0\(imageSequenceFormat)d%@",forImageSequenceBaseUrl, index, extensionString)
                if let url = URL(string: urlString){
                    imageSequenceUrls.append(url)
                }
            }
        }
        return imageSequenceUrls
    }

    public var images:[NSImage?]{
        get{
            var array = [NSImage?]()
            for i in 0..<self.imageValues.count{
                let value = self.imageValues[i]
                array.append(value.image)
                
            }
            return array
        }
    }
    
    public var downloadedImages:[NSImage]{
        get{
            var arrayImages = [NSImage]()
            for i in 0..<self.images.count{
                if let image = self.images[i]{
                    arrayImages.append(image)
                }
            }
            return arrayImages
        }
    }
    
    public var imageUrls:[URL?]{
        get{
            var array = [URL?]()
            for i in 0..<self.imageValues.count{
                let value = self.imageValues[i]
                var completeUrl = value.url
                if let root = rootUrl, let url = value.url{
                    completeUrl = root.appendingPathComponent(url.absoluteString)
                }
                array.append(completeUrl)
                            }
            return array
        }
    }
    
    public var imageUrl:URL?{
        get{
            if self.imageUrls.count > 0{
                return self.imageUrls[0]
            }
            return nil
        }
    }
    public var image:NSImage?{
        get{
            if self.images.count > 0{
                return self.images[0]
            }
            return nil
        }
    }
    public var imageUrlStringLastPaths:String?{
        get{
            if let url = self.imageUrl?.absoluteString{
                var finalUrl = url
                if var root = self.rootUrl?.absoluteString{
                    root = root + "/"
                    finalUrl = finalUrl.replacingOccurrences(of: root, with: "")
                }
                return finalUrl
            }
            return nil
        }
    }
    public convenience init(imageUrlStrings: [String]) {
        var urls = [URL]()
        for index in 0..<imageUrlStrings.count{
            let string = imageUrlStrings[index]
            if let url = URL(string: string){
                urls.append(url)
            }
        }
        self.init(imageUrls: urls)
    }
    
    public var isSequence:Bool{
        get{
            return self.imageValues.count > 1
        }
    }
    
    public func getOnlyDownloadedImages() -> [NSImage] {
        var result = [NSImage]()
        for value in self.imageValues{
            if let image = value.image{
                result.append(image)
            }
        }
        return result
    }
    
    public func allImagesHasBeenDownloaded() -> Bool {
        var result = false
        for value in self.imageValues{
            result = true
            if value.image == nil{
                result = false
                break
            }
        }
        return result
        
    }
   
    //MARK:Saving remotely
    fileprivate static func generateFileName(_ baseFileName:String, imageNumber:Int, totalCount:Int)->String{
        let number = imageNumber + 1
        let digits = ImageObject.digitNumber(for: totalCount)
        let digitsImageNumber = "\(number)".count
        var result = "\(baseFileName)"
        for _ in digitsImageNumber...digits{
            result = result + "0"
        }
        result = "\(result)\(number)"
        return result
    }
    
    public static func digitNumber(for count:Int)->Int{
        return "\(count)".count
    }
    var imageNumberUploaded:Int?
    var finalDirectory:String?
    
    public func saveRemotely(withDelegate delegate:ImageObjectProtocol,
                             baseFileName:String,
                             directoryName:String,
                             rootUrl:URL?,
                             overwrite:Bool,
                             checkForSubDirectoryName:Bool,
                             percentUploadBlock:@escaping (_ percentUpload: Float)->Void,
                             completionImage: @escaping (_ imageNumber:Int, _ imageTotalCount:Int)->Void,
                             completionFinal: @escaping (_ imageCount:Int)->Void){
        
        //check if image number is set
        if self.imageNumberUploaded == nil{
            self.imageNumberUploaded = 0
        }
        if self.imageNumberUploaded! < self.imageValues.count{
            //First: Check if it is already saved
            if self.imageValues[self.imageNumberUploaded!].url != nil{
                //alreadySaved
                if overwrite{
                    
                }else{
                    self.imageNumberUploaded = nil
                    self.finalDirectory = nil
                    completionFinal(self.imageValues.count)
                    return
                }
                
            }
            //Second: if not set, set the needed subdirectory by retrieving it from remote url
            if checkForSubDirectoryName{
                if let phpFileToGetSubDirectoryName = delegate.imageObjectProtocolPhpFileToGetSubDirectoryName(){
                    if self.finalDirectory == nil{
                        let postToGetSubDirectoryName = PostObject(with: phpFileToGetSubDirectoryName)
                        //print("\nChecking dir: \(directoryName)\n")
                        postToGetSubDirectoryName.sendAsynchronousPost(withDict: ["directoryName":directoryName, "baseSubDirectoryName":"imageSequence"]) { [weak self](data, response, error) in
                            var finalDirectory = directoryName
                            if let d = data{
                                let subDirName = PostObject.stringifyData(d)
                                finalDirectory = "\(directoryName)\(subDirName)/"
                            }
                            self?.finalDirectory = finalDirectory
                            self?.saveRemotely(withDelegate: delegate,
                                               baseFileName: baseFileName,
                                               directoryName: finalDirectory,
                                               rootUrl:rootUrl,
                                               overwrite: overwrite,
                                               checkForSubDirectoryName:false,
                                               percentUploadBlock: percentUploadBlock,
                                               completionImage: completionImage,
                                               completionFinal: completionFinal)
                            
                        }
                        return
                    }
                }
            }
            
            //Third: upload the image
            if self.imageValues[self.imageNumberUploaded!].url == nil{
                //url not present -> need to be saved
                if let image = self.images[self.imageNumberUploaded!] {
                    let postUpload = PostObject(with: delegate.imageObjectProtocolPhpFileToUpload())
                    if let dataUnw = image.PNGRepresentation(){
                        let fileName = ImageObject.generateFileName(baseFileName, imageNumber: self.imageNumberUploaded!, totalCount: self.imageValues.count)
                        let uploadDirectoryName = "\(delegate.imageObjectProtocolDirectoryImage())\(directoryName)"
                        //print("Upload directoryName : \(uploadDirectoryName)")
                        postUpload.uploadAsynchronous(data: dataUnw,
                                                      withFileName: fileName,
                                                      dictPost: ["dir":uploadDirectoryName, "overwrite":String(overwrite), "extension":"png"],
                                                      percentUploadBlock: percentUploadBlock,
                                                      withCompletionBlock: { [weak self](data, response, error) in
                                                        if let imageNumber = self?.imageNumberUploaded, let imageTotalCount = self?.imageValues.count{
                                                            if let d = data{
                                                                
                                                                let url = PostObject.stringifyData(d)
                                                                let completeUrl = directoryName + url
                                                                //print(completeUrl)
                                                                self?.imageValues[imageNumber].url = URL(string: completeUrl)
                                                                
                                                            }
                                                            completionImage(imageNumber, imageTotalCount)
                                                            
                                                            self?.imageNumberUploaded = imageNumber + 1
                                                            self?.saveRemotely(withDelegate: delegate,
                                                                               baseFileName: baseFileName,
                                                                               directoryName: directoryName,
                                                                               rootUrl:rootUrl,
                                                                               overwrite: overwrite,
                                                                               checkForSubDirectoryName:false,
                                                                               percentUploadBlock: percentUploadBlock,
                                                                               completionImage: completionImage,
                                                                               completionFinal: completionFinal)
                                                        }
                                                        
                        })
                    }
                }
                
            }
            
        }else{ //Forth: count ended, set the other parameters and call the final completion
            self.imageSequenceBaseUrl = directoryName + baseFileName
            self.imageSequenceCount = self.imageValues.count
            self.imageSequenceFormat = ImageObject.digitNumber(for: self.imageValues.count) + 1
            self.rootUrl = rootUrl
            self.imageNumberUploaded = nil
            self.finalDirectory = nil
            
            completionFinal(self.imageValues.count)
            
        }
    }
    
    //Removing
    public func deleteFromRemote(withDelegate delegate:ImageObjectProtocol, completionBlock completion:((Data?, URLResponse?, Error?) -> Swift.Void)?){
        let post = PostObject(with: delegate.imageObjectProtocolPhpFileToDelete())
        let folderUrl = self.imageSequenceBaseUrl?.stringByDeletingLastPathComponent
        
        if let baseUrlImage = folderUrl{
            let finaleBaseUrlImage = "\(delegate.imageObjectProtocolDirectoryImage())\(baseUrlImage)"
            post.sendAsynchronousPost(withDict: ["url":finaleBaseUrlImage]) { (data, response, error) in
                if let d = data{
                    PostObject.logData(d)
                }
                completion?(data, response, error)
            }
        }
    }
    
    
    //Downloading
    
    var post:ImageDownloader?
    
    public func downloadImages(utilizeFileManager: Bool,
                               utilizeMainBundle:Bool,
                               baseUrlToRemoveForMainBundle:String?,
                               headers:[String:String]?,
                               percentDownloadBlock: @escaping (Float) -> Void,
                               completionStartImageWithNumber: @escaping (_ imageCount:Int, _ totalCount:Int) -> Void,
                               completionSingleImage: @escaping (NSImage?, Int, Error?) -> Void,
                               completionAllImages: @escaping ([NSImage]) -> Void) {
        
        if self.post != nil {
            self.post?.cancelDownload()
        }
        if self.imageUrls.count > 0{
            if let urlImage = self.imageUrls[0]{
                self.post = ImageDownloader(with: urlImage)
                self.post?.utilizeFileManager = utilizeFileManager
                self.post?.utilizeMainBundle = utilizeMainBundle
                self.post?.baseUrlToRemoveForMainBundle = baseUrlToRemoveForMainBundle
                if let headerDict = headers{
                    self.post?.headerDict = headerDict
                }
                self.post?.downloadImages(from: self,
                                          percentDownloadBlock: percentDownloadBlock,
                                          completionStartImageWithNumber: completionStartImageWithNumber,
                                          completionSingleImage: completionSingleImage,
                                          completionAllImages: completionAllImages)
            }
        }
    }
}
