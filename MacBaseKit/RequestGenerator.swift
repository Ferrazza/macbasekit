//
//  RequestGenerator.swift
//  PostKit
//
//  Created by Alessandro Ferrazza on 10/03/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//


class RequestGenerator: NSObject {
    //MARK: request
    public var url:URL
    public var uploadFileName:String?
    public var cacheUrlRequest:Bool
    
    var headerValues = [String: String]()

    public init(with url: URL){
        self.url = url
        self.cacheUrlRequest = false
        
    }
    
    func generateRequest() -> URLRequest{
        return self.generateRequest(with: self.url)
    }
    private func generateRequest(with url:URL) -> URLRequest{
        var cachePolicy: URLRequest.CachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
        if self.cacheUrlRequest {
            cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
        }
        var request = URLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: 60.0)
        for key in self.headerValues.keys {
            if let headerValue = self.headerValues[key]{
                request.addValue(headerValue, forHTTPHeaderField: key)
            }
        }
        return request
    }
    
    func generateRequest(post:String) -> URLRequest{
        var request = self.generateRequest(with:self.url)
        let postEncoded = PostObject.urlEncode(string: post)
        if let postEncodedUnw = postEncoded{
            let postData = postEncodedUnw.data(using: String.Encoding.ascii, allowLossyConversion: true)
            if let postDataUnw = postData{
                let postLength: String? = "\(UInt(postDataUnw.count))"
                //Setto i parametri della richiesta. Per questa va specificato:
                //- metodo POST o altro
                //- dati dell’header:
                //	- come lunghezza per headerField @"Content-Length"
                //	- il valore di default @"application/x-www-form-urlencoded" per l’headerField @"Content-Type"
                //- il body per cui passo i dati del post creato sopra
                request.httpMethod = "POST"
                request.setValue(postLength, forHTTPHeaderField: "Content-Length")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.httpBody = postData
            }
        }
        return request
    }
    
    func generateRequest(get:String) -> URLRequest{
        var request = self.generateRequest(with:self.url)
        
        let getEncoded = PostObject.urlEncode(string: get)
        if let getEncodedUnw = getEncoded{
            var stringURL: String = self.url.absoluteString
            stringURL = stringURL.appendingFormat("?%@", getEncodedUnw)
            let finalURL = URL(string: stringURL)
            if let finalURLUnw = finalURL{
                request = self.generateRequest(with:finalURLUnw)
            }else{
                print("Error in generating get url")
            }
        }
        return request
    }
    
    
    func generateRequest(for fileData: Data, withFileName fileName: String, dictPost: [String: String]?) -> URLRequest {
        //--------------------------------------
        //ATTENZIONE: con urlsession the body stream and body data in this request object are ignored.
        //Utilize uploadtask with a simple request
        //--------------------------
        
        var request = generateRequest(with: self.url)
        self.uploadFileName = fileName
        request.httpMethod = "POST"
        //1. nell’header field content type va specificato anche un boundary per poter separare i vari campi:
        let boundary: String = "fsdhiupfhuiph"
        //il boundary è una qualsiasi stringa
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        //2. Creo il body del post creando un NSData che contiene le stringhe e i file:
        var body = Data()
        //boundary
        if let boundaryData = "\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8){
            body.append(boundaryData)
        }
        //nome del file
        if let fileNameData = "Content-Disposition: form-data; name=\"userfile\"; filename=\"\(fileName)\"\r\n".data(using: String.Encoding.utf8){
            body.append(fileNameData)
        }
        //tipo di dati
        if let fileTypeData = "Content-Type: application/octet-stream\r\n\r\n".data(using: String.Encoding.utf8){
            body.append(fileTypeData)
        }
        //dati
        body.append(fileData)
        //POST
        if let dictPostUnw = dictPost {
            for key in dictPostUnw.keys {
                if let value = dictPostUnw[key]{
                    //BONDUARY INTERMEDIO
                    if let midBoundaryData = "\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8){
                        body.append(midBoundaryData)
                    }
                    //altro post semplice
                    if let postKey = "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8){
                        body.append(postKey)
                    }
                    if let postValue = "\(value)".data(using: String.Encoding.utf8){
                        body.append(postValue)
                    }
                }
            }
        }
        //boundary di chiusura
        if let closingBoundaryData = "\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8){
            body.append(closingBoundaryData)
        }
        //setto il body
        request.httpBody = body
        //setto la lunghezza del content alla lunghezza dei dati appena creati
        //NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
        //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        return request
    }

}
