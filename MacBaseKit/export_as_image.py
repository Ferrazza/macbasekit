###-per importare sottopacchetti: from PACKAGEMOTHER import PACKAGESON
###-per salvare un immagine (oggetto di tipo PIL.Image.

# based on pydicom_PIL.py from pydicom
# Copyright (c) 2009 Darcy Mason, Adit Panchal
# This file is part of pydicom, relased under an MIT license.
#    See the file license.txt included with this distribution, also
#    available at http://pydicom.googlecode.com

# Based on image.py from pydicom version 0.9.3,
#    LUT code added by Adit Panchal
# Tested on Python 2.5.4 (32-bit) on Mac OS X 10.6
#    using numpy 1.3.0 and PIL 1.1.7b1
import dicom, os, sys, StringIO
from os.path import basename


have_PIL = True
try:
    from PIL import Image
except:
    have_PIL = False

have_numpy = True
try:
    import numpy as np
except:
    have_numpy = False

have_numpy = True
try:
    import numpy as np
except:
    have_numpy = False



def get_LUT_value(data, window, level):
    """Apply the RGB Look-Up Table for the given data and window/level value."""
    if not have_numpy:
        raise ImportError("Numpy is not available. See http://numpy.scipy.org/ to download and install")
    w = window
    l = level

    #osirix give a DICOM with two level of window and level in form of array
    #in this case the first level is token
    if isinstance(w, list) == True:
        w = float(window [0])
    if isinstance(l, list) == True:
        l = float(level[0])

    #to the following formula founded on internet I added this to actually correct for the right level
    l = l + 1024

    #print("Window: " + str(window))
    #print("Level: " + str(level))
    condlist = [data <= (l - 0.5 - (w - 1) / 2), data > (l - 0.5 + (w - 1) / 2)]
    funclist = [0, 255, lambda data: ((data - (l - 0.5)) / (w - 1) + 0.5) * (255 - 0)]
    return np.piecewise(data,
                        condlist,
                        funclist)

def save_PIL(dataset, window = None, level = None):
    """Create a PIL image object from the specified dataset
	to create the dataset utilize -> dicom.read_file(filename)
	window and level are optional values to be specifies
	if not specified, those present in dataset will be utilized
	if no WindowWidth and WindowCenter are specified in dataset, will be utilized default values (window 600, level 0)
	return None and print the error if there is a problem in creating the image"""
    log = False #set true to see log data
    if not have_PIL:
        raise ImportError("Python Imaging Library is not available. See http://www.pythonware.com/products/pil/ to download and install")
    if ('PixelData' not in dataset):
        raise TypeError("Cannot show image -- DICOM dataset does not have pixel data")
    #calculating mode with bits and samples
    bits = dataset.BitsAllocated
    samples = dataset.SamplesPerPixel
    if bits == 8 and samples == 1:
        mode = "L"
    elif bits == 8 and samples == 3:
        mode = "RGB"
    elif bits == 16:
        mode = "I;16"  # not sure about this -- PIL source says is 'experimental' and no documentation. Also, should bytes swap depending on endian of file and system??
    else:
        raise TypeError("Don't know PIL mode for %d BitsAllocated and %d SamplesPerPixel" % (bits, samples))
    #calculating image size
    size = (dataset.Columns, dataset.Rows)
    
    
    #log some data
    if log == True:
        print("\nCreating image with following data:")
        print("Bits allocated: " + str(bits))
        print("Samples: " + str(samples))
        print("Mode: " + mode)
        print("Image dimensions: " + str(size))
    #checking for window and level
    #if not specified try to get those in dataset
    #if not present assign default values
    if (window == None) or (level == None):
        if ('WindowWidth' in dataset) and ('WindowCenter' in dataset):
            if log == True:
                print("Utilize Window and Level in dataset:")
            window = dataset.WindowWidth
            level = dataset.WindowCenter
        else:
            if log == True:
                print("No window or Level set and no WindowWidth and WindowCenter in dataset of dicom, utilize default values")
            window = 600
            level = 0
    else:
        if log == True:
            print("Utilize Window and Level specified:")
        
    if log == True:
        print("Window: " + str(window) + "\nLevel: " + str(level))
    
    #------------------------------------------------------
    #if (window == None) or (level == None):
        #if no window or level is set framebuffer is utilized but this does not work for all dicom
        #With the following save only image in tiff and the images are black (I think because of there are no window or level)
        #im = PIL.Image.frombuffer(mode, size, dataset.PixelData, "raw", mode, 0, 1)  # Recommended to specify all details by http://www.pythonware.com/library/pil/handbook/image.htm
    #------------------------------------------------------
    
    try:
        #scaling data on the base of window and level
        image = get_LUT_value(dataset.pixel_array, window, level)
        #create the image object
        im = Image.fromarray(image, mode).convert("L")# Convert mode to L since LUT has only 256 values: http://www.pythonware.com/library/pil/handbook/image.htm
        #otherwise cannot be converted in png if is in format I;16. It must be converted in "L" mode
        return(im)

    except BaseException as e:
        if log == True:
            print("------------------------------")
            print("ERROR IN GENERATING THE IMAGE: ")
            print(e)
        return None

def convert_to_png_in_path(path, window = None, level = None):
    for root, dirs, files in os.walk(path):
        print("------------------------------")
        print('Evaluating folder "' + root + '"' + '\ncontaining ' + str(len(files)) + ' files')
        
        files.sort()
        countTotal = 0
        countImageConv = 0
        dcmfiles = list()
        for file in files:
            if file.endswith('.dcm'):
                dcmfiles.append(file)
        
        if len(dcmfiles) > 0:
            dir = root + '/images'
            try:
                os.stat(dir)
            except:
                os.mkdir(dir)       
            countTotal = len(dcmfiles)
            dcmfiles_not_conv= list()
            for file in dcmfiles:
                filePath = root + '/' + file
                print(' - converting ' + filePath)
                dcm = dicom.read_file(filePath)
                img = save_PIL(dcm, window, level)
                if (img != None):
                    countImageConv += 1
                    #name = basename(file) not working
                    #name.replace('.dcm','') not working
                    name = os.path.splitext(file)[0]
                    img.save(dir + '/' + name + '.png')
                else:
                    print('  -> error with ' + filePath)
                    dcmfiles_not_conv.append(filePath)
            print('\n')
            print('Total dcm files = ' + str(countTotal))
            print('Total dcm files converted = ' + str(countImageConv))
            if len(dcmfiles_not_conv) > 0:
                print('The following files have not be converted:')
                for f in dcmfiles_not_conv:
                    print(' - ' + f)
        else:
            print('no dcm files in "' + root + '"')

def convert_to_png(files, window = None, level = None):
    dcmfiles = list()
    images = list()
    imageNames = list()

    dcmfiles_not_conv = list()
    countTotal = 0
    countImageConv = 0
    
    for file in files:
        if file.endswith('.dcm'):
            path = file.replace("file://", "")
            path = path.replace("%20", " ")
            dcmfiles.append(path)
    countTotal = len(dcmfiles)

    if len(dcmfiles) > 0:
        for filePath in dcmfiles:
            #print(' - converting ' + filePath)
            dcm = dicom.read_file(filePath)

            img = save_PIL(dcm, window, level)
            if (img != None):
                countImageConv += 1
                #write the image on string thath can be passed to swift
                output = StringIO.StringIO()
                format = 'PNG' # or 'JPEG' or whatever you want
                img.save(output, format)
                contents = output.getvalue()
                output.close()
                images.append(contents)
                #img.show()
                #name = basename(file) not working
                #name.replace('.dcm','') not working
                #name = os.path.splitext(file)[0]

                #imageName = 'image' + str(countImageConv) + '.png'
                #img.save('/' + imageName)
                #imageNames.append(imageName)



            else:
                #print('  -> error with ' + filePath)
                dcmfiles_not_conv.append(filePath)

    '''
    print('\n')
    print('Total dcm files = ' + str(countTotal))
    print('Total dcm files converted = ' + str(countImageConv))
    if len(dcmfiles_not_conv) > 0:
        print('The following files have not be converted:')
        for f in dcmfiles_not_conv:
            print(' - ' + f)
    else:
        print('no errors')
    '''
    return images

def convert_dicom_to_png(file, window = None, level = None):
    image = ""
    path = file.replace("file://", "")
    path = path.replace("%20", " ")

    dcm = dicom.read_file(path)
        
    img = save_PIL(dcm, window, level)
    if (img != None):
        #write the image on string thath can be passed to swift
        output = StringIO.StringIO()
        format = 'PNG' # or 'JPEG' or whatever you want
        img.save(output, format)
        contents = output.getvalue()
        output.close()
        image = contents
        #img.show()
        #name = basename(file) not working
        #name.replace('.dcm','') not working
        #name = os.path.splitext(file)[0]
        
        #imageName = 'image' + str(countImageConv) + '.png'
        #img.save('/' + imageName)
        #imageNames.append(imageName)
        

    return image

