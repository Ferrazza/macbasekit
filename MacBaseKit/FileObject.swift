//
//  FileObject.swift
//  FileKit
//
//  Created by Alessandro Ferrazza on 11/03/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//


public class FileObject: NSObject {
    public static let shared = FileObject()

    public static let mainDocDirectory = "main_doc_directory"
    public var docDirectory:String!{
        get{
            let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let selectedPath = path[0]
            let url = URL(string: selectedPath)
            let result = url?.appendingPathComponent(FileObject.mainDocDirectory).absoluteString
            return result
        }
    }

    func docDirectoryStringByAppending(pathComponent: String)->String?{
        var url = URL(string: self.docDirectory)
        url?.appendPathComponent(pathComponent)
        return url?.absoluteString
    }
    func docDirectoryUrlByAppending(pathComponent: String)->URL?{
        var url = URL(string: self.docDirectory)
        url?.appendPathComponent(pathComponent)
        return url
    }
    
    public func url(for fileName:String)->URL?{
        return self.docDirectoryUrlByAppending(pathComponent: fileName)
    }
    
    public func listOfFileInDocDirectory() -> [String]{
        var result = [String]()
        do{
            result = try FileManager.default.contentsOfDirectory(atPath: self.docDirectory)
        }catch{
            print("error in evaluating content of doc directory")
        }
        return result
    }
    
    func image(for path:String?)->NSImage?{
        var image:NSImage?
        if let p = path{
            if FileManager.default.fileExists(atPath: p){
                image = NSImage(contentsOfFile: p)
            }
        }else{
            print("FileManager do not find a file with path \(String(describing: path))")
        }
        return image
    }
    
    public func imageInMainBundle(with path:String)->NSImage?{
        let bundlePath = Bundle.main.path(forResource: path, ofType: nil)
        return self.image(for: bundlePath)
    }
    
    public func image(named: String)->NSImage?{
        let filePath = self.docDirectoryStringByAppending(pathComponent: named)
        return self.image(for: filePath)
    }
    
    public func save(image:NSImage, with name:String){
        if !FileManager.default.fileExists(atPath: self.docDirectory){
            do{
                try FileManager.default.createDirectory(atPath: self.docDirectory,
                                                        withIntermediateDirectories: false,
                                                        attributes: nil)
            }catch{
                print("Error in creating docDirectory with error:\(error.localizedDescription)")
            }
        }
        if let pathUrl = self.docDirectoryUrlByAppending(pathComponent: name){
            var finalPath = URL(string: "file://")
            finalPath?.appendPathComponent(pathUrl.absoluteString)
            if let path = finalPath{
                if let data = image.PNGRepresentation(){
                    do{
                        try data.write(to: path, options: Data.WritingOptions.atomic)
                    }catch{
                        print("Error in writing the image in fileName \(name), with error:\(error.localizedDescription)")
                    }
                }else{
                    print("Error in converting image with NSImagePNGRepresentation")
                }
            }
            
        }
    }
    
    //MARK: Managing file
    func file(for path:String?)->Data?{
        var data:Data?
        if let p = path{
            if FileManager.default.fileExists(atPath: p){
                if let url = URL(string: p){
                    do{
                        data = try Data(contentsOf: url)
                    }catch{
                        print("Error in loading file in fileName \(p)")
                    }
                }
            }
        }else{
            print("FileManager do not find a file with path \(String(describing: path))")
        }
        return data
    }
    
    public func fileInMainBundle(with path:String)->Data?{
        let bundlePath = Bundle.main.path(forResource: path, ofType: nil)
        return self.file(for: bundlePath)
    }
    
    public func fileNamed(name: String) -> Data? {
        let filePath: String = URL(fileURLWithPath: docDirectory).appendingPathComponent(name).absoluteString
        if FileManager.default.fileExists(atPath: filePath) {
            return self.file(for: filePath)
        }
        return nil
    }
    
    public func save(data: Data?, name: String) {
        if !FileManager.default.fileExists(atPath: docDirectory) {
            if !(((try? FileManager.default.createDirectory(atPath: docDirectory, withIntermediateDirectories: false, attributes: nil)) != nil)) {
                
                print("Create directory error")
            }
        }
        let filePath = URL(fileURLWithPath: docDirectory).appendingPathComponent(name)
        do{
            try data?.write(to: filePath, options: Data.WritingOptions.atomic)
        }catch{
            print("error in saving data")
        }
        
    }

    //MARK: Remove
    public func removeFile(withName name:String){
        if let filePath = self.docDirectoryStringByAppending(pathComponent: name){
            if FileManager.default.fileExists(atPath: self.docDirectory){
                do{
                    try FileManager.default.removeItem(atPath: filePath)
                }catch{
                    print("Error in removing file with name \(name)")
                }
            }
  
        }
    }
    
    public func removeFileThatBegin(withName name:String){
        let filesWithSelectedPrefix = self.listOfFileInDocDirectory().filter { (element) -> Bool in
            return element.hasPrefix(name)
        }
        for file in filesWithSelectedPrefix{
            self.removeFile(withName: file)
        }
    }
    public func removeFile(withNames names:[String]){
        for file in names{
            self.removeFile(withName: file)
        }
    }
    
    public func removeAllFileInDocDirectory(){
        if FileManager.default.fileExists(atPath: self.docDirectory){
            do{
                try FileManager.default.removeItem(atPath: self.docDirectory)
            }catch{
                print("Error in removing all file in doc directory")
            }
        }
    }

    //MARK: Helper
    public static func fileName(for url:URL)->String{
        let components = url.pathComponents
        var result = ""
        let separator = "-"
        for i in 0..<components.count{
            let component = components[i]
            if i == 1{
                result = component
            }else{
                result = "\(result)\(separator)\(component)"
            }
        }
        return result
    }
}













