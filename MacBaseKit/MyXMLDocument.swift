//
//  MyXMLDocument.swift
//  XmlKit
//
//  Created by Alessandro Ferrazza on 05/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Foundation


public class MyXMLDocument:NSObject, XMLParserDelegate{
    public var root: MyXMLElement!
    public var characterEncoding = "UTF-8"
    public var versionXML = "1.0"
    var parser:XMLParser!
    var actualElement:MyXMLElement?
    
    
    public init(root:MyXMLElement) {
        self.root = root
        self.root.childLevel = 0

    }
    //MARK: init with data

    public init(data:Data) {
        super.init()

        self.parser = XMLParser(data: data)
        self.parser.delegate = self
        if self.parser.parse() == false{
            if let error = self.parser.parserError{
                print("parse error: \(error)")
            }
        }
    }
    
    public convenience init(contentsOfFile path: URL) {
        var dataParser = Data()
        do{
            dataParser = try Data(contentsOf: path, options: .uncached)
        }catch{
            print("error in loading xml from file path \(path))")
        }
        self.init(data: dataParser)
    }

    //MARK: Helper
    public func printXML() {
        print(self.toString())
    }
    public func toData() -> Data?{
        let string = self.toString()
        let data = string.data(using: String.Encoding.utf8)
        return data
    }
    
    public func toString() -> String{
        var result = "<?xml"
        result.append(" version=\"\(self.versionXML)\"")
        result.append(" encoding=\"\(self.characterEncoding)\"")
        result.append("?>\n")
        result.append(self.root.toString())
        return result
    }
    
    
    
    //MARK: XMLParserDelegate
    public func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String] = [:]) {
        let element = MyXMLElement(name: elementName,
                                   attributes: attributeDict,
                                   value: nil)
        if self.actualElement == nil{
            self.actualElement = element
            self.root = element
            
        }else{
            self.actualElement?.add(child: element)
            self.actualElement = element
        }
    }
    
    public func parser(_ parser: XMLParser,
                foundCharacters string: String) {
        if self.actualElement?.value == nil{
            self.actualElement?.value = string
        }else{
            self.actualElement?.value?.append(string)
        }
        
    }
    
    public func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?) {
        if let actual = self.actualElement{
            if actual === self.root{
                //end parsing
            }else{
                self.actualElement = self.actualElement?.parent as? MyXMLElement
            }
        }
    }
}
