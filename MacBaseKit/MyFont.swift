//
//  MyFont.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 02/07/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation
extension NSFont {
    
    func withTraits(traits:NSFontDescriptor.SymbolicTraits) -> NSFont {
        let descriptor = self.fontDescriptor.withSymbolicTraits(traits)
        return NSFont(descriptor: descriptor, size: 0)! //size 0 means keep the size as it is
    }
    
    public func bold() -> NSFont {
        return self.withTraits(traits: NSFontDescriptor.SymbolicTraits.bold)
    }
    
    public func italic() -> NSFont {
        return self.withTraits(traits: NSFontDescriptor.SymbolicTraits.italic)
    }
    
    public func italicBold() -> NSFont {
        return self.withTraits(traits: [NSFontDescriptor.SymbolicTraits.italic,  NSFontDescriptor.SymbolicTraits.bold])
    }
    
}
