//
//  XMLDownloader.swift
//  XmlKit
//
//  Created by Alessandro Ferrazza on 28/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//


public class XMLDownloader: PostObject {
    public static var password:String?//This is a password utilized to decrypt xml encripted on download. If not set, no decription will be applied

    public var utilizeFileManager = false //if set on utilize File Manager To Save files - default = YES
    public var utilizeMainBundle = false //if set YES utilize MainBundle To Load Files (if there are no images in main bundle utilizes other resources)
    public var baseUrlToRemoveForMainBundle: String? //Remove the base url from the main url
    
    public var saveInFileManager = false //this permits to save on file at every new download even if the utilizeFileManager is set to false. This is useful to refresh the file
    
    public func downloadXMLDocument(withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void, completionBlock completion: @escaping (_ document: MyXMLDocument?, _ error: Error?) -> Void) {
        
        if self.utilizeMainBundle {
            DispatchQueue.global(qos: .default).async(execute: {[weak self]() -> Void in
                var mainBundleUrl = self?.url?.path
                
                if let baseUrlToRemoveForMainBundle = self?.baseUrlToRemoveForMainBundle {
                    mainBundleUrl = self?.url?.path.replacingOccurrences(of: baseUrlToRemoveForMainBundle, with: "")
                }
                var docSaved:MyXMLDocument?
                if let url = mainBundleUrl{
                    if let dataSaved = FileObject.shared.fileInMainBundle(with: url){
                        docSaved = MyXMLDocument(data: dataSaved)
                        
                    }
                }
                
                if docSaved != nil {
                    DispatchQueue.main.async(execute: {() -> Void in
                        percentDownloadBlock(1)
                        completion(docSaved, nil)
                    })
                }else{
                    //If there is not image in main bundle else utilize other resources setting it to NO
                    self?.utilizeMainBundle = false
                    self?.downloadXMLDocument(withPercentDownloadBlock: percentDownloadBlock, completionBlock: completion)
                    //if (completion) completion(NO, nil, nil);
                    /*
                     dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf innerDownloadImageWithPercentDownloadBlock:percentDownloadBlock completionBlock:completion];
                     });
                     */
                }
            })
        }else if self.utilizeFileManager {
            let docName = FileObject.fileName(for: self.url)
            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                var docSaved:MyXMLDocument?
                if let dataSaved = FileObject.shared.fileNamed(name: docName){
                    docSaved = MyXMLDocument(data: dataSaved)
                }
                
                if docSaved != nil {
                    DispatchQueue.main.async(execute: {() -> Void in
                        percentDownloadBlock(1)
                        completion(docSaved, nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.innerDownloadXMLDocument(withPercentDownloadBlock: percentDownloadBlock, completionBlock: completion)
                    })
                }
            })
        }else{
            self.innerDownloadXMLDocument(withPercentDownloadBlock: percentDownloadBlock, completionBlock: completion)
        }
        
    }
    
    func innerDownloadXMLDocument(withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void, completionBlock completion: @escaping (_ document: MyXMLDocument?, _ error: Error?) -> Void) {
        self.downloadAsynchronous(percentDownloadBlock: percentDownloadBlock) { [weak self](data, response, error) in
            var doc: MyXMLDocument?
            
            if var d = data {
                
                //-----------------------ENCRIPTION----------------------
                //If the password has been set the data will be decripted
                //Note that only downloaded data will be decripted and not data stored locally
                //The data must be properly encripted on upload
                //-------------------------------------------------------
                
                if let pass = XMLDownloader.password{
                    if let decriptedData = XOREncription.decrypt(dataUtf8String: d, with: pass){
                        d = decriptedData
                    }
                }
                doc = MyXMLDocument(data: d)
                if let document = doc{

                    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                        //save anyway in file manager on downloading
                        if let weakSelf = self{
                            if weakSelf.saveInFileManager || weakSelf.utilizeFileManager {
                                if let docData = document.toData(){
                                    FileObject.shared.save(data: docData, name: FileObject.fileName(for: weakSelf.url))
                                }
                            }
                        }
                        
                    })
                }
                completion(doc, error)

            }else{
                completion(nil, error)
            }
        }
    }
}
