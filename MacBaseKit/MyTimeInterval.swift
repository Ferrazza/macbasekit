//
//  MyTimeInterval.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 21/05/2020.
//  Copyright © 2020 Alessandro Ferrazza. All rights reserved.
//

import Foundation
extension TimeInterval {

func stringify() -> String {
    let ti = NSInteger(self)
    let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
    let seconds = ti % 60
    let minutes = (ti / 60) % 60
    let hours = (ti / 3600)
    return String(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
}
}
