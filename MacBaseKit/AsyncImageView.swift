//
//  AsyncImageView.swift
//  TableKit
//
//  Created by Alessandro Ferrazza on 09/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//


public class AsyncImageView: NSImageView {

/*
    var post:ImageDownloader?
    public var imageObject: ImageObject?
/*
    public var images = [NSImage]()
    public var imageUrls = [URL]()
    public var imageUrl:URL?
 */
    public var headerDict:[String:String]?
    public var utilizeFileManager = false //if set on utilize File Manager To Save files - default = NO
    public var utilizeMainBundle = false //if set YES utilize MainBundle To Load Files (if there are no images in main bundle utilizes other resources)
    public var baseUrlToRemoveForMainBundle:String? //Remove the base url from the main url
    
    public var automaticallySetImage = true //if set the image of imageView or not - default = YES
    public var progressViewStyle = UIProgressViewStyle.default
    public var progressView:UIProgressView!
    public var labelNumberImageInSequence:UILabel!
    
    public var hasUrl:Bool{
        get{
            if let obj = self.imageObject{
                if obj.imageValue?.url != nil{
                    return true
                }
            }
            
            return false
        }
    }
    override public var image: NSImage?{
        didSet{
            self.removeProgressionObjects()
        }
    }
    public init(frame: CGRect, progressViewStyle: UIProgressViewStyle?) {
        super.init(frame: frame)
        
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        if let style = progressViewStyle{
            self.progressViewStyle = style
        }
        self.loadProgressionViewsIsImageSequence(imageSequence: false)
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: Load single image
    public func loadImage(from urlImage: URL,
                   withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void,
                   withCompletionBlock completionBlock: @escaping (_ success: Bool, _ image: NSImage, _ error: Error?) -> Void) {
        self.image = nil
        self.loadProgressionViewsIsImageSequence(imageSequence: false)
        self.imageObject = ImageObject(imageUrl: urlImage)
        if self.post != nil {
            self.post?.cancelDownload()
        }
        self.post = ImageDownloader(with: urlImage)
        self.post?.utilizeFileManager = self.utilizeFileManager
        self.post?.utilizeMainBundle = self.utilizeMainBundle
        self.post?.baseUrlToRemoveForMainBundle = self.baseUrlToRemoveForMainBundle
        
        self.post?.downloadImage(percentDownloadBlock: { [weak self](percent) in
                self?.progressView.progress = percent},
                                 completionBlock: { [weak self] (image, error) in
            if let i = image {
                if let setImage = self?.automaticallySetImage{
                    if setImage{
                        self?.image = i
                        self?.imageObject?.imageValue?.image = image
                    }
                }
                completionBlock(true, i, error)
            }else{
                //create a fake image
                if let fakeImage = self?.createFakeImage(){
                    completionBlock(false, fakeImage, error)
                }
            }
        })
    }
    public func loadImage(from urlImage: URL,
                   withCompletionBlock completionBlock: @escaping (_ success: Bool, _ image: NSImage, _ error: Error?) -> Void) {
        self.loadImage(from: urlImage,
                       withPercentDownloadBlock: { (percent) in
                        //do nothing
        }, withCompletionBlock: completionBlock)
    }

    
    
    //MARK: Load multiple images
    /*
    public func loadImages(from urlsImage: [URL]?,
                    withArray array: [Any],
                    withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void,
                    withStartImageNumberBlock startImageNumberBlock: @escaping (_ imageCount: Int, _ totalCount: Int) -> Void,
                    withImageDownloadingBlock imageDownloadingBlock: @escaping (_ imageNumber: Int, _ image: NSImage) -> Void,
                    withCompletionBlock completionBlock: @escaping (_ images: [Any]) -> Void) {
        if let urlsImageUnw = urlsImage{
            self.imageUrl = urlsImageUnw[0];
            self.imageUrls = urlsImageUnw;
            self.loadImages(fromArray: array,
                            withPercentDownloadBlock: percentDownloadBlock,
                            withStartImageNumberBlock: startImageNumberBlock,
                            withImageDownloadingBlock: imageDownloadingBlock,
                            withCompletionBlock: completionBlock)

        }else{
            completionBlock(array)
        }
        
    }
 */

    public func loadImages(fromUrlsImage urlsImage: [URL],
                    withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void,
                    withStartImageNumberBlock startImageNumberBlock: @escaping (_ imageCount: Int, _ totalCount: Int) -> Void,
                    withImageDownloadingBlock imageDownloadingBlock: @escaping (_ imageNumber: Int, _ image: NSImage) -> Void,
                    withCompletionBlock completionBlock: @escaping (_ images: [NSImage]) -> Void) {
        self.imageObject = ImageObject(imageUrls: urlsImage)
        self.loadImages(fromImageObject: self.imageObject!,
                        withPercentDownloadBlock: percentDownloadBlock,
                        withStartImageNumberBlock: startImageNumberBlock,
                        withImageDownloadingBlock: imageDownloadingBlock,
                        withCompletionBlock: completionBlock)
        
    }
    
   
    public func loadImages(fromImageObject imageObject: ImageObject,
                           withPercentDownloadBlock percentDownloadBlock: @escaping (_ percentDownload: Float) -> Void,
                           withStartImageNumberBlock startImageNumberBlock: @escaping (_ imageCount: Int, _ totalCount: Int) -> Void,
                           withImageDownloadingBlock imageDownloadingBlock: @escaping (_ imageNumber: Int, _ image: NSImage) -> Void,
                           withCompletionBlock completionBlock: @escaping (_ images: [NSImage]) -> Void) {
        self.image = nil;
        self.imageObject = imageObject
        self.loadProgressionViewsIsImageSequence(imageSequence: imageObject.isSequence)
        //if imageObject.isSequence{
        //    self.addSubview(self.labelNumberImageInSequence)
        //}
        
        self.imageObject?.downloadImages(utilizeFileManager: self.utilizeFileManager,
                                         utilizeMainBundle: self.utilizeMainBundle,
                                         baseUrlToRemoveForMainBundle: self.baseUrlToRemoveForMainBundle,
                                         headers: nil,
                                         percentDownloadBlock: { [weak self](percent) in
                                            percentDownloadBlock(percent)
                                            self?.progressView.progress = percent},
                                         completionStartImageWithNumber: startImageNumberBlock,
                                         completionSingleImage: { [weak self](image, number, error) in
                                            if let i = image{
                                                imageDownloadingBlock(number, i)
                                            }
                                            if let label = self?.labelNumberImageInSequence{
                                                label.text = "\(number + 1)/\(imageObject.images.count)"
                                            }},
                                         completionAllImages: { (images) in
                                            completionBlock(images)}
                                            //self.removeProgressionObjects()
                                            //self.image = images[0]

        )
        
        
    }

    //MARK: Setup subviews
    public func loadProgressionViewsIsImageSequence(imageSequence: Bool) {
        if self.progressView == nil {
            self.progressView = UIProgressView(progressViewStyle: .default)
        }
        var progressionViewFrame: CGRect = self.bounds.insetBy(dx: CGFloat(10), dy: CGFloat(10))
        progressionViewFrame.size.height = 10
        progressionViewFrame.origin.y = self.bounds.size.height / 3 * 2
        self.progressView.frame = progressionViewFrame
        self.addSubview(self.progressView)
        if imageSequence {
            if self.labelNumberImageInSequence == nil {
                self.labelNumberImageInSequence = NST)
                self.labelNumberImageInSequence.font = UIFont.systemFont(ofSize: CGFloat(20))
                self.labelNumberImageInSequence.textColor = UIColor.white
                self.labelNumberImageInSequence.textAlignment = .center
            }
            let inset: CGFloat = 10
            var labelFrame: CGRect = bounds.insetBy(dx: CGFloat(inset), dy: CGFloat(inset))
            labelFrame.size.height = progressionViewFrame.origin.y - inset * 2;
            self.labelNumberImageInSequence.frame = labelFrame;
            self.addSubview(self.labelNumberImageInSequence)

        }
    }
    public func removeProgressionObjects(){
        if self.labelNumberImageInSequence != nil{
            self.labelNumberImageInSequence.removeFromSuperview()
        }
        if self.progressView != nil{
            self.progressView.removeFromSuperview()
        }
    }
    
    //MARK: HELPER
    
    public func createFakeImage() -> NSImage{
        return AsyncImageView.noConnectionImage(with: UIColor.lightGray, size: CGFloat.maximum(self.frame.size.width, self.frame.size.height))
        
    }
    static public func noConnectionImage(with color: UIColor, size: CGFloat) -> NSImage {
        // begin a graphics context of sufficient size
        UIGraphicsBeginImageContextWithOptions(CGSize(width: size, height: size), false, UIScreen.main.scale)
        var initialImage = NSImage()
        // draw original image into the context
        initialImage.draw(at: CGPoint.zero)
        // get the context for CoreGraphics
        if let ctx = UIGraphicsGetCurrentContext(){
            // set stroking color and draw circle
            color.setStroke()
            //get radius
            ctx.saveGState()
            let insectFactor: CGFloat = 0.1
            let drawingInset: CGFloat = size * insectFactor
            var drawingRect = CGRect(x: 0, y: 0, width: size, height: size)
            drawingRect = drawingRect.insetBy(dx: drawingInset, dy: drawingInset)
            let drawingSize = drawingRect.size.height
            let initialAngle:CGFloat = CGFloat( -Double.pi/2 - Double.pi/4)
            let finalAngle:CGFloat = CGFloat( -Double.pi/2 + Double.pi/4)
            let radius = drawingSize / 6
            let centerX = size / 2
            let centerY = size - drawingInset
            let lineWidth = drawingInset / 4
            ctx.saveGState()
            let centerPath = CGMutablePath()
            centerPath.addRelativeArc(center: CGPoint(x: centerX, y: centerY), radius: lineWidth, startAngle: 0, delta: CGFloat(Double.pi * 2), transform: .identity)
            ctx.addPath(centerPath)
            for i in 1..<6 {
                let path = CGMutablePath()
                path.addRelativeArc(center: CGPoint(x: centerX, y: centerY), radius: radius * CGFloat(i), startAngle: initialAngle, delta: finalAngle - initialAngle, transform: .identity)
                ctx.addPath(path)
            }
            ctx.setLineWidth(CGFloat(lineWidth))
            ctx.strokePath()
            ctx.restoreGState()
            let cruxPath = CGMutablePath()
            //CGPathMoveToPoint(cruxPath, NULL, CGRectGetMinX(drawingRect), CGRectGetMinY(drawingRect));
            //CGPathAddLineToPoint(cruxPath, NULL, CGRectGetMaxX(drawingRect), CGRectGetMaxY(drawingRect));
            cruxPath.move(to: CGPoint(x: drawingRect.maxX, y: drawingRect.minY), transform: .identity)
            cruxPath.addLine(to: CGPoint(x: drawingRect.minX, y: drawingRect.maxY), transform: .identity)
            ctx.addPath(cruxPath)
            ctx.setLineWidth(CGFloat(lineWidth * 2))
            ctx.setLineCap(CGLineCap.round)
            ctx.strokePath()
            // make image out of bitmap context
            let retImage: NSImage? = UIGraphicsGetImageFromCurrentImageContext()
            // free the context
            UIGraphicsEndImageContext()
            if let image = retImage{
                initialImage = image
            }
            
        }
        
        return initialImage
    }
*/
}
