//
//  NNDatasetGenerator.swift
//  MacBaseKit
//
//  Created by Alessandro Ferrazza on 20/01/18.
//  Copyright © 2018 Alessandro Ferrazza. All rights reserved.
//

import Foundation
public class NNElement{
    public var percentages:[Float]
    public init(percentages: [Float]) {
        self.percentages = percentages
    }
}
public class NNDatasetGenerator{
    
    public static let shared = NNDatasetGenerator()

    
    //Batch size = 1 -> Array of an array of 4 values
    public func newDataset(with elements:[NNElement], size:Int) -> NeuralNet.Dataset?{
        let inputsAndLabelsTrain = self.generateInputAndLabels(with: elements, size: size)
        let trainInputs = inputsAndLabelsTrain.inputs
        let trainLabels = inputsAndLabelsTrain.labels
        //regenerate for validation
        let inputsAndLabelsValidation = self.generateInputAndLabels(with: elements, size: size)
        let validationInputs = inputsAndLabelsValidation.inputs
        let validationLabels = inputsAndLabelsValidation.labels
        
        do{
            let dataset = try NeuralNet.Dataset(trainInputs: trainInputs,
                                                trainLabels: trainLabels,
                                                validationInputs: validationInputs,
                                                validationLabels: validationLabels)
            
            return dataset
        }catch{
            print("Error in generating dataset")
        }
        return nil
    }
    
    //Batch size > 1 -> Array of a 2D array: Array of Batch size of array of 4 values
    public func new3DDataset(with elements:[NNElement], maxEphocs:Int, batchSize:Int) -> NeuralNet.Dataset?{
        var trainInputs = [[[Float]]]()
        var trainLabels = [[[Float]]]()
        var validationInputs = [[[Float]]]()
        var validationLabels = [[[Float]]]()
        
        for _ in 0...maxEphocs - 1{
            let inputsAndLabelsTrain = self.generateInputAndLabels(with: elements, size: batchSize)
            trainInputs.append(inputsAndLabelsTrain.inputs)
            trainLabels.append(inputsAndLabelsTrain.labels)
            //regenerate for validation
            let inputsAndLabelsValidation = self.generateInputAndLabels(with: elements, size: batchSize)
            validationInputs.append(inputsAndLabelsValidation.inputs)
            validationLabels.append(inputsAndLabelsValidation.labels)
        }
        
        do{
            let dataset = try NeuralNet.Dataset(trainInputs: trainInputs,
                                                trainLabels: trainLabels,
                                                validationInputs: validationInputs,
                                                validationLabels: validationLabels)
            return dataset
        }catch{
            print("Error in generating dataset")
        }
        return nil
        
        
    }
    
    var dataSetElementCount = 0
    func generateInputAndLabels(with elements:[NNElement], size:Int)->(inputs: [[Float]],labels: [[Float]]) {
        //create dict of inputs
        var count = 0
        var inputs = [[Float]]()
        var labels = [[Float]]()
        
        while count < size {
            let element = elements[self.dataSetElementCount]
            //generate single input
            var input = [Float]()
            for p in 0...element.percentages.count - 1{
                let percentage = element.percentages[p]
                input.append(self.generateRandom(for: percentage))
            }
            inputs.append(input)
            //generate single label
            var label = [Float]()
            for e in 0...elements.count - 1{
                if e == self.dataSetElementCount{
                    label.append(1)
                }else{
                    label.append(0)
                }
            }
            labels.append(label)
            //Increasing count
            count = count + 1
            //updating dataSetElementCount
            self.dataSetElementCount = self.dataSetElementCount + 1
            if self.dataSetElementCount >= elements.count{
                self.dataSetElementCount = 0
            }
        }
        let result = (inputs:inputs, labels:labels)
        return result
    }

    
    //MARK: Helper
    /**
     take a percentage and give back 1 or 0 with that frequence
     */
    func generateRandom(for percentage:Float) -> Float{
        let randomNumber = Float(arc4random_uniform(100))
        let num = randomNumber/100
        if num < percentage{
            return 1
        }else{
            return 0
        }
    }
    
    //MARK: Try

    func tryOneMillionRandomGeneration(with percentage:Float){
        var array = [Float]()
        var totalCount:Int = 0
        for _ in 0...1000000{
            totalCount = totalCount + 1
            let num = self.generateRandom(for: percentage)
            if num > 0.5{
                array.append(num)
            }
        }
        print("\(percentage) = \(Float(array.count)/Float(totalCount))")
    }
}
