//
//  DragDestinationView.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 11/05/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

public protocol DragDestinationViewProtocol {
    func dragDestinationViewConvertedImageFromDicom(withPercent:Double, name:String, errorString: String?, view:DragDestinationView)

    func dragDestinationViewReturn(images:[NSImage], point: NSPoint, view:DragDestinationView)
    func dragDestinationViewCrop(in rect:CGRect, view:DragDestinationView)
    
}
open class DragDestinationView: NSView{
    
    

    
    public var delegate:DragDestinationViewProtocol? = nil
    open var isDrawing = false {
        didSet {
            self.needsDisplay = true
        }
    }
    var drawStartPoint = CGPoint(x: 0, y: 0)
    var drawEndPoint = CGPoint(x: 0, y: 0){
        didSet {
            self.needsDisplay = true
        }
    }
    

    open override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        //To color the view if a drag in perfoming
        if self.isReceivingDrag {
            NSColor.selectedControlColor.set()
            
            let path = NSBezierPath(rect:bounds)
            path.lineWidth = 5
            path.stroke()
        }
        if self.isDrawing{
            NSColor.selectedControlColor.set()
            let rect = self.rectToDraw()
            let path = NSBezierPath(rect:rect)
            path.lineWidth = 1
            path.stroke()
        }
    }
    
    func rectToDraw()->CGRect{
        var origin = self.drawStartPoint
        var width = self.drawEndPoint.x - self.drawStartPoint.x
        var height = self.drawEndPoint.y - self.drawStartPoint.y
        if self.drawEndPoint.x < origin.x{
            origin.x = self.drawEndPoint.x
            width = self.drawStartPoint.x - self.drawEndPoint.x
        }
        if self.drawEndPoint.y < origin.y{
            origin.y = self.drawEndPoint.y
            height = self.drawStartPoint.y - self.drawEndPoint.y
        }
        return CGRect(x: origin.x, y: origin.y, width: width, height: height)
    }
    
    var acceptableTypes: [NSPasteboard.PasteboardType] {
        //return [NSPasteboard.PasteboardType.fileURL]//only availaben on ios 10.13
        return [NSPasteboard.PasteboardType("Apple URL pasteboard type")]

    }
    func setup() {
        
        self.registerForDraggedTypes(self.acceptableTypes)
    }
    //Created a dictionary to define the desired URL types (images)
    var filteringOptions:[NSPasteboard.ReadingOptionKey:[String]]{
        get{
            var types = NSImage.imageTypes
            types.append("org.nema.dicom") //this is the UTI Universal Type Identifier for dicom file
            //types.append("public.data")
            let result = [NSPasteboard.ReadingOptionKey.urlReadingContentsConformToTypes:types]
            return result
        }
    }
    

    func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
        if self.isImportingImages{
            return false
        }else{
            //Got a reference to the dragging pasteboard from the dragging session info
            let pasteBoard = draggingInfo.draggingPasteboard
            //print(draggingInfo)
            //Asked pasteboard if it has any URLs and whether those URLs are references to images. If it has images, it accepts the drag. Otherwise, it rejects it.
            var canAccept = false
            if pasteBoard.canReadObject(forClasses: [NSURL.self], options: self.filteringOptions) {
                canAccept = true
            }
            return canAccept
        }
        
    }
    
    
    //A property to know if is receiving drag. triggers a redraw on the view each time it is set
    var isReceivingDrag = false {
        didSet {
            self.needsDisplay = true
        }
    }
    
    //Called when drag entered or exit
    open override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        let allow = self.shouldAllowDrag(sender)
        self.isReceivingDrag = allow
        return allow ? NSDragOperation.copy : NSDragOperation()
        //During drag, if you hold a button can change what append. For example, if you hold down Option during a Finder drag, the pointer gains a green + symbol to show you a file copy is about to happen. This value (NSDragOperation) is how you control those pointer changes.
        //This returns .copy to show the user that you’re about to copy the image.
    }
    open override func draggingExited(_ sender: NSDraggingInfo?) {

        self.isReceivingDrag = false
    }
    
    //Called first when release the drag - last method to reject
    open override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let allow = self.shouldAllowDrag(sender)
        return allow
    }
    
    var imageCount = 0
    var isImportingImages = false

    open override func performDragOperation(_ draggingInfo: NSDraggingInfo) -> Bool {
        //Here you can do animation for dropping
        //1.
        self.isReceivingDrag = false
        self.isImportingImages = true
        let pasteBoard = draggingInfo.draggingPasteboard
        
        //Convert windows coordinate to view coordinate
        let point = self.convert(draggingInfo.draggingLocation, from: nil)
        //Accept url
        
        if let urls = pasteBoard.readObjects(forClasses: [NSURL.self], options:filteringOptions) as? [URL], urls.count > 0 {
            //Type
            let url = urls[0]
            let extention = url.pathExtension

            var images = [NSImage]()
            self.imageCount = 0
            if extention == "dcm"{
                
                PythonManager.shared.dicomsToImages(from: urls,
                                                    completionSingleImage: {[weak self] (imageCount, image, errorString) in
                                                        if let weakSelf = self{
                                                            let actualUrl = urls[weakSelf.imageCount]
                                                            let imageName = actualUrl.lastPathComponent
                                                           
                                                            weakSelf.imageCount = weakSelf.imageCount + 1
                                                            let percent = Double(weakSelf.imageCount)/Double(urls.count)
                                                            self?.delegate?.dragDestinationViewConvertedImageFromDicom(withPercent:percent, name: imageName, errorString: errorString, view: weakSelf)
                                                        }},
                                                    completionFinal: {[weak self] (images) in
                                                        if let weakSelf = self{
                                                            self?.delegate?.dragDestinationViewReturn(images: images, point: point, view: weakSelf)
                                                            weakSelf.isImportingImages = false
                                                        }
                })
            }else{
                for url in urls{
                    if let image = NSImage(contentsOf:url) {
                        images.append(image)
                    }
                }
                delegate?.dragDestinationViewReturn(images: images, point: point, view: self)
                self.isImportingImages = false

            }
            return true
        }
        return false
        
    }
    //Cropping image
    
    open override func mouseDown(with event: NSEvent) {
        let point = self.convert(event.locationInWindow, from: nil)
        self.drawStartPoint = point
        self.drawEndPoint = point
        self.isDrawing = true
    }
    open override func mouseDragged(with event: NSEvent) {
        let point = self.convert(event.locationInWindow, from: nil)
        self.drawEndPoint = point
    }
    open override func mouseUp(with event: NSEvent) {
        let point = self.convert(event.locationInWindow, from: nil)

        self.drawEndPoint = point

        let rect = self.rectToDraw()
        self.delegate?.dragDestinationViewCrop(in: rect, view: self)
        self.isDrawing = false
        self.needsDisplay = true
    }

    
    
}
